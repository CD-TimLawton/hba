<?php
/**
 *  @package     AkeebaStrapper
 *  @copyright   Copyright (c) 2010 - 2015 Nicholas K. Dionysopoulos / Akeeba Ltd. All rights reserved.
 *  @license     GNU General Public License version 2, or later
 */

// Protect from unauthorized access
defined('_JEXEC') or die;

define('AKEEBASTRAPPER_VERSION', 'rev88D4B337');
define('AKEEBASTRAPPER_DATE', '2017-10-04');
define('AKEEBASTRAPPER_MEDIATAG', md5(AKEEBASTRAPPER_VERSION . AKEEBASTRAPPER_DATE));