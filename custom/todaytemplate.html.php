<?php
$template = <<<EOD
{headertemplate}

<td width="599" align="left" valign="top" style="background: #FFFFFF" height="237" bgcolor="#FFFFFF">
<div class="faux_h2 " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 18px; line-height: 26px; margin: 0; padding: 0"><span class="faux_h2_color"><b>Dear {firstname}</b></span></div>
<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color"><br /></span></div>

<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color">Your membership has now expired and your teacher listing will be removed from our website shortly.</span></div>
<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color"><br /></span></div>

<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color">We are sorry to see you go and if you would like to renew your membership at any time please login to the website <a href="http://www.thehypnobirthingassociation.com/log-in.html">here</a> and follow the steps for renewal.</span></div>
<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color"><br /></span></div>

<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color">Do call us if you would like to talk it through on 01264 731437.</span></div>
<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color"><br /></span></div>

<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color">Warm regards,</span></div>
<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color">Jane Pelham</span></div>
<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color">The Hypnobirthing Association</span></div>
<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color"><br /></span></div>
</td>


{footertemplate}
EOD;
?>