<?php
//  Send Subscription Expiry emails

define( '_JEXEC', 1 );
define( 'DS', DIRECTORY_SEPARATOR );
define( 'JPATH_BASE', $_SERVER[ 'DOCUMENT_ROOT' ] );

require_once( "../configuration.php");
require_once( "../includes/defines.php");
require_once( "../includes/framework.php");
require_once( "../libraries/joomla/base/object.php");
require_once( "../libraries/joomla/factory.php");
require_once( "../libraries/joomla/methods.php");
require_once( "../libraries/joomla/filesystem/path.php");
require_once( "../libraries/loader.php");
require_once( "../libraries/joomla/error/error.php");
require_once( "../libraries/joomla/database/database.php");
require_once( "../libraries/joomla/database/table.php");

//comment out as appropriate
//$live = 1;
echo "<h1>Expiry Emails</h1>";
$live = 1;

$testaddress = 'luke@channeldigital.co.uk';

$db = &JFactory::getDBO();

//dates for expiry
$less28days = strtotime("+28 day");
$less28daysformat = date('Y-m-d', $less28days);

$less7days = strtotime("+7 day");
$less7daysformat = date('Y-m-d', $less7days);

$todayday = strtotime("now");
$todaydayformat = date('Y-m-d', $todayday);

include "headertemplate.html.php"; // sets $headertemplate
include "footertemplate.html.php"; // sets $footertemplate
include "cssinclude.html.php"; // sets $cssinclude

//query for Paypal expiry in 28 days
$query = $db->getQuery(true);
$query->select(array('*'))
    ->from ('#__acctexp_subscr a')
    ->join ('LEFT',' #__users u ON u.id = a.userid ')
     ->where (' expiration >= '.$db->quote($less28daysformat.' 00:00:00').' AND expiration <= '.$db->quote($less28daysformat.' 23:59:59').' AND lifetime = 0 AND TYPE = "paypal_subscription" AND status IN ("Active", "Trial") AND block = 0');
$db->setQuery($query);
$results = $db->loadObjectList();
echo "<h2>28 Day : Paypal</h2>";
echo count($results)." users<br />";
if ($db->getErrorMsg() != "")
    echo $db->getErrorMsg()."<br />";
if (count($results) > 0 )
{
    foreach ($results as $result)
    {
        // Send this person an email
        include "28daypaypaltemplate.html.php"; // sets $template
        $template = str_replace("{headertemplate}", $headertemplate, $template);
        $template = str_replace("{cssinclude}", $cssinclude, $template);
        $template = str_replace("{footertemplate}", $footertemplate, $template);
        $fullname = $result->name;
        $namebits = explode(" ", $fullname,2);
        $template = str_replace("{firstname}", $namebits[0], $template);
        $template = str_replace("{expirydate}", date("jS M Y", $less28days), $template);

        $subject = "The Hypnobirthing Association Membership Renewal";
        $from = "The Hypnobirthing Association <office@thehypnobirthingassociation.co.uk>";

        $headers  = "From: $from\r\n";
        if ($live == 1)
        {
            $to = $result->email;
            $headers .= "Bcc: luke@channeldigital.co.uk\r\n";
        } else
        {
            $to = $testaddress;
        }
        $uid = md5(uniqid(time()));
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: multipart/alternative; boundary=\"".$uid."\"\r\n\r\n";
        $headers .= "--".$uid."\r\n";

        $headers .= "Content-Type: text/html\r\n";

        $footer = "\r\n\r\n--".$uid."--\r\n\r\n";

        echo "to: $to<br />";
        echo "subject: $subject<br />";
        echo "headers : <pre>$headers</pre><br />";
        echo "Message : <pre>$template</pre></br />";

        $ok = mail($to, $subject, $template.$footer, $headers);
    }
}

//query for No Auto Renewal expiry in 28 days
$query = $db->getQuery(true);
$query->select(array('*'))
    ->from ('#__acctexp_subscr a')
    ->join ('LEFT',' #__users u ON u.id = a.userid ')
     ->where (' expiration >= '.$db->quote($less28daysformat.' 00:00:00').' AND expiration <= '.$db->quote($less28daysformat.' 23:59:59').' AND lifetime = 0 AND TYPE = "none" AND status IN ("Active", "Trial")');
$db->setQuery($query);
$results = $db->loadObjectList();
echo "<h2>28 Day : No auto renewal</h2>";
echo count($results)." users<br />";
if ($db->getErrorMsg() != "")
    echo $db->getErrorMsg()."<br />";
if (count($results) > 0 )
{
    foreach ($results as $result)
    {
        // Send this person an email
        include "28daytemplate.html.php"; // sets $template
        $template = str_replace("{headertemplate}", $headertemplate, $template);
        $template = str_replace("{cssinclude}", $cssinclude, $template);
        $template = str_replace("{footertemplate}", $footertemplate, $template);
        $fullname = $result->name;
        $namebits = explode(" ", $fullname,2);
        $template = str_replace("{firstname}", $namebits[0], $template);
        $template = str_replace("{expirydate}", date("jS M Y", $less28days), $template);

        $subject = "The Hypnobirthing Association Membership Renewal";
        $from = "The Hypnobirthing Association <office@thehypnobirthingassociation.co.uk>";

        $headers  = "From: $from\r\n";
        if ($live == 1)
        {
            $to = $result->email;
            $headers .= "Bcc: luke@channeldigital.co.uk\r\n";
        } else
        {
            $to = $testaddress;
        }
        $uid = md5(uniqid(time()));
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: multipart/alternative; boundary=\"".$uid."\"\r\n\r\n";
        $headers .= "--".$uid."\r\n";

        $headers .= "Content-Type: text/html\r\n";

        $footer = "\r\n\r\n--".$uid."--\r\n\r\n";

        echo "to: $to<br />";
        echo "subject: $subject<br />";
        echo "headers : <pre>$headers</pre><br />";
        echo "Message : <pre>$template</pre></br />";

        $ok = mail($to, $subject, $template.$footer, $headers);
    }
}

//query for Paypal expiry in 7 days
// It'll auto-renew and we've already told them
// Don't send another email

//query for No Auto Renewal expiry in 7 days
$query = $db->getQuery(true);
$query->select(array('*'))
    ->from ('#__acctexp_subscr a')
    ->join ('LEFT',' #__users u ON u.id = a.userid ')
     ->where (' expiration >= '.$db->quote($less7daysformat.' 00:00:00').' AND expiration <= '.$db->quote($less7daysformat.' 23:59:59').' AND lifetime = 0 AND TYPE = "none" AND status IN ("Active", "Trial")');
$db->setQuery($query);
$results = $db->loadObjectList();
echo "<h2>7 Day : No auto renewal</h2>";
echo count($results)." users<br />";
if ($db->getErrorMsg() != "")
    echo $db->getErrorMsg()."<br />";
if (count($results) > 0 )
{
    foreach ($results as $result)
    {
        // Send this person an email
        include "7daytemplate.html.php"; // sets $template
        $template = str_replace("{headertemplate}", $headertemplate, $template);
        $template = str_replace("{cssinclude}", $cssinclude, $template);
        $template = str_replace("{footertemplate}", $footertemplate, $template);
        $fullname = $result->name;
        $namebits = explode(" ", $fullname,2);
        $template = str_replace("{firstname}", $namebits[0], $template);
        $template = str_replace("{expirydate}", date("jS M Y", $less7days), $template);

        $subject = "The Hypnobirthing Association Membership Renewal";
        $from = "The Hypnobirthing Association <office@thehypnobirthingassociation.co.uk>";

        $headers  = "From: $from\r\n";
        if ($live == 1)
        {
            $to = $result->email;
            $headers .= "Bcc: luke@channeldigital.co.uk\r\n";
        } else
        {
            $to = $testaddress;
        }
        $uid = md5(uniqid(time()));
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: multipart/alternative; boundary=\"".$uid."\"\r\n\r\n";
        $headers .= "--".$uid."\r\n";

        $headers .= "Content-Type: text/html\r\n";

        $footer = "\r\n\r\n--".$uid."--\r\n\r\n";

        echo "to: $to<br />";
        echo "subject: $subject<br />";
        echo "headers : <pre>$headers</pre><br />";
        echo "Message : <pre>$template</pre></br />";

        $ok = mail($to, $subject, $template.$footer, $headers);
    }
}

//query for Paypal expiry today
// No email, will auto renew

//query for Expiry today, no auto renewal
$query = $db->getQuery(true);
$query->select(array('*'))
    ->from ('#__acctexp_subscr a')
    ->join ('LEFT',' #__users u ON u.id = a.userid ')
     ->where (' expiration >= '.$db->quote($todaydayformat.' 00:00:00').' AND expiration <= '.$db->quote($todaydayformat.' 23:59:59').' AND lifetime = 0 AND `type` = "none" AND status IN ("Active", "Trial")');
$db->setQuery($query);
$results = $db->loadObjectList();
echo "<h2>Expiry today</h2>";
echo count($results)." users<br />";
if ($db->getErrorMsg() != "")
    echo $db->getErrorMsg()."<br />";
if (count($results) > 0 )
{
    foreach ($results as $result)
    {
        // Send this person an email
        include "todaytemplate.html.php"; // sets $template
        $template = str_replace("{headertemplate}", $headertemplate, $template);
        $template = str_replace("{cssinclude}", $cssinclude, $template);
        $template = str_replace("{footertemplate}", $footertemplate, $template);
        $fullname = $result->name;
        $namebits = explode(" ", $fullname,2);
        $template = str_replace("{firstname}", $namebits[0], $template);
        $template = str_replace("{expirydate}", date("jS M Y", $todayday), $template);

        $subject = "The Hypnobirthing Association Membership Renewal";
        $from = "The Hypnobirthing Association <office@thehypnobirthingassociation.co.uk>";

        $headers  = "From: $from\r\n";
        if ($live == 1)
        {
            $to = $result->email;
            $headers .= "Bcc: luke@channeldigital.co.uk\r\n";
        } else
        {
            $to = $testaddress;
        }
        $uid = md5(uniqid(time()));
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: multipart/alternative; boundary=\"".$uid."\"\r\n\r\n";
        $headers .= "--".$uid."\r\n";

        $headers .= "Content-Type: text/html\r\n";

        $footer = "\r\n\r\n--".$uid."--\r\n\r\n";

        echo "to: $to<br />";
        echo "subject: $subject<br />";
        echo "headers : <pre>$headers</pre><br />";
        echo "Message : <pre>$template</pre></br />";

        $ok = mail($to, $subject, $template.$footer, $headers);

        // Send an email to Admin telling them someone has expired
        include "todayadmintemplate.html.php"; // sets $template
        $template = str_replace("{headertemplate}", $headertemplate, $template);
        $template = str_replace("{cssinclude}", $cssinclude, $template);
        $template = str_replace("{footertemplate}", $footertemplate, $template);
        $subject = "HBA Membership Expiry";
        $template = str_replace("{name}", $fullname, $template);
        $template = str_replace("{expirydate}", date("jS M Y", $todayday), $template);
        $template = str_replace("{email}", $result->email, $template);
        $template = str_replace("{userid}", $result->id, $template);

        echo "<br />to: $to<br />";
        echo "subject: $subject<br />";
        echo "headers : <pre>$headers</pre><br />";
        echo "Message : <pre>$template</pre></br />";

        if ($live == 1)
        {
            $to = "office@thehypnobirthingassociation.com";
            $headers .= "Bcc: luke@channeldigital.co.uk\r\n";
        } else
        {
            $to = $testaddress;
        }
        $ok = mail($to, $subject, $template.$footer, $headers);
    }
}



?>
