<?php
    $template = <<<EOD
{headertemplate}


<td width="599" align="left" valign="top" style="background: #FFFFFF" height="237" bgcolor="#FFFFFF">

<div class="faux_h2 " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 18px; line-height: 26px; margin: 0; padding: 0"><span class="faux_h2_color"><b>Dear {firstname}</b></span></div>
<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color"><br /></span></div>

<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color">We are sure you have enjoyed and benefitted from your membership of The Hypnobirthing Association during the year.  Your membership is due for renewal on {expirydate} and we look forward to welcoming you as a member again.</span></div>
<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color"><br /></span></div>

<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color">In order to renew in good time, please take the following steps:</span></div>
<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color"><br /></span></div>

<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color">1. Login to the Hypnobirthing Association website by clicking here  (<a href="http://www.thehypnobirthingassociation.com/log-in.html">http://www.thehypnobirthingassociation.com/log-in.html</a>)</span></div>
<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color"><br /></span></div>

<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color">2. On the Members Home Page, please click "Renew Your Membership" which you can find under the "Your Hypnobirthing teacher listing" heading.</span></div>
<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color"><br /></span></div>

<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color">By doing this you can renew automatically, the hassle-free way, so you won't have to worry about your membership lapsing.  It is easy to make any changes at any time by calling the office.</span></div>


<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color">It is a great help to us if you set up automatic renewals as to do this manually over the phone takes nearly half an hour for Jane or Sandy in the office, although of course we are always happy to help when needed.  If you set up an automated renewal you will be sent two reminders in advance each year before any money is taken from your account when you will be given the option to cancel if you so desire.  You would always receive two reminders, well in advance, before the subscription became due.  I hope this sets your mind at rest.</span></div>


<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color"><br /></span></div>

<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color">Please renew your membership as soon as possible so that you can continue to enjoy membership of The Hypnobirthing Association.</span></div>
<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color"><br /></span></div>

<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color">Warm regards,</span></div>
<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color">Jane Pelham</span></div>
<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color">The Hypnobirthing Association</span></div>
<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color"><br /></span></div>
</td>


{footertemplate}
EOD;
?>