<?php
    $template = <<<EOD
{headertemplate}


<td width="599" align="left" valign="top" style="background: #FFFFFF" height="237" bgcolor="#FFFFFF">
<div class="faux_h2 " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 18px; line-height: 26px; margin: 0; padding: 0"><span class="faux_h2_color"><b>Dear {firstname}</b></span></div>
<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color"><br /></span></div>

<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color">Your Hypnobirthing Association membership is due for renewal on {expirydate}.  You don't have to worry about your membership lapsing because you have arranged to renew automatically the hassle-free way and pay your subscription of &pound;60 with Paypal on that date.</span></div>
<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color"><br /></span></div>

<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color">If you wish to make any changes or cancel your subscription, please contact us on 0845 337 9149 or email us at office@thehypnobirthingassociation.com and we can help you, but you will need to log in to your Paypal account and cancel the automatic payment yourself.</span></div>
<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color"><br /></span></div>

<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color">We look forward to welcoming you to another year's membership of The Hypnobirthing Association.</span></div>
<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color"><br /></span></div>

<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color">Warm regards,</span></div>
<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color">Jane Pelham</span></div>
<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color">The Hypnobirthing Association</span></div>
<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color"><br /></span></div>
</td>


{footertemplate}
EOD;
?>