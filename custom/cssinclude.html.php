<?php
  $cssinclude = <<<CSSINCLUDE
    <style type="text/css">
    html, body{ width:100%; margin-top: 0px !important; padding-top: 0px !important; }
    body{ background-color:#FFFFFF; margin-top: 0px !important; padding-top: 0px !important; font-family:sans-serif; }
    table{ margin-top: 0px !important; padding-top: 0px !important; }
    a img{ color:#000001 !important; }

body{ padding-top:0; padding-right:0; padding-bottom:0; padding-left:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0; color:#000000 !important; font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size:13px; line-height:17px; }
    h1, .faux_h1{ padding-top:0; padding-right:0; padding-bottom:0; padding-left:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0; color:#000000 !important; font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size:20px; line-height:26px; }
    .faux_h1_color_color{ color:#000000; }
    h2, .faux_h2{ padding-top:0; padding-right:0; padding-bottom:0; padding-left:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0; color:#000000 !important; font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size:18px; line-height:26px; }
    .faux_h2_color_color{ color:#000000; }
    p, .faux_p{ padding-top:0; padding-right:0; padding-bottom:0; padding-left:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0; color:#000000 !important; font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size:12px; line-height:17px; }
    .faux_p_color_color{ color:#000000; }
    a, .faux_a{ padding-top:0; padding-right:0; padding-bottom:0; padding-left:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0; color:#1122CC !important; }
    .faux_a_color_color{ color:#1122CC; }
    .wysiwyg-text-align-right{ text-align: right; }
    .wysiwyg-text-align-center { text-align: center; }
    .wysiwyg-text-align-left{ text-align: left; }
    .wysiwyg-text-align-justify{ text-align: justify; }

</style>
CSSINCLUDE;
?>
