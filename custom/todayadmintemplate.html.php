<?php
    $template = <<<EOD
{headertemplate}


<td width="599" align="left" valign="top" style="background: #FFFFFF" height="237" bgcolor="#FFFFFF">
<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color">{name}'s membership has expired today ({expirydate}).</span></div>
<div class="faux_p " style="color: #000000 !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 17px; margin: 0; padding: 0"><span class="faux_p_color">Their email address is {email} if you would like to contact them, or alternatively click <a href="http://www.thehypnobirthingassociation.com/administrator/index.php?option=com_acctexp&task=editMembership&userid={userid}">here</a> to view more details</span></div>
</td>


{footertemplate}
EOD;
?>