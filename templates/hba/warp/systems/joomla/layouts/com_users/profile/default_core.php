<?php
/**
* @package   Warp Theme Framework
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// no direct access
defined('_JEXEC') or die;

?>

<?php 
// lets check for AEC plan, if none redirect them to the sign up page
$user =& JFactory::getUser();
$userId = $user->get( 'id' );
$userEmail = $user->get( 'email' );
$userName = $user->get( 'name' );
$formatteduserName = strtolower(str_replace(" ", "_", $userName));

 $db = &JFactory::getDBO();
// $db->setQuery("select userid from #__acctexp_subscr where userid = '62'");
 $db->setQuery("select userid from #__acctexp_subscr where userid = '".$userId."'");  
 $db->query();

 $rows = $db->loadObjectList();
	$itemrow = $rows[0];
	if (!$itemrow) {
		
		header( 'Location: /index.php?option=com_acctexp&task=subscribe&usage=3' );	

	} else {
			// lets check to see if they exist in sobipro, if not lets pop some data into them.
		   $dbSobi = &JFactory::getDBO();
		   $dbSobi->setQuery("select pid from #__sobipro_relations where pid = '".$userId."'");  
		   $sobiRows = $dbSobi->loadObjectList();
 		   $insertRow = $sobiRows[0];
		   $dbSobi->query();
		   
		   if (!$insertRow) {
  			   		   $date = date('Y-m-d');					   
					   
					   $insertObjectDb = JFactory::getDBO();
				
						$insertObjectDb->setQuery("insert into #__sobipro_object (approved, nid, name, confirmed, cout, oType, createdTime, owner, validSince, state) values (1, '".$formatteduserName."', '".$userName."', 0, 0, 'entry', CURRENT_TIMESTAMP, 63, CURRENT_TIMESTAMP, 1)");
						
						  if ($insertObjectDb->getErrorNum()) {
							 JError::raiseError("Error", $insertObjectDb->stderr());
						  }					
						
						$insertObjectDb->query();
						$lastId = $insertObjectDb->insertid();

					   
					 	$insertRelationDb = JFactory::getDBO();
						$insertQuery = "insert into #__sobipro_relations (id, pid, oType, position, validSince) values (".$lastId.", ".$userId.", 'entry', 1, CURRENT_TIMESTAMP)";
					    $insertRelationDb->setQuery($insertQuery);
						  if ($insertRelationDb->getErrorNum()) {
							 JError::raiseError("Error", $insertRelationDb->stderr());
						  }
						$insertRelationDb->query();
						
						$insertFieldDb = JFactory::getDBO();
						$insertFieldQuery = "insert into #__sobipro_field_option_selected (fid, sid, optValue, copy) values (98, ".$lastId.", 'subscription_full' ,0)";
					    $insertFieldDb->setQuery($insertFieldQuery);
						  if ($insertFieldDb->getErrorNum()) {
							 JError::raiseError("Error", $insertFieldDb->stderr());
						  }
						$insertFieldDb->query();

						$insertEmailDb = JFactory::getDBO();
						$insertEmailQuery = "insert into #__sobipro_field_data (fid, sid, section, lang, enabled, baseData, approved, createdTime, createdBy) values (48, ".$lastId.", 55, 'en-GB', 1, '".$userEmail."', 1, CURRENT_TIMESTAMP, 63)";
					    $insertEmailDb->setQuery($insertEmailQuery);
						  if ($insertEmailDb->getErrorNum()) {
							 JError::raiseError("Error", $insertEmailDb->stderr());
						  }
						$insertEmailDb->query();
						
						$insertNameDb = JFactory::getDBO();
						$insertNameQuery = "insert into #__sobipro_field_data (fid, sid, section, lang, enabled, baseData, approved, createdTime, createdBy) values (37, ".$lastId.", 55, 'en-GB', 1, '".$userName."', 1, CURRENT_TIMESTAMP, 63)";
					    $insertNameDb->setQuery($insertNameQuery);
						  if ($insertNameDb->getErrorNum()) {
							 JError::raiseError("Error", $insertNameDb->stderr());
						  }
						$insertNameDb->query();


					 	
						
			
			   
		   } else {
			// else if they exist, redirect to homepage
			//header( 'Location: /index.php' );	
	
	
		



//$getUser = "SELECT userid FROM e987h_acctexp_subscr WHERE userid = ".$userId."";
//$getResult = mysql_query($getUser) or die("Invalid Category query: " . mysql_error().__LINE__.__FILE__);
//if (mysql_num_rows == 0) {
//	header( Location: /index.php?option=com_acctexp&task=subscribe&usage=3 );
//}
?>
<h3><?php echo JText::_('COM_USERS_PROFILE_CORE_LEGEND'); ?></h3>
<ul>
	<li><strong><?php echo JText::_('COM_USERS_PROFILE_NAME_LABEL'); ?>:</strong> <?php echo $this->data->name; ?></li>
	<li><strong><?php echo JText::_('COM_USERS_PROFILE_USERNAME_LABEL'); ?>:</strong> <?php echo htmlspecialchars($this->data->username); ?></li>
	<li><strong><?php echo JText::_('COM_USERS_PROFILE_REGISTERED_DATE_LABEL'); ?>:</strong> <?php echo JHtml::_('date', $this->data->registerDate); ?></li>
	<li><strong><?php echo JText::_('COM_USERS_PROFILE_LAST_VISITED_DATE_LABEL'); ?>:</strong> <?php echo ($this->data->lastvisitDate != '0000-00-00 00:00:00' ? JHtml::_('date', $this->data->lastvisitDate) : JText::_('COM_USERS_PROFILE_NEVER_VISITED')); ?></li>
</ul>

<?php } 

	}
	
?>