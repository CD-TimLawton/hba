<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
<xsl:output method="xml" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"/>

<xsl:include href="../../../../../../../../components/com_sobipro/usr/templates/channel/common/navigation.xsl" />
<xsl:include href="../../../../../../../../components/com_sobipro/usr/templates/channel/common/topmenu.xsl" />
<xsl:include href="../../../../../../../../components/com_sobipro/usr/templates/channel/common/alphamenu.xsl" />
<xsl:include href="../../../../../../../../components/com_sobipro/usr/templates/channel/common/entries.xsl" />
<xsl:include href="../../../../../../../../components/com_sobipro/usr/templates/channel/common/category.xsl" />

<xsl:variable name="subcatsNumber">
	<xsl:value-of select="/category/number_of_subcats" />
</xsl:variable>
	
	<xsl:template match="/category">
		<xsl:variable name="rssUrlSection">{"sid":"<xsl:value-of select="section/@id"/>","sptpl":"feeds.rss","out":"raw"}</xsl:variable>	
		<xsl:variable name="sectionName"><xsl:value-of select="section"/></xsl:variable>
		<xsl:value-of select="php:function( 'SobiPro::AlternateLink', $rssUrlSection, 'application/atom+xml', $sectionName )" />
		<xsl:variable name="rssUrl">{"sid":"<xsl:value-of select="id"/>","sptpl":"feeds.rss","out":"raw"}</xsl:variable>	
		<xsl:variable name="categoryName"><xsl:value-of select="name"/></xsl:variable>
		<xsl:value-of select="php:function( 'SobiPro::AlternateLink', $rssUrl, 'application/atom+xml', $categoryName )" />
		<div class="SPListing">
		    <div>
		      <xsl:apply-templates select="menu" />
		      <xsl:apply-templates select="alphaMenu" />
		    </div>	
			<div style="clear:both;"/>
			
			<div class="spCategoryDesc"><h1>Hypnobirthing <xsl:value-of select="name"/> - Teacher's Directory</h1></div>
			
			{loadposition sobimap}
			
			<div class="spCatListContainer">
				<xsl:variable name="catsInLine">
					<xsl:value-of select="categories_in_line" />
				</xsl:variable>
				<xsl:variable name="cellWidth">
					<xsl:value-of select="(100 div $catsInLine) - 5" />
				</xsl:variable>
				<xsl:for-each select="categories/category">
					<div class="spCatListCell" style="width: 35%">
						<xsl:choose>
							<xsl:when test="(position() - 1) mod $catsInLine"></xsl:when>
							<xsl:otherwise><div style="clear:both;"/></xsl:otherwise>
						</xsl:choose>
						<xsl:call-template name="category" />
					</div>
				</xsl:for-each>
				<div style="clear:both;"/>
			</div>
			<div style="clear:both;"/>
			
			<xsl:call-template name="entriesLoop" />
			<xsl:apply-templates select="navigation" />
			<div style="clear:both;"/>
			
			<div class="spCategoryDesc">
				<h2 class="catdesch2">Hypnobirthing <xsl:value-of select="name"/></h2>
				<xsl:value-of select="description" disable-output-escaping="yes"/>
			</div>
			
			<div style="clear:both;"/>
		</div>
	
        <!-- TODO:JETS:Hack to Add the Category name to the Meta Title -->
        <xsl:variable name="myTitle">
            <xsl:text>Hypnobirthing </xsl:text><xsl:value-of select="name"/>
        </xsl:variable>
        <xsl:value-of select="php:function( 'RestaurantsTpl::AddTitle', $myTitle )" />
        <!-- END OF HACK -->
    </xsl:template>
    
</xsl:stylesheet>