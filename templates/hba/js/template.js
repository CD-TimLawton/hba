/* Copyright (C) YOOtheme GmbH, YOOtheme Proprietary Use License (http://www.yootheme.com/license) */

/* LB Channel - Custom JS for redrawing map */
function redrawmap() {
   document.getElementById("map").style.display="block";
   document.getElementById("map").style.position="inherit";
   jQuery(".posmap").css({"position":"relative","left":"0"});
}
function hideMap() {
   document.getElementById("map").style.display="none";
   jQuery(".posmap").css({"position":"absolute","left":"-999999px"});
   //document.getElementById("tabs-1").style.padding="0px";
}
/* LB Channel - Custom JS for redrawing map */

(function($){

	$(document).ready(function() {
		
		/* LB Channel - Hide map on page load if on SobiPro category page 	
		if ($(".SPListing")[0]){
		  	document.getElementById("map").style.position="relative";
			document.getElementById("map").style.left="-999999999px";
		}
		*/
							
		// Replace message for legacy customers with no billing details
		if ($('#system-message dd ul >li:contains("Missing value for First Name")').length > 0) {
			if ($('#system-message dd ul >li:contains("Missing value for Last Name")').length > 0) {
				if ($('#system-message dd ul >li:contains("Missing value for Phone")').length > 0) {
					if ($('#system-message dd ul >li:contains("Please accept the terms of service to confirm")').length > 0) {
				
						$("#system-message dd ul").html("<li>We don't seem to have any billing details for you yet, please enter them <a href='index.php?option=com_virtuemart&view=user&task=editaddresscart&addrtype=BT&Itemid=161'>here</a>.</li>");
					}
				}
			} 
		} 
		
		// Replace message for legacy customers with no billing details
		if ($('#system-message dd ul >li:contains("Missing value for State / Province / Region")').length > 0) {
			$("#system-message dd ul").html("<li>We seem to be missing the Region for your address, please enter it <a href='index.php?option=com_virtuemart&view=user&task=editaddresscart&addrtype=BT&Itemid=161'>here</a>.</li>");
		}

		var config = $('body').data('config') || {};
		
		// Accordion menu
		$('.menu-sidebar').accordionMenu({ mode:'slide' });

		// Smoothscroller
		$('a[href="#page"]').smoothScroller({ duration: 500 });

		// Social buttons
		$('article[data-permalink]').socialButtons(config);

		$(window).on("load", function(){
			// Dropdown menu
			$('#menu').dropdownMenu({ mode: 'slide', dropdownSelector: 'div.dropdown'});
		});

	});

	$.onMediaQuery('(min-width: 960px)', {
		init: function() {
			if (!this.supported) this.matches = true;
		},
		valid: function() {
			$.matchWidth('grid-block', '.grid-block', '.grid-h').match();
			$.matchHeight('main', '#maininner, #sidebar-a, #sidebar-b').match();
			$.matchHeight('top-a', '#top-a .grid-h', '.deepest').match();
			$.matchHeight('top-b', '#top-b .grid-h', '.deepest').match();
			$.matchHeight('bottom-a', '#bottom-a .grid-h', '.deepest').match();
			$.matchHeight('bottom-b', '#bottom-b .grid-h', '.deepest').match();
			$.matchHeight('innertop', '#innertop .grid-h', '.deepest').match();
			$.matchHeight('innerbottom', '#innerbottom .grid-h', '.deepest').match();

			// Match height if grid-a-licious is activated 
			if(window.GRIDALICIOUS_IN_USE) {

				$.onMediaQuery.spyclockside = true;

				setInterval((function(){

					var ret = function() {
						if(!$.onMediaQuery.spyclockside) return;

						$('#maininner, #sidebar-a, #sidebar-b').css("min-height", "");
						$.matchHeight('main', '#maininner, #sidebar-a, #sidebar-b').match();

					}

					ret();

					return ret;

				})(), 1000);
			}

		},
		invalid: function() {
			$.matchWidth('grid-block').remove();
			$.matchHeight('main').remove();
			$.matchHeight('top-a').remove();
			$.matchHeight('top-b').remove();
			$.matchHeight('bottom-a').remove();
			$.matchHeight('bottom-b').remove();
			$.matchHeight('innertop').remove();
			$.matchHeight('innerbottom').remove();

			$.onMediaQuery.spyclockside = false;
		}
	});

	var pairs = [];

	$.onMediaQuery('(min-width: 480px) and (max-width: 959px)', {
		valid: function() {
			$.matchHeight('sidebars', '.sidebars-2 #sidebar-a, .sidebars-2 #sidebar-b').match();
			pairs = [];
			$.each(['.sidebars-1 #sidebar-a > .grid-box', '.sidebars-1 #sidebar-b > .grid-box', '#top-a .grid-h', '#top-b .grid-h', '#bottom-a .grid-h', '#bottom-b .grid-h', '#innertop .grid-h', '#innerbottom .grid-h'], function(i, selector) {
				for (var i = 0, elms = $(selector), len = parseInt(elms.length / 2); i < len; i++) {
					var id = 'pair-' + pairs.length;
					$.matchHeight(id, [elms.get(i * 2), elms.get(i * 2 + 1)], '.deepest').match();
					pairs.push(id);
				}
			});
		},
		invalid: function() {
			$.matchHeight('sidebars').remove();
			$.each(pairs, function() { $.matchHeight(this).remove(); });
		}
	});

	$.onMediaQuery('(max-width: 767px)', {
		valid: function() {
			var header = $('#header-responsive');
			if (!header.length) {
				header = $('<div id="header-responsive"/>').prependTo('#header');
				$('#logo').clone().removeAttr('id').addClass('logo').appendTo(header);
				$('.searchbox').first().clone().removeAttr('id').appendTo(header);
				$('#menu').responsiveMenu().next().addClass('menu-responsive').appendTo(header);
			}
		}
	});

	
})(jQuery);