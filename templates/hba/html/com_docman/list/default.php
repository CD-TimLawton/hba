<?
/**
 * @package     DOCman
 * @copyright   Copyright (C) 2012 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */
defined('KOOWA') or die; ?>

<?= @helper('behavior.bootstrap'); ?>
<?= @helper('behavior.mootools'); ?>

<!--
<script src="media://lib_koowa/js/koowa.js" />
-->

<div class="com_docman">
    <? if ($params->show_add_document_button):
    	$route = '&view=document&layout=form&slug='.($category->slug ? '&category_slug='.$category->slug : ''); 
    ?>
    <div class="btn-toolbar">
        <a class="btn" href="<?= @route($route); ?>">
            <i class="icon-plus"></i> <?= @text('Add new document')?>
        </a>
    </div>
    <? endif; ?>

    <div id="docman-category" class="docman-heading category">
       
        <div>
            <? if ($params->show_category_title): ?>
            <h1 class="title">
                <?= @escape($category->title); ?>
            </h1>
            <? endif; ?>

            <? if ($category->description && $params->show_description): ?>
            <div class="description"><?= @prepareText($category->description); ?></div>
            <? endif; ?>
        </div>
    </div>
    <? if ($params->show_subcategories && count($subcategories)): ?>
    	<? if ($category->id): ?>
        <p class="docman-heading">
        	<?= @text('<strong>Select from</strong>') ?>
        </p>
        <? endif; ?>
        <?=@template('com://site/docman.view.list.categories', array(
        	'categories' => $subcategories, 
        	'params' => $params, 
        	'config' => $config,
        	'start_level' => $category->level
        ))?>
    <? endif; ?>

    <? if ($params->show_documents && count($documents)): ?>
    <p class="docman-heading"><?= @text('<strong>Available Downloads</strong>')?></p>
	<form action="<?= @route()?>" method="get" class="-koowa-grid">	
		<?= @template('com://site/docman.view.documents.list', array('documents' => $documents, 'params' => $params))?>

		<? if ($params->show_pagination !== '0' && $total): ?>
			<?= @helper('paginator.pagination', array_merge(array('total' => $total), $state->getData())) ?>
		<? endif; ?>
	</form>
    <? endif; ?>
</div>
