<? defined('KOOWA') or die('Restricted access'); ?>

<? if ($params->track_downloads): ?>
    <?= @helper('behavior.download_tracker'); ?>
<? endif; ?>

<? foreach ($documents as $document): ?>
<div class="docman-row">
    <div class="docman-group">
    	<? if ($params->show_document_created 
    		|| ($params->show_document_modified && $document->modified_by)
    		|| ($params->show_document_size && $document->size)): ?>
        <div class="docman-document-details">
            <table class="table table-bordered">
                <tbody>
                    <? if ($params->show_document_created): ?>
                    <tr>
                        <td class="detail-label"><?= @text('Created date'); ?></td>
                        <td><?= @date(array('date' => $document->publish_date)); ?></td>
                    </tr>
                    <? endif; ?>

                    <? //if ($params->show_document_modified && $document->modified_by): ?>
                   <!-- <tr>
                        <td class="detail-label"><?= @text('Modified date'); ?></td>
                        <td><?= @date(array('date' => $document->modified_on)); ?></td>
                    </tr>-->
                    <? //endif; ?>

                    <? if ($params->show_document_size && $document->size): ?>
                    <tr>
                        <td class="detail-label"><?= @text('Filesize'); ?></td>
                        <td>
                            <?= $document->size
                                ? @helper('com://admin/docman.template.helper.string.bytes2text', array('bytes' => $document->size))
                                : ''; ?>
                        </td>
                    </tr>
                    <? endif; ?>
                </tbody>
            </table>
        </div>
        <? endif; ?>
        <h4 class="docman-document-header">
            <? if (!empty($document->params->icon) && $params->show_document_icon): ?>
                <img align="left" src="<?= @helper('icon.path', array('icon' => $document->params->icon)) ?>" class="icon" />
            <? endif ?>
            <? if ($params->document_title_link): ?>
                <!--<a href="<?= ($document->title_link) ?>">--><?= @escape($document->title);?><!--</a>-->
            <? else: ?>
                <?= @escape($document->title);?>
            <? endif; ?>
            
            <?= @event('onContentAfterTitle', array($event_context, &$document, &$params)); ?>

            <? //if ($params->show_document_recent && $document->new): ?>
               <!-- <span class="label label-success"><?= @text('New'); ?></span>-->
            <? //endif; ?>
        </h4>

        <?= @event('onContentBeforeDisplay', array($event_context, &$document, &$params)); ?>
        
        <? if ($params->show_document_description): ?>
            <div class="docman-document-description"><?= @prepareText($document->description); ?></div>
        <? endif ?>
        <?= @template('com://site/docman.view.document.manage', array('document' => $document)) ?>
        
        <?= @event('onContentAfterDisplay', array($event_context, &$document, &$params)); ?>
	</div>
</div>
<? endforeach ?>