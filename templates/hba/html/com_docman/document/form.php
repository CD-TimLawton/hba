<?
/**
 * @package     DOCman
 * @copyright   Copyright (C) 2012 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */
defined('KOOWA') or die; ?>

<?= @helper('behavior.jquery'); ?>
<?= @helper('behavior.bootstrap', array('namespace' => false)); ?>
<?= @helper('behavior.mootools'); ?>
<?= @helper('behavior.keepalive'); ?>
<?= @helper('behavior.validator'); ?>

<!--
<script src="media://com_docman/js/document.js" />
<script src="media://lib_koowa/js/koowa.js" />
-->

<div class="frontend-toolbar">
<?= @helper('com://admin/docman.template.helper.toolbar.render', array(
	'toolbar' => $this->getView()->getToolbar()
));?>
</div>

<form action="<?= @route($document, 'layout=form'); ?>" method="post" class="-koowa-form" data-toolbar=".toolbar-list">
<div class="com_docman boxed" style="padding: 20px;">
	<fieldset class="form-horizontal">
		<legend><?= @text('Details'); ?></legend>
		<div class="control-group">
			<label class="control-label"><?= @text('Title'); ?></label>
			<div class="controls">
				<input class="required" id="title_field" type="text" class="title" size="25" name="title" value="<?= $document->title; ?>" class="required" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label"><?= @text('Category'); ?></label>
			<div class="controls">
				<?= @helper('listbox.categories', array(
						'identifier' => 'com://site/docman.model.categories',
				        'required' => true,
						'check_access' => true,
						'name' => 'docman_category_id', 
						'selected' => $document->docman_category_id,
						'filter' => array(
						    'page' => $state->page,
						    'access' => JFactory::getUser()->getAuthorisedViewLevels()
						)
				))?>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label"><?= @text('File Type'); ?></label>
			<div class="controls">
				<?= @helper('listbox.storage_types', array(
					'name'       => 'storage_type',
					'attribs'    => array('id' => 'storage_type'),
					'selected'   => $document->storage_type,
				))?>
			</div>
		</div>
		<?
			$http_value = $document->storage_type == 'http' ? $document->storage_path : '';
			$file_value = $document->storage_type == 'file' ? $document->storage_path : '';
		?>
		<div class="control-group" id="document-http-path-row" style="display: none">
			<label class="control-label"><?= @text('URL of Document'); ?></label>
			<div class="controls">
				<input class="validate-storage" data-type="http" id="storage_path_http" type="text" size="25"
					name="storage_path_http" value="<?= $http_value; ?>" />
			</div>
		</div>
		<div class="control-group" id="document-file-path-row" style="display: none">
			<label class="control-label"><?= @text('File path'); ?></label>
			<div class="controls">
				<div class="input-append">
				<?= @helper('modal.select', array(
					'name'  => 'storage_path_file',
				    'id'  => 'storage_path_file',
					'value' => $file_value,
					'link'  => JRoute::_('index.php?option=com_docman&view=files&layout=select&tmpl=component&Itemid='.$state->page),
					'link_selector' => 'modal-button',
					'attribs' => array('class' => 'validate-storage', 'data-type' => 'file')
				)) ?>
				</div>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend><?= @text('Description'); ?></legend>
		<?= @editor(array(
			'name'    => 'description',
		    'id'      => 'description',
			'width'   => '100%', 'height' => '200',
			'rows'    => '20'
		)); ?>
	</fieldset>
	<fieldset class="form-horizontal">
		<legend><?= @text('Metadata options'); ?></legend>
        <div class="control-group">
            <label class="control-label"><?= @text('Image'); ?></label>
            <div class="controls">
                <?= @helper('behavior.thumbnail', array(
                'automatic_thumbnail' => $document->image === 'generated/'.md5($document->id).'.png',
                'automatic_thumbnail_image' => 'generated/'.md5($document->id).'.png',
                'value' => $document->image,
                'name'  => 'image',
                'id'  => 'image',
            )) ?>
            </div>
        </div>
		<div class="control-group">
			<label class="control-label"><?= @text('Icon'); ?></label>
			<div class="controls">
				<?= @helper('modal.icon', array(
					'name'  => 'params[icon]',
				    'id' => 'params_icon',
					'value' => $document->params->icon ? $document->params->icon : 'default.png',
					'link'  => @route('index.php?option=com_docman&view=files&layout=select_icon&tmpl=component&container=docman-icons&types[]=image'),
					'link_selector' => 'modal-button'
				))?>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="name"><?= @text('Alias') ?></label>
			<div class="controls">
				<input type="text" name="slug" size="32" maxlength="255" value="<?= $document->slug ?>" />
			</div>
		</div>	
		<? if ($document->canPerform('manage')): ?>
		<div class="control-group">
		    <label class="control-label" for="enabled"><?= @text('Status'); ?></label>
        	<div class="controls">
        	    <?= @helper('select.booleanlist', array(
	        	    	'name' => 'enabled', 
	        	    	'selected' => $document->enabled,
	        	    	'true' => 'Published',
	        	    	'false' => 'Unpublished'
	        	    )); ?>
        	</div>
		</div>
		<? endif; ?>
		<? if (!$document->isNew()): ?>
		<div class="control-group">
			<label class="control-label"><?= @text('Created by'); ?></label>
			<div class="controls">
				<span class="help-info">
				<?= JFactory::getUser($document->created_by)->name; ?>
				<?= @text('on') ?>
				<?= @date(array('date' => $document->created_on)); ?>
				</span>
			</div>
		</div>
		<? if ($document->modified_by): ?>
		<div class="control-group">
			<label class="control-label"><?= @text('Modified by'); ?></label>
			<div class="controls">
				<span class="help-info">
				<?= JFactory::getUser($document->modified_by)->name; ?>
				<?= @text('on') ?>
				<?= @date(array('date' => $document->modified_on)); ?>
				</span>
			</div>
		</div>
		<? endif; ?>
		<? endif; ?>
		
	</fieldset>
</div>

</form>