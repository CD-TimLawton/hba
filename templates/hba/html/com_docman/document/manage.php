<?
/**
 * @package     DOCman
 * @copyright   Copyright (C) 2012 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */
defined('KOOWA') or die; ?>

<?
$display_download = (!isset($downloadable) || $downloadable !== false) && ($document->canPerform('download') || $login_to_download);
$display_actions  = !isset($editable) || $editable !== false;
$display_actions  = $display_actions && ($document->canPerform('edit') || $document->canPerform('delete'));
$type  = ($document->isPreviewable() && $document->canPreview()) ? 'view' : 'download';
$token = version_compare(JVERSION, '3.0', 'ge') ? JSession::getFormToken() : JUtility::getToken(); 
?>

<? if ($display_download || $display_actions): ?> 
<div class="btn-toolbar">
    <? if ($display_download): ?>
	    <div class="btn-group docman-btn-group-download">
	        <a class="btn btn-mini docman-download" 
	        	data-title="<?= @escape($document->title); ?>" 
	        	data-id="<?= $document->id; ?>" 
	        	href="<?= $document->download_link; ?>"
                target="_blank">
	        	
	        	<!--<i class="icon-download"></i>-->
	        	<?= @text('Download'); ?>
	        </a>
	    </div>
    <? endif ?>
    
    <? if ($display_actions && ($document->isLockable() && $document->locked())): ?>
        <div class="btn btn-warning btn-mini">
            <i class="icon-lock icon-white"></i>
            <?= $document->lockMessage(); ?>
        </div>
    <? elseif ($display_actions): ?>
        <div class="btn-group">
    
        <? if ($document->canPerform('edit')): ?>
        <a class="btn btn-mini" href="<?= @route($document, 'layout=form');?>"><i class="icon-pencil"></i> <?= @text('Edit'); ?></a>
        <? endif ?>

        <? if ($document->canPerform('delete')):
            $data = sprintf("{method:'post', url:'%s', params:{_token:'%s', action:'delete'}}", @route($document), $token);
        ?>
        <!-- <script src="media://lib_koowa/js/koowa.js" />  -->
        <?= @helper('behavior.mootools'); ?>
        <?= @helper('behavior.deletable'); ?>
        
        <a class="btn btn-mini btn-danger docman-deletable" href="#" rel="<?= $data ?>">
            <i class="icon-trash icon-white"></i> <?= @text('Delete') ?>
        </a>
        <? endif ?>
    </div>
    <? endif ?>
</div>
<? endif ?>
