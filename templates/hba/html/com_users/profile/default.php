<?php
/**
 * @package        Joomla.Site
 * @subpackage    com_users
 * @copyright    Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license        GNU General Public License version 2 or later; see LICENSE.txt
 * @since        1.6
 */

defined('_JEXEC') or die;
JHtml::_('behavior.tooltip');
?>
<div class="profile<?php echo $this->pageclass_sfx?>">
<h1>
    Welcome to the Members Area of The Hypnobirthing Association
</h1>

<p>In this private area of the website you will find the following resources:</p>
<ul>
<li><a href="/member-options/member-downloads.html" title="Member Downloads">Downloads</a> - all your scripts and course notes are available here to be downloaded in electronic format.</li>
<li><a href="/forum.html" title="Member Forum">Forum</a> - need help or support, or would like to share your experiences here?  Peer support and guidance from the experts here.</li>
<li><a href="/member-options/shop.html" title="Member Shop">Shop</a> - CD's to support your courses are available at a discounted rate here</li>
</ul>

<p><strong>Your Hypnobirthing Teacher Listing</strong></p>
<ul>
<li>Directory listing guide - information on setting up and managing your listing.</li>
<li>Add your directory listing - create your directory listing.</li>
<li>To Edit your Existing select from the menu option under the Members Drop Down or view your page. If you are logged in to the site you will see an Edit button there.</li>
</ul>

<p><strong>Great News for Hypnobirthing Teachers, Mothers and Babies!</strong></p>

<p>It has been established by the UK Intellectual Property Office on absolute grounds that the word 'hypnobirthing' cannot be trademarked in this country and we can all use it freely. Now we are confident that Hypnobirthing can grow and prosper to the benefit of mothers and babies. Even better! The trademark authorities in the EU which covers all 27 states of Europe have also decided on final ground that 'hypnobirthing' cannot be trademarked, though there is still one minor point to be decided.</p>
<p>Unfortunately a false impression has been created and concern caused by a circular that was sent round claiming that the word has been registered in the EU.</p>
<p>The following will reassure you that this is not the case: You may also be interested to know that the word can be used freely in Canada because four separate applications to Trademark the word there were abandoned, presumably on the advise that they would be rejected.</p>

<p><a href="/index.php?option=com_content&view=article&id=101">Click here to read more</a></p>

<?php //echo $this->loadTemplate('core'); ?>

<?php //echo $this->loadTemplate('params'); ?>

<?php //echo $this->loadTemplate('custom'); ?>

<?php if (JFactory::getUser()->id == $this->data->id) : ?>
<!--<a href="<?php //echo JRoute::_('index.php?option=com_users&task=profile.edit&user_id='.(int) $this->data->id);?>">
    <?php //echo JText::_('COM_USERS_Edit_Profile'); ?></a>-->
<?php endif; ?>
</div>
