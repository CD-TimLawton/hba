<?php

/**
 *
 * Show Confirmation message from Offlien Payment
 *
 * @package	VirtueMart
 * @subpackage
 * @author Valerie Isaksen
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: default.php 3217 2011-05-12 15:51:19Z alatak $
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.modal');

if ($this->paymentResponseHtml) {

	echo "<h3>" . $this->paymentResponse . "</h3>";

	$position = strpos($this->paymentResponseHtml,'Order Number');

    if ($position !== false) {

    	$order_num  = '';

    	if (!empty($_GET['on'])) {
    		$order_num = $_GET['on'];
    	} else {
	    	$order_num = trim(strip_tags(substr($this->paymentResponseHtml, ($position + 12))));
    	}

	    echo '<p>Your payment was successful.</p>';

	    echo '<p><a href="/index.php?option=com_virtuemart&lang=en&layout=details&order_number=' . $order_num . '&view=orders" title="View order details">Click here to view order details</a></p>';

	} else {
		echo '<p>Your payment was unsuccessful. Please try again.</p>';
	}

} else {

	echo "<h3>" . $this->paymentResponse . "</h3>";
	echo '<p>' . $this->paymentResponseHtml . '</p>';

}