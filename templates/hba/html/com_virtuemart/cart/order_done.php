<?php
defined('_JEXEC') or die('');

/**
*
* Template for the shopping cart
*
* @package	VirtueMart
* @subpackage Cart
* @author Max Milbers
*
* @link http://www.virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
*/

// LB Channel - Original code
// ****************************
//echo "<img src='/images/preloader.gif' align='left' height='100' width='100' alt='Preloading Symbol' style='margin:30px 20px 0;' />";
//echo "<h3>".JText::_('COM_VIRTUEMART_CART_ORDERDONE_THANK_YOU')."</h3><p>".JText::_('COM_VIRTUEMART_CART_ORDERDONE_COPY')."</p>";
//echo $this->html;
//echo "<br/><br/>";
// ****************************


// ****************************
// Show different message if order was for free products
// ****************************
$string = $_SESSION['__vm']['vmcart'];
$pattern = "/<span class=\"vmpayment_name\">(.*?)<\/span>/";
preg_match($pattern, $string, $matches);
$matches[1];

if ($matches[1] === 'Free products') {

  echo "<h3>" . JText::_('COM_VIRTUEMART_CART_ORDERDONE_THANK_YOU') . "</h3>";
  echo '<p>Thank you for you order, we hope you will enjoy your free items from the Hypnobirthing Association.</p><p>Your order has been processed successfully and you should recieve an email shortly containing your order details.</p><p><a href="/" title="Return to the home page">Click here</a> to return to the home page or <a href="index.php?option=com_content&view=article&id=27" title="Go ">click here</a> to go to the members dashboard.</p>';

} else {
  echo "<img src='/images/preloader.gif' align='left' height='100' width='100' alt='Preloading Symbol' style='margin:30px 20px 0;' />";
  echo "<h3>" . JText::_('COM_VIRTUEMART_CART_ORDERDONE_THANK_YOU') . "</h3>";
  echo "<p>" . JText::_('COM_VIRTUEMART_CART_ORDERDONE_COPY') . "</p>";
  echo $this->html;
}


echo "<br/><br/>";