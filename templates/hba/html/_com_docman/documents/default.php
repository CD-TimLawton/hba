<?
/**
 * @package     DOCman
 * @copyright   Copyright (C) 2012 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */
defined('KOOWA') or die; ?>

<?= @helper('behavior.bootstrap'); ?>
<?= @helper('behavior.mootools');?>

<!--
<script src="media://lib_koowa/js/koowa.js" />
-->

<form action="<?= @route()?>" method="get" class="-koowa-grid">	
	<?= @template('com://site/docman.view.documents.list', array('documents' => $documents, 'params' => $params))?>

	<? if ($params->show_pagination !== '0' && $total): ?>
		<?= @helper('paginator.pagination', array_merge(array('total' => $total), $state->getData())) ?>
	<? endif; ?>
</form>