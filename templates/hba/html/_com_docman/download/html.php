<?php
/**
 * @package    DOCman
 * @copyright   Copyright (C) 2011 Timble CVBA and Contributors. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.eu
 */

class ComDocmanViewDownloadHtml extends KViewFile
{
    public function display()
    {
        $document = $this->getModel()->getItem();

        if (!$document->id) {
            throw new KViewException('Document not found');
        }

        if ($document->isPreviewable() && $document->canPreview() && !KRequest::get('get.force_download', 'int')) {
            $this->disposition = 'inline';
        }

        $file = $document->storage;

        $mimetype = $file->mimetype;

        if (empty($this->mimetype)) {
            $row = $this->getService('com://admin/docman.model.mimetypes')
                ->extension($file->extension)
                ->getList()->top();
            $mimetype = $row ? $row->mimetype : 'application/octet-stream';
        }

        $this->filename = $file->path;
        $this->path = $file->fullpath;
        $this->mimetype = $mimetype;

        if (!file_exists($this->path)) {
            throw new KViewException('File not found');
        }
        
        return parent::display();
    }      
    
    protected function _setDisposition()
    {
        //if (isset($this->disposition) && $this->disposition == 'inline') {
        //        header('Content-Disposition: inline; filename="'.$this->filename.'"');
        //} else {
           header("Content-Type: application/force-download");
           header("Content-Type: application/octet-stream");
           header("Content-Type: application/download");
           header("Content-Description: File Transfer");
           header('Content-Disposition: attachment; filename="'.$this->filename.'"');
        //header('Content-Disposition: inline; filename="'.$this->filename.'"');
        //}
        
        return $this;
    }
}
