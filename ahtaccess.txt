##
# @package		Joomla
# @copyright	Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.
# @license		GNU General Public License version 2 or later; see LICENSE.txt
##

##
# READ THIS COMPLETELY IF YOU CHOOSE TO USE THIS FILE!
#
# The line just below this section: 'Options +FollowSymLinks' may cause problems
# with some server configurations.  It is required for use of mod_rewrite, but may already
# be set by your server administrator in a way that dissallows changing it in
# your .htaccess file.  If using it causes your server to error out, comment it out (add # to
# beginning of line), reload your site in your browser and test your sef url's.  If they work,
# it has been set by your server administrator and you do not need it set here.
##

## Can be commented out if causes errors, see notes above.
Options +FollowSymLinks

## Mod_rewrite in use.

RewriteEngine On

## Begin - Rewrite rules to block out some common exploits.
# If you experience problems on your site block out the operations listed below
# This attempts to block the most common type of exploit `attempts` to Joomla!
#
# Block out any script trying to base64_encode data within the URL.
RewriteCond %{QUERY_STRING} base64_encode[^(]*\([^)]*\) [OR]
# Block out any script that includes a <script> tag in URL.
RewriteCond %{QUERY_STRING} (<|%3C)([^s]*s)+cript.*(>|%3E) [NC,OR]
# Block out any script trying to set a PHP GLOBALS variable via URL.
RewriteCond %{QUERY_STRING} GLOBALS(=|\[|\%[0-9A-Z]{0,2}) [OR]
# Block out any script trying to modify a _REQUEST variable via URL.
RewriteCond %{QUERY_STRING} _REQUEST(=|\[|\%[0-9A-Z]{0,2})
# Return 403 Forbidden header and show the content of the root homepage
RewriteRule .* index.php [F]
#
## End - Rewrite rules to block out some common exploits.

## Begin - Custom redirects
#
# If you need to redirect some pages, or set a canonical non-www to
# www redirect (or vice versa), place that code here. Ensure those
# redirects use the correct RewriteRule syntax and the [R=301,L] flags.
#

# Added by LB Channel - 04.03.2014
redirect 301 /registration-area.html http://www.thehypnobirthingassociation.com/membership/join-the-hypnobirthing-association.html
redirect 301 /birth-stories/ http://www.thehypnobirthingassociation.com/information/birth-stories/
redirect 301 /hypnobirthing-videos/hypnobirthing-videos.html http://www.thehypnobirthingassociation.com/about-us/hypnobirthing-videos.html
redirect 301 /hypnobirthing-links/childbirth-links/ http://www.thehypnobirthingassociation.com/
redirect 301 /index.php?option=com_docman&Itemid=25 http://www.thehypnobirthingassociation.com/members-area/member-downloads
redirect 301 /hypnobirthing-teachers.html http://www.thehypnobirthingassociation.com/find-your-teacher/uk-hypnobirthing-teachers
redirect 301 /contact-us/the-hypnobirthing-association/ http://www.thehypnobirthingassociation.com/component/com_chronoforms/Itemid,107/view,form/
redirect 301 /hypnobirthing-videos/hypnobirthing-techniques-is-it-powerful-or-painful.html http://www.thehypnobirthingassociation.com/about-us/hypnobirthing-videos.html
redirect 301 /contact/the-hypnobirthing-association.html http://www.thehypnobirthingassociation.com/component/com_chronoforms/Itemid,107/view,form/
redirect 301 /hypnobirthing-links/ http://www.thehypnobirthingassociation.com/
redirect 301 /testimonials/ http://www.thehypnobirthingassociation.com/information/testimonials/
redirect 301 /hypnobirthing-course-notes/view-category/Page-2.html http://www.thehypnobirthingassociation.com/
redirect 301 /finding-your-hypnobirthing-teacher.html http://www.thehypnobirthingassociation.com/find-your-teacher/uk-hypnobirthing-teachers
redirect 301 /hypnobirthing-videos/an-introduction-to-hypnobirthing.html http://www.thehypnobirthingassociation.com/about-us/hypnobirthing-videos.html

redirect 301 /hypnobirthing-association-a5-leaflets-detail http://www.thehypnobirthingassociation.com/50-a5-leaflets-detail

redirect 301 /find-your-teacher/world-hypnobirthing-teachers.html http://www.kghypnobirthing.com/hypnobirthing-teachers/rest-of-the-world.html

## End - Custom redirects

##
# Uncomment following line if your webserver's URL
# is not directly related to physical file paths.
# Update Your Joomla! Directory (just / for root).
##
# RewriteBase /

## Begin - Joomla! core SEF Section.
#
RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
#
# If the requested path and file is not /index.php and the request
# has not already been internally rewritten to the index.php script
RewriteCond %{REQUEST_URI} !^/index\.php
# and the request is for something within the component folder,
# or for the site root, or for an extensionless URL, or the
# requested URL ends with one of the listed extensions
RewriteCond %{REQUEST_URI} /component/|(/[^.]*|\.(php|html?|feed|pdf|vcf|raw))$ [NC]
# and the requested path and file doesn't directly match a physical file
RewriteCond %{REQUEST_FILENAME} !-f
# and the requested path and file doesn't directly match a physical folder
RewriteCond %{REQUEST_FILENAME} !-d
# internally rewrite the request to the index.php script
RewriteRule .* index.php [L]
#
## End - Joomla! core SEF Section.

#AuthType Basic
#AuthName "hbaprotected"
#AuthUserFile "/home/hbachann/.htpasswds/public_html/passwd"
#require valid-user