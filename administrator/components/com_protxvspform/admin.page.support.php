<?php
// *************************************************************************
// * admin.page.support.php                                                *
// * Code Word: Srednekolymsk                                              *
// *************************************************************************
?>
<?php  
?>
<h2>Support Options</h2>
<table width="99%" style="background-color:#FFFFFF;border:1px solid #CCCCCC;" >
  <tr valign="top"> 
    <td width="80"> <a href="<?php echo $payment_method->THIS_SUPPORT_WEBSITE; ?>/kb" target="_blank"><img 
	  style="padding:8px;"
	  src="<?php echo  JURI::root( false ); ?>components/<?php echo $payment_method->THIS_OPTION; ?>/knowledgebase.gif" width="60" height="60" align="left" border="0" ></a></td>
    <td> 
      <h3>FAQ/Knowledgebase </h3>
      <p>Free support is available via a regularly updated <a href="<?php echo $payment_method->THIS_SUPPORT_WEBSITE; ?>/kb" target="_blank">FAQ/Knowledgebase</a> 
        which includes Installation Guides, answers to common issues and and articles 
        on using the component. Try the following categories. </p>
      <p><a href="<?php echo $payment_method->THIS_SUPPORT_WEBSITE; ?>/kb?c=10" target="_blank">Common 
        Setup Problems</a> <br>

<?php if( $payment_method->THIS_LICENCE != 'GPL') { ?>		
        <a href="<?php echo $payment_method->THIS_SUPPORT_WEBSITE; ?>/kb?c=15" target="_blank">Licence 
        Issues</a> <br>
<?php } // GPL ?>  
		
        <a href="<?php echo $help_url;?>"  target="_blank"> 
        FAQ: <?php echo $payment_method->THIS_PRODUCT; ?></a> 
    </td>
  </tr>
</table>
<br>
<table width="99%" style="background-color:#FFFFFF;border:1px solid #CCCCCC;" >
  <tr valign="top"> 
    <td width="80"><img 
	  style="padding:8px;"
	  src="<?php echo JURI::root( false ); ?>components/<?php echo $payment_method->THIS_OPTION; ?>/submitticket.gif"
	  width="60" height="60" align="left" > </td>
    <td> 
      <h3>Technical Support</h3>
      <p> Technical support via <a href="<?php echo $payment_method->THIS_SUPPORT_WEBSITE; ?>">Support 
        Ticket</a> system is included with all purchases for 6 months, and can 
        be extended for longer if required. To take advantage of technical support, 
        you must be using the <a href="index2.php?task=updates&option=<?php echo $payment_method->THIS_OPTION; ?>">latest 
        version</a> of the component. </p>
      <p><a href="http://www.virtuemart-solutions.com/sp6m" target="_blank">Purchase 
        Extended Technical Support</a> 
      <p>Please first read <a target="_blank" 
		href="<?php echo $payment_method->THIS_SUPPORT_WEBSITE; ?>/kb?108" id="articlelink">Before 
        submitting a Support Ticket</a> 
      <p>
<?php support_form("Create Support Ticket", "Support Request", $payment_method); ?>
    </td>
  </tr>
</table>
<br>

<?php if(COMP_IS_PAYMENT) { ?> 
<table width="99%" style="background-color:#FFFFFF;border:1px solid #CCCCCC;" >
  <tr valign="top"> 
    <td width="80"> <a href="<?php echo $payment_method->THIS_SUPPORT_WEBSITE; ?>/kb" target="_blank"><img 
	  style="padding:8px;"
	  src="<?php echo  JURI::root( false ); ?>components/<?php echo $payment_method->THIS_OPTION; ?>/knowledgebase.gif" width="60" height="60" align="left" border="0" ></a></td>
    <td> 
      <h3>
	
<img align="right" style="margin-right:6px;"
src="<?php echo JURI::root( false ) ?>components/<?php echo $payment_method->THIS_OPTION; ?>/logo.gif" 
 border="0" vspace="4" hspace="8" > 
 Payment Provider </h3>
 
 
      <p>You can contact your payment provider directly for <br>
        support issues such as how to get your payment <br>
        provider credentials, and other queries relating to your merchant account, 
      </p>
      <p> 
        <?php include('admin.panel.provider_urls.php'); ?>
    </td>
  </tr>
</table>
<br>
<?php } // COMP_IS_PAYMENT ?>  


<table width="99%" style="background-color:#FFFFFF;border:1px solid #CCCCCC;" >
  <tr valign="top"> 
    <td width="80"><img 
	  style="padding:8px;"
	  src="<?php echo JURI::root( false ); ?>components/<?php echo $payment_method->THIS_OPTION; ?>/bugs.gif" align="left" > 
    </td>
    <td> 
      <h3>Submit Bug Report</h3>
      <p> To submit a bug report to technical support please click the button 
        below </p>
      <p> 
        <?php support_form("Submit", "Bug Report", $payment_method ); ?>
    </td>
  </tr>
</table>
<?php  
function support_form($btn, $subj, $pm) {
	$user = &JFactory::getUser(); 
?>
<form name="submitticket" method="post"  target="_blank"
action="<?php echo $pm->THIS_SUPPORT_WEBSITE; ?>/index.php">

        <input type="hidden" name="subject" value="<?php echo $subj; ?>">
        <input type="hidden" name="fullname" value="<?php echo $user->name; ?>">
        <input type="hidden" name="email" value="<?php echo $user->email; ?>">
        <input type="hidden" name="889f2f84ad" value="<?php echo $user->username; ?>">
        <input type="hidden" name="5adc6c37e9" value="<?php echo JURI::root( false ) ; ?>">
        <input type="hidden" name="56c422af7b" value="<?php echo $pm->THIS_PRODUCT; ?>">
        <input name="submit" type="submit"  value="<?php echo $btn; ?>">
        <input type="hidden" name="departmentid" id="departmentid[1]" value="1">
        <input type="hidden" name="_m" value="tickets">
        <input type="hidden" name="_a" value="submit">
        <input type="hidden" name="step" value="1">
      </form> 

<?php 
}

?>