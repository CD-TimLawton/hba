<?php
// *************************************************************************
// * Filename: admin.page.test.php                                         *
// * Code Word: Srednekolymsk                                              *
// *************************************************************************
?> 
<h2>Testing</h2>
<p>You can test the Sage Pay installation by using the <b>Simulator</b> and <b>Test</b> 
  modes.</p>
<h4>Simulator Mode</h4>
<p>Simulator mode simulates all possible responses from the real systems. The 
  component settings initially contain our Simulator details which can be used 
  for testing in Simulator mode. The Simulator can be configured on the following 
  URL:<br>
  <a href="https://test.sagepay.com/simulator" target="_blank">https://test.sagepay.com/simulator</a></p>
<h4>Test Mode</h4>
<p>You need to enter your merchant details in the <a 
href="index2.php?option=<?php echo $payment_method->THIS_OPTION; ?>&amp;task=edit">Payment 
  Method Configuration</a> to use test mode. </p>
<p>These test card numbers can be used in Test mode mode with security code 123</p>
<p>Visa (VISA)<br>
  4929000000006</p>
<p>MasterCard (MC)<br>
  5404000000000001</p>
<p>Visa Debit / Delta (DELTA)<br>
  4462000000000003</p>
<p>Solo (SOLO)<br>
  6334900000000005<br>
  Issue 1 </p>
<p>UK Maestro / International Maestro (MAESTRO)<br>
  5641820000000005<br>
  Issue 01</p>
<p>American Express (AMEX)<br>
  374200000000004</p>
<p>Visa Electron (UKE)<br>
  4917300000000008</p>
<p>JCB (JCB)<br>
  3569990000000009</p>
