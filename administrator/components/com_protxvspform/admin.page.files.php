<?php
// *************************************************************************
// * admin.page.files.php                                                  *
// * Code Word: Srednekolymsk                                              *
// *************************************************************************
?> 
<h2>Files and Folders</h2>
<p>Files with a red cross symbol may not have been copied properly from the installation 
  folder during installation and should be uploaded again from the manual installation 
  folder in the product download zip file.</p>
<p> 
  <?php  
	
	$extrafiles = getInstallXMLFiles($payment_method, 'files', 'filename');			   
	foreach ($extrafiles as $k=>$v) {				  
		$possfile1 =  DS . 'components' . DS . $payment_method->THIS_OPTION . DS . $k ;
		$possfile2 =  DS . 'administrator' . DS . 'components' . DS . $payment_method->THIS_OPTION . DS . $k ;
		if(!file_exists(JPATH_SITE . $possfile1)) $possfile1 = $possfile2;
		
		echo InstallCheckFile($payment_method, $possfile1 ). "<br />";								
	} 
				  
	//$extrafiles = getExtraFiles($payment_method);				  
	$extrafiles = getInstallXMLFiles($payment_method, 'extrafolder', 'extrafilename');			   
	foreach ($extrafiles as $k=>$v) {			  
		echo InstallCheckFile($payment_method, DS . $v . DS . $k ) . "<br />";	
	} 

?>
</p>
<p>

