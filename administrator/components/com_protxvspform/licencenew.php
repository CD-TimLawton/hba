<?php 
// *************************************************************************
// *                                                                       *
// * Ecom Solution Licence Manager                                         *
// * Copyright (c) 2008 Ecom Solution LTD. All Rights Reserved             *
// * Email: support@virtuemart-solutions.com                               *
// * Website: http://www.virtuemart-solutions.com                          *
// * Release Date: 2010.07.03                                              *
// * Build Time: 3 July 2010 20:00                                         *
// * Version 1.5 for Joomla 1.5                                            *
// * Code Word: Srednekolymsk                                              *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * This software is furnished under a license and may be used and copied *
// * only  in  accordance  with  the  terms  of such  license and with the *
// * inclusion of the above copyright notice.  This software  or any other *
// * copies thereof may not be provided or otherwise made available to any *
// * other person.  No title to and  ownership of the  software is  hereby *
// * transferred.                                                          *
// *                                                                       *
// * You may not reverse  engineer, decompile, defeat  license  encryption *
// * mechanisms, or  disassemble this software product or software product *
// * license.  Ecom Solution  may  terminate  this license  if you fail to *
// * comply with any of the terms and conditions set forth in our end user *
// * license agreement (EULA).  In such event,  licensee  agrees to return *
// * licensor  or destroy  all copies of software  upon termination of the *
// * license.                                                              *
// *                                                                       *
// *************************************************************************
// 

error_reporting(E_ALL);

$my_cfg_path = dirname(__FILE__)."/../../../configuration.php";
require_once($my_cfg_path);	
$mosConfig_absolute_path = dirname($my_cfg_path);

define( '_JEXEC', 1 );
define('JPATH_BASE', realpath($mosConfig_absolute_path));
define( 'DS', DIRECTORY_SEPARATOR );
// Load the framework
require_once ( JPATH_BASE . DS . 'includes' . DS . 'defines.php' );
require_once ( JPATH_BASE . DS . 'includes' . DS . 'framework.php' );
// create the mainframe object
$mainframe = & JFactory::getApplication('administrator');
$mainframe->initialise(array(
	'language' => $mainframe->getUserState( "application.lang", 'lang' )
));
JPluginHelper::importPlugin('system');
$mainframe->render();

$user = & JFactory::getUser();
if (!$user->authorize('com_components', 'manage')) {
	$mainframe->redirect('../../index.php', JText::_('ALERTNOTAUTH'));
}

	
// include licencecheck 
require_once('licencecheck.e.php');

// want to release?
if (get_req('Release')=='Release') $licence->EL_set_new(0);

// want to enter key?
$new_key = get_req('licence_key');
if ($new_key != '') $licence->EL_set_new($new_key);

//get info into local vars
$mylic_status = $licence->EL_status;
$mylic_statusdesc = $licence->EL_description;
$mylic_info = $licence->EL_get_info(); 

function get_req($var){
  $val = '';
  if (isset($_REQUEST[$var])) {
     $val = $_REQUEST[$var];
   }
  return trim($val);
}
 
?>
<html>
<head>
<title>Ecom Solutions Licence Manager</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
body,p,td,th {  
  font-family: Verdana, Arial, Helvetica, sans-serif; 
  font-size: 11px
}
-->
</style>

</head>
<body bgcolor="#FFFFFF" text="#000000"> 

  
<p> <img src="../../../components/<?php echo get_req('com'); ?>/ecom-solution.gif" align="right" hspace="2" alt="Ecom Solutions"> 
  <b><font size="+1"> Licence Manager</font> <br>
    Version 1.5 for Joomla 1.5</b> 	
	
	
<p> 

<table width="100%" border="0" cellspacing="0" cellpadding="4" bgcolor="#EEEEEE">
<form>
    <tr valign="top">
      <td width="120"><b>Product</b></td>
      <td> 
        <?php echo $mylic_info['prod'] ?>      </td>
      <td>&nbsp;</td>
    </tr>
    <tr valign="top">
      <td width="120"><b>Host</b></td>
      <td> 
        <?php echo $mylic_info['host'] ?>      </td>
      <td>&nbsp;</td>
    </tr>
    <tr valign="top">
      <td width="120" valign="middle"><b>Status</b></td>
      <td valign="middle"> 
        <?php  if ($licence->EL_need_input()) { ?>
        <img src="../../images/publish_x.png" width="16" height="16" align="absmiddle">
        <?php } else { ?>
        <img src="../../images/tick.png" width="16" height="16" align="absmiddle"> 
        <?php } ?>
        
        <?php echo $mylic_statusdesc  ?>      </td>
      <td>&nbsp;</td>
    </tr>
    <tr valign="top">
      <td width="120" height="36"><b>Licence Key</b></td>
      <td height="36"> 
        <?php echo $mylic_info['key'] ?>
        <?php  if ($licence->EL_need_input()) { ?>
        <input type="text" name="licence_key" maxlength="64" size="40" >
        <?php  } ?>      </td>
      <td>&nbsp;</td>
    </tr>
    
    
    <tr valign="top">
      <td width="120">&nbsp;</td>
      <td> 
        <input type="submit" name="Submit2" value="Close" onClick="window.close();">
        <input type="submit" name="Submit" value="Update">
        <input type="hidden" name="com" value="<?php echo get_req('com'); ?>">


        <?php  if ($licence->EL_need_input()) { ?>
        <?php  } else {?>
        <input type="checkbox" name="Release" value="Release">
        Release Licence 
        <?php } ?>      </td>
      <td align="right"><a href="http://support.virtuemart-solutions.com/kb?c=15" target="_blank">Support/FAQ<img
		  src="../../images/help_f2.png" 
		  width="32" height="32" hspace="2" border="0" align="absMiddle"
	      title="Help"></a></td>
    </tr>
    <tr valign="top">
      <td width="120" height="6"></td>
      <td height="6"></td>
      <td></td>
    </tr>

</form>
  </table>  
  
<br>

Copyright &copy; 2008 Ecom Solutions Ltd. All rights reserved. 


<p>
<br>
<br>
<br>
<br>
<br>

<table width="100%" border="0" cellspacing="0" cellpadding="4" bgcolor="#EEEEEE">
    

    <tr valign="top">
      <td width="120"><b>Cache Period</b></td>
      <td> 
        <?php echo $mylic_info['period']; ?>s  </td>
      <td>&nbsp;</td>
    </tr>
    <tr valign="top">
      <td width="120"><b>Last check</b></td>
      <td> 
        <?php echo $mylic_info['saved']; ?>      </td>
      <td width="50">&nbsp;</td>
    </tr>
    
    <tr valign="top">
      <td width="120"><b>Next Check</b></td>
      <td> 
        <?php echo $mylic_info['cache_expires']; ?>      </td>
      <td>&nbsp;</td>
    </tr>

    <tr valign="top">
      <td width="120"><b>Latest Version</b></td>
      <td> 
        <?php echo $mylic_info['vsn']; ?> </td>
      <td>&nbsp;</td>
    </tr>
    
    <tr valign="top">
      <td width="120"><b>Support Period</b></td>
      <td> 
        <?php echo $mylic_info['lic_expires']; ?>      </td>
      <td>&nbsp;</td>
    </tr>
  </table>  

</body>
</html>
