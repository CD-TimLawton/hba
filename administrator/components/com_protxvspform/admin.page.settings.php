<?php
// *************************************************************************
// * admin.page.settings.php                                               *
// * Code Word: Srednekolymsk                                              *
// *************************************************************************


global $licence;   
if($payment_method->THIS_LICENCE != 'GPL') {
  require_once(JPATH_BASE . DS . 'components' . DS . $payment_method->THIS_OPTION . DS . 'licencecheck.e.php');    
}	

$JCfg = new JConfig();
$cachepath = $JCfg->tmp_path;
$filename = "settings_" . $payment_method->THIS_OPTION . "_" . time(). ".txt";
	
$option= JRequest::getCmd('option'); 
$task = JRequest::getCmd('task'); 
switch($task){
	
  case 'import':
	  
	//Retrieve file details from uploaded file, sent from upload form
	$file = JRequest::getVar('import_settings', null, 'files', 'array');
 
	if ($file['tmp_name'] == '') {
		 //Redirect and throw an error message 
		 $msg = 'No file uploaded ';
  		 $mainframe->enqueueMessage($msg, 'error'); 
    	 //$mainframe->redirect( 'index.php?task=settings&option='. $option, $msg );
	   }

	//Import filesystem libraries. Perhaps not necessary, but does not hurt
	jimport('joomla.filesystem.file');
	
	//Clean up filename to get rid of strange characters like spaces etc
	$txtfile = JFile::makeSafe($file['name']);
	
	//Set up the source and destination of the file
	$src = $file['tmp_name'];
	$dest = $cachepath . DS . $txtfile;

	//First check if the file has the right extension, we need txt only
	if ( strtolower(JFile::getExt( $file['name']) ) == 'txt') {

	   if ( JFile::upload($src, $dest) ) {
	       // process the file
		   import_settings($dest, $payment_method);
	   } else {
		  //Redirect and throw an error message 
		   $msg = 'Error uploading: ' . $file['name'];
		   
  		   $mainframe->enqueueMessage($msg, 'error');
    	   //$mainframe->redirect( 'index.php?task=settings&option='. $option, $msg );
	   }

	} else {
	   //Redirect and notify user file is not right extension 
		   $msg = 'Wrong extension: ' . $file['name'];
		   
  		   $mainframe->enqueueMessage($msg, 'error');
    	   //$mainframe->redirect( 'index.php?task=settings&option='. $option, $msg );
	}
 	break;	

  case 'export':
   
	$cfg_str = "\r\n" . trim(create_settings_str($payment_method)) . "\r\n";

	header("Content-type: application/force-download"); 
	header('Content-Type: application/octet-stream');
	header('Content-Disposition: inline; filename="'. $filename .'"'); 
	header("Content-Transfer-Encoding: Binary"); 
	header("Content-length: ".strlen($cfg_str)); 
	header('Content-Disposition: attachment; filename="'. $filename .'"'); 
	 
	echo $cfg_str;
	exit;

  case 'settings': 
      // html below
}

function import_settings($dest, $payment_method) {
  global $mainframe;

  if (!JFile::exists( $dest )) { 
    $msg = 'File does not exist: ' . $dest;
	
  	$mainframe->enqueueMessage($msg, 'error');
    //$mainframe->redirect( 'index.php?task=settings&option='. $payment_method->THIS_OPTION, $msg );
    return;
  }
  
  $cfg_fz = trim( JFile::read( $dest ) );
  $cfg_ary = explode( "\r\n", $cfg_fz);  
 
  if(count($cfg_ary) == 0) {
    $msg = 'Error parsing settings (1)';
	
  	$mainframe->enqueueMessage($msg, 'error');
    //$mainframe->redirect( 'index.php?task=settings&option='. $payment_method->THIS_OPTION, $msg );
    return;
  }

  if(trim($cfg_ary[0]) != "[". $payment_method->THIS_OPTION."]") {
    $msg = 'Settings not for ' . $payment_method->THIS_OPTION;
	
  	$mainframe->enqueueMessage($msg, 'error');
    //$mainframe->redirect( 'index.php?task=settings&option='. $payment_method->THIS_OPTION, $msg );
    return;
  }

  $cfg_ini = settings_parseContent($cfg_ary);  
  $cfg = $cfg_ini['Settings'];  

  if(!is_array($cfg)) {
    $msg = 'Error parsing settings (2)';
	    
  	$mainframe->enqueueMessage($msg, 'error');
    //$mainframe->redirect( 'index.php?task=settings&option='. $payment_method->THIS_OPTION, $msg );
    return;
  }
  
  $msg = 'Settings imported. ';
  
  // set the licence  
  if($payment_method->THIS_LICENCE != 'GPL') {
		// include licence 
 	    global $licence;   
		$licence->EL_set_new($cfg['LicenceKey']);
		$msg .= " - Licence Status: " . $licence->EL_description;
		
		unset($cfg['LicenceKey']);
  }
    
  // set the payment method   
  $payment_method->write_configuration( $cfg );
 
  // redirect to reload payment method class
  //$mainframe->enqueueMessage($msg);  
  $mainframe->redirect( 'index.php?task=settings&option='. $payment_method->THIS_OPTION, $msg );
  
  return;
}

function settings_parseContent( $content )   {
        $ini2 = array();		
        $section = '_global_';
		
        foreach ( $content as $line ) {
            $line = trim( $line );
            if ( preg_match( '~^\[(.+?)\]$~', $line, $m ) ) {
                // Section header
                $section = $m[1];
            } elseif ( preg_match( '~^;(.*?)$~', $line, $m ) ) {
                // Comment
            } elseif ( preg_match( '~^\s*(.+?)\s*=\s*(.*?)$~', $line, $m ) ) {
                // Option/value pair				
				$ini2[$section][$m[1]] = trim($m[2]);
            } else {
                // Unknown line type
            }
        }
        return $ini2;
}

function create_settings_str($payment_method) {

$version = new JVersion();  

// add info
$str_settings = "[" . $payment_method->THIS_OPTION. "]";
$str_settings .= "\r\nName=" . $payment_method->THIS_PRODUCT;
$str_settings .= "\r\nVersion=" . $payment_method->THIS_VERSION;
$str_settings .= "\r\nPHP=" . phpversion();
$str_settings .= "\r\nMySQL=" . mysql_get_server_info();
$str_settings .= "\r\nJoomla=" . $version->getShortVersion();
$str_settings .= "\r\nVirtueMart=" . getVirtueMartVersion(true);
$str_settings .= "\r\nDateTime=" .date("Y-m-d G:i:s", time());
$str_settings .= "\r\nServer=" . JURI::root();
$str_settings .= "\r\n\r\n[Settings]\r\n";

// add licence
if($payment_method->THIS_LICENCE != 'GPL') {
 	global $licence;   
	$mylic_info = $licence->EL_get_info(); 
	if(!isset($mylic_info['key'])) $mylic_info['key'] = ''; 
	$str_settings .= "LicenceKey=" . $mylic_info['key']. "\r\n";
}


// add payment method   
$str_settings .= trim(extract_cfg_names($payment_method->classname));
return trim($str_settings) ;
}

function extract_cfg_names($pc){
  $str_cfg = "";

  /** Read current Configuration ***/ 
  $f = CLASSPATH ."payment". DS . $pc.".cfg.php";
  
  if (file_exists($f)) {  
    $a = file($f);
    foreach($a as $k=>$v) {
      $x = trim($v);
      $x = str_replace(' ', '', $x);
      if(stripos( $x, "define('" ) === 0)
      if(stripos( $x, "\r" ) === false ){
        $x = substr($x, 8);
        $x = substr($x, 0, strpos($x, "'") );

        $str_cfg .= "\r\n" . $x . "=" ; 
        $phpcode =  '$str_cfg .= ' . $x. ';';
        eval($phpcode ); 
      }
    }
  }
  return $str_cfg;
}

// html below

$cfg_str = create_settings_str($payment_method);
?> 
<h2>Import/Export Settings</h2>
<p> Here you can export and import your settings. This is useful when updating to the latest version


<form enctype="multipart/form-data" action="index.php" method="post" name="adminForm">

  <table>
    <tr> 
      <td width="150"> <label>Download Settings File:</label> </td>
      <td> <a href="index.php?task=export&option=<?php echo $payment_method->THIS_OPTION; ?>"> 
        <?php echo $filename; ?></a> </td>
    </tr>
    <tr> 
      <td colspan="2"></td>
    </tr>
    <tr> 
      <td width="150"> <label for="install_package">Upload Settings File:</label> 
      </td>
      <td> 
        <input class="input_box" id="import_settings" name="import_settings" type="file" size="57" />
        <input class="button" type="button" value="Upload File" onclick="submitbutton()" />
      </td>
    </tr>
    <tr> 
      <td colspan="2"></td>
    </tr>
  </table>
  <input type="hidden" name="task" value="import" />
  <input type="hidden" name="option" value="<?php echo $payment_method->THIS_OPTION; ?>" />
</form>

<h3>Settings  </h3>
<pre><?php echo htmlentities($cfg_str); ?></pre> 
