<?php
// *************************************************************************
// *                                                                       *
// * Product: VirtueMart Sage Pay (Protx) VSP Forms                        *
// * Filename: ps_protxvspform.php                                         *
// * Release Date: 2011.01.08                                              *
// * Build Time: 08 January 2011 00:01                                     *
// * Code Word: Srednekolymsk                                              *
// * Version: 1.5.31                                                       *
// * For: Joomla 1.5/VirtueMart 1.1.x                                      *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * Author: E-commerce Solution                                           *
// * Email: support@virtuemart-solutions.com                               *
// * Website: http://www.virtuemart-solutions.com                          *
// * Copyright: (C) 2008-2010 All rights reserved.                         *
// * Licence: Commercial                                                   *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * This software is furnished under a license and may be used and copied *
// * only  in  accordance  with  the  terms  of such  license and with the *
// * inclusion of the above copyright notice.  This software  or any other *
// * copies thereof may not be provided or otherwise made available to any *
// * other person.  No title to and  ownership of the  software is  hereby *
// * transferred.                                                          *
// *                                                                       *
// * You may not reverse  engineer, decompile, defeat  license  encryption *
// * mechanisms, or  disassemble this software product or software product *
// * license.  Ecom Solution  may  terminate  this license  if you fail to *
// * comply with any of the terms and conditions set forth in our end user *
// * license agreement (EULA).  In such event,  licensee  agrees to return *
// * licensor  or destroy  all copies of software  upon termination of the *
// * license.                                                              *
// *                                                                       *
// *************************************************************************

defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );

class ps_protxvspform {

    var $classname = "ps_protxvspform";
    var $payment_code = "PXFM";
	
    // *** Component Settings ***
    var $THIS_OPTION = 'com_protxvspform';
    var $THIS_COMPONENT = 'protxvspform';
    var $THIS_VERSION = '1.5.31';
    var $THIS_VERSION_DATE = '2011.01.08';
    var $THIS_VERSION_FOR = 'Joomla 1.5/VirtueMart 1.1.x';
    var $THIS_CODEWORD = 'Srednekolymsk';
    var $THIS_SUPPORT_EMAIL = 'support@virtuemart-solutions.com';
    var $THIS_VENDOR_WEBSITE = 'http://www.virtuemart-solutions.com';
    var $THIS_SUPPORT_WEBSITE = 'http://support.virtuemart-solutions.com';
    var $THIS_COPYRIGHT = '(C) 2008-2010 All rights reserved.';
    var $THIS_VENDOR = 'E-commerce Solution';
    var $THIS_PRODUCT = 'VirtueMart Sage Pay (Protx) VSP Forms';
    var $THIS_SHORTNAME = 'Sage Pay (Protx) VSP Forms';
    var $THIS_PRODUCT_DESC = 'The Sage Pay (Protx) VSP Forms component for VirtueMart adds a payment method which enables you to take payments with your Sage Pay (Protx)account.';
    var $THIS_RSS_ID = '56';
    var $THIS_HELP_ID = '23';
    var $THIS_LICENCE = 'Commercial';	

    var $my_config_array = array(
        	"PXFM_VENDOR", 
		"PXFM_PASS", 
		"PXFM_VENDOREMAIL", 
		"PXFM_DESC", 
		"PXFM_TEST", 
		"PXFM_PAYMENTTYPE", 
		"PXFM_ApplyAVSCV2", 
		"PXFM_Apply3DSecure", 
		"PXFM_AUTO", 
		"PXFM_DEBUG", 
		"PXFM_BTN_TEXT" ,   
		"PXFM_EMAILMSG"                         
		);

    /**
    * Show all configuration parameters for this payment method
    * @returns boolean False when the Payment method has no configration
    */
    function show_configuration() {
        global $VM_LANG;
        $db = new ps_DB();
          
        /** Read current Configuration ***/
        include_once(CLASSPATH ."payment/".$this->classname.".cfg.php");

	require_once(JPATH_BASE . DS . 'components' . DS. $this->THIS_OPTION . DS. 'generic_fns.php'); 		 
	check_config($this->my_config_array);

	// this uses a file also used by the admin page
	$payment_method = $this;
	include(JPATH_BASE . DS. 'components' . DS. $this->THIS_OPTION . DS. 'admin.panel.licence.php');

        include(JPATH_SITE . DS. 'administrator' . DS . 'components' . DS . $this->THIS_OPTION . DS . 'admin.fields.php'); 

   	// return false if there's no configuration
   	return true;
    }
    
    function has_configuration() {
      // return false if there's no configuration
      return true;
   }
   
  /**
	* Returns the "is_writeable" status of the configuration file
	* @param void
	* @returns boolean True when the configuration file is writeable, false when not
	*/
   function configfile_writeable() {
      return is_writeable( CLASSPATH."payment/".$this->classname.".cfg.php" );
   }
   
  /**
	* Returns the "is_readable" status of the configuration file
	* @param void
	* @returns boolean True when the configuration file is writeable, false when not
	*/

   function configfile_readable() {
      return is_readable( CLASSPATH."payment/".$this->classname.".cfg.php" );
   }
   
  /**
	* Writes the configuration file for this payment method
	* @param array An array of objects
	* @returns boolean True when writing was successful
	*/
   function write_configuration( &$d ) {
      
	// include fns
	require_once(JPATH_SITE . DS. 'administrator' . DS. 'components' . DS. $this->THIS_OPTION . DS. 'generic_fns.php');
	// alert if any missing
	$my_config_array = config_names($this->my_config_array, $d);

      $config = "<?php\r\n";
      $config .= "defined('_JEXEC') or die('Direct Access to this location is not allowed.'); \r\n\r\n";
      foreach( $my_config_array as $key => $value ) {
        $config .= "define ('$key', '$value');\r\n";
      } 
      $config .= "?>";
  
      if ($fp = fopen(CLASSPATH ."payment/".$this->classname.".cfg.php", "w")) {
          fputs($fp, $config, strlen($config));
          fclose ($fp);
          return true;
     }
     else
        return false;
   }
   
  /**************************************************************************
  ** name: process_payment()
  ** returns: 
  ***************************************************************************/
   function process_payment($order_number, $order_total, &$d) {
      // show the form... extra payment info
      return true;
   }
	
   function get_notify_url($ordernumber) {
		$url = JURI::root( false ) . 'administrator/components/' . $this->THIS_OPTION . '/' . $this->THIS_COMPONENT . '_notify.php?order_number=' . $ordernumber;
		return $url;
   }
}


// functions 
//__________________________________________________________
      

function simpleXor($InString, $Key) {
  // Initialise key array
  $KeyList = array();
  // Initialise out variable
  $output = "";
  
  // Convert $Key into array of ASCII values
  for($i = 0; $i < strlen($Key); $i++){
    $KeyList[$i] = ord(substr($Key, $i, 1));
  }

  // Step through string a character at a time
  for($i = 0; $i < strlen($InString); $i++) {
    // Get ASCII code from string, get ASCII code from key (loop through with MOD), XOR the two, get the character from the result
    // % is MOD (modulus), ^ is XOR
    $output.= chr(ord(substr($InString, $i, 1)) ^ ($KeyList[$i % strlen($Key)]));
  }

  // Return the result
  return $output;
}

function getToken($thisString) {

  // List the possible tokens
  $Tokens = array(
    "Status",
    "StatusDetail",
    "VendorTxCode",
    "VPSTxId",
    "TxAuthNo",
    "Amount",
    "AVSCV2", 
    "AddressResult", 
    "PostCodeResult", 
    "CV2Result", 
    "GiftAid", 
    "3DSecureStatus", 
    "CAVV" );

  // Initialise arrays
  $output = array();
  $resultArray = array();
  
  // Get the next token in the sequence
  for ($i = count($Tokens)-1; $i >= 0 ; $i--){
    // Find the position in the string
    $start = strpos($thisString, $Tokens[$i]);
    // If it's present
    if ($start !== false){
      // Record position and token name
      $resultArray[$i]->start = $start;
      $resultArray[$i]->token = $Tokens[$i];
    }
  }
  
  // Sort in order of position
  sort($resultArray);

  // Go through the result array, getting the token values
  for ($i = 0; $i<count($resultArray); $i++){
    // Get the start point of the value
    $valueStart = $resultArray[$i]->start + strlen($resultArray[$i]->token) + 1;
    // Get the length of the value
    if ($i==(count($resultArray)-1)) {
      $output[$resultArray[$i]->token] = substr($thisString, $valueStart);
    } else {
      $valueLength = $resultArray[$i+1]->start - $resultArray[$i]->start - strlen($resultArray[$i]->token) - 2;
      $output[$resultArray[$i]->token] = substr($thisString, $valueStart, $valueLength);
    }      

  }

  // Return the ouput array
  return $output;
}
 

?>