<?php
// *************************************************************************
// * admin.page.intro.php                                                  *
// * Code Word: Srednekolymsk                                              *
// *************************************************************************
?> 
<h2><img align="right" style="margin-right:8px;"
src="<?php echo JURI::root( false ) ?>components/<?php echo $payment_method->THIS_OPTION; ?>/logo.gif" 
 border="0" > 
  <?php echo $payment_method->THIS_PRODUCT; ?>
</h2>

<br><br>
<?php echo $payment_method->THIS_PRODUCT_DESC ?>

<br><br>   
  
<?php 

if(COMP_IS_PAYMENT)
	include('admin.panel.cpanel.php');
else
	include('admin.panel.component_cpanel.php'); 

?>   