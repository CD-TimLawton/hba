<?php
// *************************************************************************
// * admin.page.diag.php                                                   *
// * Code Word: Srednekolymsk                                              *
// *************************************************************************

  // display string
  $version = new JVersion();  
?> 
<h2> Diagnostics </h2>
<p>This information may help identify the cause of any technical problems that 
  may arise with the component.<br>
</p>
  
<p><strong>Database</strong> </p>

<?php if(COMP_IS_PAYMENT) {

	$rec_exists = ($payment_method_id != ''); 
	echo showYesNoImg($rec_exists);
	echo '&nbsp;';
	if($rec_exists) { 
	  echo 'The payment method record exists in the database';
	} else {  
	  echo 'The payment method record does not exist in the database - run the SQL again. ';
	} 
	echo '<br>';
	
	$infopath = JPATH_SITE . DS . 'administrator' . DS . 'components' . DS . $payment_method->THIS_OPTION . DS . 'payment_extrainfo.txt';
	$infourl =  'components/' . $payment_method->THIS_OPTION . '/payment_extrainfo.txt';
	
	$payment_extrainfo1 = trim(file_get_contents1($infopath));
	
	$q = "SELECT payment_extrainfo FROM #__vm_payment_method WHERE payment_method_id='" . $payment_method_id . "';"; 
	$database->setQuery( $q );
	$payment_extrainfo2 = trim($database->loadResult());
	
	$payment_extrainfo1 = str_replace("\r", "", $payment_extrainfo1);
	$payment_extrainfo1 = str_replace("\n", "", $payment_extrainfo1);
	
	$payment_extrainfo2 = str_replace("\r", "", $payment_extrainfo2);
	$payment_extrainfo2 = str_replace("\n", "", $payment_extrainfo2);
	
	$info_match = (trim($payment_extrainfo1) == trim($payment_extrainfo2) );
	echo showYesNoImg($info_match);
	echo '&nbsp;';
	if($info_match) { 
	  echo 'Payment method Extra Info Code OK';
	} else {  
	  echo 'Extra Info Code does not match the code supplied';
	} 
 
} // COMP_IS_PAYMENT 


if(isset($payment_method->THIS_DBTABLE)) {

  $tbl_name = $database->replacePrefix($payment_method->THIS_DBTABLE);
  $chk_tbl = table_exists($tbl_name);

  echo '<br>';
  echo showYesNoImg($chk_tbl );
  echo '&nbsp;';
  if($chk_tbl ) { 
    echo 'Table OK: ' . $tbl_name;
  } else {  
    echo 'Table does not exist: ' . $tbl_name;
  } 
} 
 
 
if(isset($payment_method->THIS_CC_CODES)) {
?>
<p> <a href="index2.php?option=<?php echo $payment_method->THIS_OPTION; ?>&amp;task=cards"> 
  <?php echo $img_new; ?>
  Credit Cards</a> - Check the setup of credit card short codes 
<?php   
} // isset($payment_method->THIS_CC_CODES)
?>

<p> <a href="index2.php?option=<?php echo $payment_method->THIS_OPTION; ?>&amp;task=files"> 
  <?php echo $img_new; ?>
  Check Files</a> - This will check the existence of all installation files 
<p> <a href="index2.php?option=<?php echo $payment_method->THIS_OPTION; ?>&amp;task=sqlfile"> 
  <?php echo $img_new; ?>
  Run SQL</a> - Run Installation SQL scripts again 
  
  
<?php if(COMP_IS_PAYMENT) { ?> 
  
<p> <a href="index2.php?option=<?php echo $payment_method->THIS_OPTION; ?>&amp;task=extrainfo"> 
  <?php echo $img_new; ?>
  Reset Extra Info Code</a> - Update Extra Info Code with code supplied in <a target="_blank" 
href="<?php echo $infourl; ?>">payment_extrainfo.txt</a> 
</p>
<p> <a href="index2.php?option=<?php echo $payment_method->THIS_OPTION; ?>&amp;task=settings"> 
  <?php echo $img_new; ?>
  Import/Export Settings</a></p>
  
<?php } else
	include('admin.panel.component_diag.php'); 
?> 
  
<p> <a target="_blank" href="components/<?php echo $payment_method->THIS_OPTION; ?>/backup/install.txt"> 
  <?php echo $img_new; ?>
  View Install Log</a></p>
<p><strong>Versions</strong></p>
  
<p> 
<table 
  style="background-color:#FFFFFF;border:1px solid #CCCCCC;"
  width="95%">
  <tr> 
    <td width="130">Joomla Version:</td>
    <td> 
      <?php echo $version->getShortVersion(); ?>
    </td>
  </tr>
  <tr> 
    <td width="130">VirtueMart Version:</td>
    <td> 
      <?php echo getVirtueMartVersion(true); ?>
    </td>
  </tr>
  <tr> 
    <td width="130">PHP Version:</td>
    <td> 
      <?php echo phpversion(); ?>
    </td>
  </tr>
  <tr> 
    <td width="130">MySQL Version:</td>
    <td> 
      <?php echo mysql_get_server_info(); ?>
    </td>
  </tr>
</table>
<p>