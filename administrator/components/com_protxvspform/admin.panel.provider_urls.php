<?php
// *************************************************************************
// * Filename: admin.panel.provider_urls.php                               *
// * Code Word: Srednekolymsk                                              *
// *************************************************************************
?>

<h4>Sage Pay Websites</h4>
<p>Sage Pay Help Centre: <a href="http://www.sagepay.com/help" target="_blank">http://www.sagepay.com/help</a><br />
  Sage Pay Admin (Live): <a href="https://live.sagepay.com/mysagepay/" target="_blank">https://live.sagepay.com/mysagepay/</a><br>
  Sage Pay Admin (Test): <a href="https://test.sagepay.com/mysagepay/" target="_blank"> 
  https://test.sagepay.com/mysagepay/</a><br>
  Sage Pay Simulator Admin: 
  <a href="https://test.sagepay.com/simulator" target="_blank">https://test.sagepay.com/simulator</a></p>
