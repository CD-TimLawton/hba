<?php 
// *************************************************************************
// * checkout.result_notify.php                                            *
// * Code Word: Srednekolymsk                                              *
// * This core virtuemart script has been updated to show extra fields     *
// *************************************************************************

// * this is a generic payment processor return page 

defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );

mm_showMyFileName( __FILE__ );

if( !isset( $_REQUEST["order_id"] ) || empty( $_REQUEST["order_number"] )) {
	echo "Order ID is not set or empty!";
}
else {

    $order_id = JRequest::getVar('order_id');
    $order_number = JRequest::getVar('order_number');
    $desc = JRequest::getVar('desc'); 

	$q = "SELECT order_status FROM #__{vm}_orders WHERE ";
	$q .= "#__{vm}_orders.user_id= " . $auth["user_id"] . " ";
	$q .= "AND #__{vm}_orders.order_id=$order_id ";
	$q .= "AND #__{vm}_orders.order_number LIKE '$order_number' ";

	$db->query($q);
	if ($db->next_record()) {
		$order_status = $db->f("order_status");
		if($order_status == 'C') {
		?> 
            <h2> Thank you for your order </h2>  
        <?php } else { ?>
            <span class="message">
                 Your order could not be completed.
            </span>    
        <?php } ?>
        
        <p><b>
         Your Order ID is: <?php echo $order_id ?>
        </b></p>  
           
        <p><b>Result: 
        <?php echo $desc ; ?>
        </b></p>  
           
        <p><b>Status: 
        <?php 
		    
	$q = "SELECT order_status_name FROM #__{vm}_order_status WHERE ";
	$q .= "#__{vm}_order_status.order_status_code= '" . $order_status  . "'; "; 
	$db->query($q);
	if ($db->next_record()) echo $db->f("order_status_name");
	
		  
        ?>
        </b></p>   
        
        <p><a href="index.php?option=com_virtuemart&page=account.order_details&order_id=<?php echo $order_id ?>">
        <?php echo  $VM_LANG->_('PHPSHOP_ORDER_LINK') ?></a>
        </p>
     
    <?php
	}
	else {
		echo "Order not found!";
	}
}
?>
