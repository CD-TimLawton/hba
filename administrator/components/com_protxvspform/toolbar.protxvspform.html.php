<?php
// *************************************************************************
// * toolbar.html.php                                                      *
// * Code Word: Srednekolymsk                                              *
// *************************************************************************


// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
* @package		Joomla
* @subpackage	protxvspform
*/
class TOOLBAR_protxvspform
{
	/**
	* Draws the menu
	*/
	function _DEFAULT() { 
		JToolBarHelper::title( JText::_( 'VirtueMart Sage Pay (Protx) VSP Forms' ), 'header icon-48-generic.png' ); 

		JToolBarHelper::custom('intro', 'default.png', 'default.png', 'Intro', false, false);
		JToolBarHelper::custom('edit', 'edit.png', 'edit.png', 'Edit', false, false);
		JToolBarHelper::custom('setup', 'new.png', 'new.png', 'Setup', false, false);
		JToolBarHelper::custom('test', 'apply.png', 'apply.png', 'Test', false, false);
		JToolBarHelper::custom('diag', 'config.png', 'config.png', 'Diagnostics', false, false);
		JToolBarHelper::custom('updates', 'send.png', 'send.png', 'Updates', false, false);
		JToolBarHelper::custom('support', 'help.png', 'help.png', 'Support', false, false);

	}

}

?>