<?php
// *************************************************************************
// * admin.page.updates.php                                                *
// * Code Word: Srednekolymsk                                              *
// *************************************************************************

	
  // set url
  $download_url = $payment_method->THIS_VENDOR_WEBSITE . '/download';

  if($payment_method->THIS_LICENCE != 'GPL') {	

	// include licence
	global $licence;
	require_once(JPATH_BASE . DS . 'components' . DS . $payment_method->THIS_OPTION . DS . 'licencecheck.e.php');    

	$mylic_info = $licence->EL_get_info(); 
	if(!isset($mylic_info['key'])) $mylic_info['key'] = ''; 
	if(!isset($mylic_info['exp'])) $mylic_info['exp'] = time()-1; 


	if( $mylic_info['key'] != '')
          if( intval( $mylic_info['exp'] ) > time() )
	    $download_url = $payment_method->THIS_VENDOR_WEBSITE . '/index.php?option=com_virtuemart&page=shop.downloads&download_id=' . $mylic_info['key'];
	
  }


?> 
<h2> Product Updates </h2>
<table width="99%" style="background-color:#FFFFFF;border:1px solid #CCCCCC;" >
  <tr valign="top"> 
    <td width="80" align="center"><p>
      &nbsp;&nbsp;&nbsp;&nbsp; <a href="<?php echo $download_url; ?>" target="_blank"><img 
src="<?php echo JURI::root( false ) ?>administrator/images/backup.png" 
border="0" hspace="10"></a></td>
    <td> 
      <p><a href="<?php echo $download_url; ?>" target="_blank"><strong>Download 
        Latest Version</strong></a> <br>
		
        You can also download via your 
<a href="<?php echo $payment_method->THIS_VENDOR_WEBSITE; ?>/account" target="_blank">
Account Order Detail</a> 

 page by clicking the 
        product link. </p>
		
      <p>FAQ: <a href="<?php echo $payment_method->THIS_SUPPORT_WEBSITE; ?>/kb?115" target="_blank">How 
        to update your component to the latest version</a> <br>
        See also: <a href="index2.php?task=settings&amp;option=<?php echo $payment_method->THIS_OPTION; ?>"> 
        Import/Export settings</a> </p>
      </td>
  </tr>
</table>
<p>Our components are updated regularly, and the <a href="http://www.virtuemart-solutions.com/product-updates" target="_blank"> 
  Product Updates feed</a> notifies you when new upgrades are available and other 
  product announcements. The most recent updates are listed below. 
<p> 
<table width="99%" border="0" cellspacing="0" cellpadding="0">
  <tr valign="top"> 
    <td width="49%"> 
      <?php ShowlastRSS($url_rssnews); ?>
    </td>
    <td width="2%"></td>
    <td width="49%"> 
      <?php ShowlastRSS($url_rssupd, NULL, 3); ?>
      <p><a href="<?php echo $url_rssupd_more; ?>" target="_blank">More Product 
        Updates</a> </p>
    </td>
  </tr>
</table>
