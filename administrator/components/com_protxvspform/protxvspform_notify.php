<?php
// *************************************************************************
// *                                                                       *
// * Product: VirtueMart Sage Pay (Protx) VSP Forms                        *
// * Filename: protxvspform_notify.php                                     *
// * Release Date: 2011.01.08                                              *
// * Build Time: 08 January 2011 00:01                                     *
// * Code Word: Srednekolymsk                                              *
// * Version: 1.5.31                                                       *
// * For: Joomla 1.5/VirtueMart 1.1.x                                      *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * Author: E-commerce Solution                                           *
// * Email: support@virtuemart-solutions.com                               *
// * Website: http://www.virtuemart-solutions.com                          *
// * Copyright: (C) 2008-2010 All rights reserved.                         *
// * Licence: Commercial                                                   *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * This software is furnished under a license and may be used and copied *
// * only  in  accordance  with  the  terms  of such  license and with the *
// * inclusion of the above copyright notice.  This software  or any other *
// * copies thereof may not be provided or otherwise made available to any *
// * other person.  No title to and  ownership of the  software is  hereby *
// * transferred.                                                          *
// *                                                                       *
// * You may not reverse  engineer, decompile, defeat  license  encryption *
// * mechanisms, or  disassemble this software product or software product *
// * license.  Ecom Solution  may  terminate  this license  if you fail to *
// * comply with any of the terms and conditions set forth in our end user *
// * license agreement (EULA).  In such event,  licensee  agrees to return *
// * licensor  or destroy  all copies of software  upon termination of the *
// * license.                                                              *
// *                                                                       *
// *************************************************************************


/*** access licence protected functions ***/
require_once('licencecheck.e.php'); 
require_once('standalone.e.php'); 
require_once('generic_fns.php'); 

// include config 
require_once( CLASSPATH. 'payment/ps_protxvspform.cfg.php' );

// include payment method 
global $payment_method;
require_once( CLASSPATH. 'payment/ps_protxvspform.php' );
$payment_method = new ps_protxvspform();  

$your_ip = $_SERVER['REMOTE_ADDR']; 
$hostname = gethostbyaddr($your_ip);

if (PXFM_DEBUG == '1') 
  standalone_start($payment_method, '');
else
  standalone_start(NULL, '');

// show no errors
$err_level = error_reporting(0);

// any data sent? 
$crypt = get_req('crypt');
$crypt = str_replace (" ", "+", $crypt);

if ($crypt != '') {    
	
    $Decoded=SimpleXor(base64_decode($crypt),PXFM_PASS);

    if (PXFM_DEBUG == '1') {
      echo "<pre>";
	  print_r(KeyPairtoArray($Decoded));
      echo "</pre>";	  
    }

    // ** Split out the useful information into variables we can use **
    $values = getToken($Decoded);

    $VendorTxCode = getArrayValue($values,'VendorTxCode');
    $Status = getArrayValue($values,'Status');
    $StatusDetail = getArrayValue($values,'StatusDetail');
    $TxAuthNo = getArrayValue($values,'TxAuthNo');
    $VPSTxID = getArrayValue($values,'VPSTxId');
    $AVSCV2 = getArrayValue($values,'AVSCV2');
    $Amount = getArrayValue($values,'Amount');

    // protocol 2.22 fields
    $AddressResult = getArrayValue($values, 'AddressResult' );
    $PostCodeResult = getArrayValue($values, 'PostCodeResult' );
    $CV2Result = getArrayValue($values, 'CV2Result' );
    $GiftAid = getArrayValue($values, 'GiftAid' );
    $VBVSecureStatus = getArrayValue($values, '3DSecureStatus' );
    $CAVV = getArrayValue($values, 'CAVV' );

    $order_payment_log_add = "\n<b>Payment Result</b>: ". 
			"\n<br>Status: ". $Status. 
			"\n<br>StatusDetail: ". $StatusDetail. 
			"\n<br>VPSTxID: ". $VPSTxID. 
			"\n<br>TxAuthNo: ". $TxAuthNo. 
			"\n<br>AVSCV2: ". $AVSCV2. 
			"\n<br>AddressResult: ". $AddressResult. 
			"\n<br>PostCodeResult: ". $PostCodeResult. 
			"\n<br>CV2Result: ". $CV2Result. 
			"\n<br>3DSecureStatus: ". $VBVSecureStatus; 

    $payment_status  = trim($Status); 
    $order_id = trim($VendorTxCode); 
    $total = trim($Amount);
	
    if (PXFM_DEBUG==1) {
        echo "Protx Order Id: " . $order_id . "<br>"; 
        echo "Protx Status: " . $payment_status . "<br>";
        echo "Protx Detail: " . $StatusDetail . "<br>";
        echo "Protx Total: " . $total . "<br>";
    }
    
    // check the order   
    if ( trim($order_id) == '')
      exit('<b>Error: No Order specified</b>');     
	  
    // check the order in db
    $q = "SELECT order_id, order_total, order_number FROM #__{vm}_orders "; 
    $q .= " WHERE order_id='" . $order_id . "'; "; 
    $db = new ps_DB;
    $db->query($q);
    $db->next_record();

    if (PXFM_DEBUG==1) 
      echo("<br>" . $q . "<br>");

    $order_number = $db->f("order_number");
    $order_total = $db->f("order_total");

    if (PXFM_DEBUG==1) {
        echo "Order Id: " . $order_id . "<br>"; 
        echo "Order Number: " . $order_number . "<br>"; 
        echo "Order Total: " . $total . "<br>";
    }

    if ($order_total == '')
      exit('<b>Error: Order not recognized</b>');          
    //if ($order_total != $total)
    //  exit('<b>Error: Wrong amount</b>');  
    if ($order_number != trim(get_req('order_number'))) 
      exit('<b>Error: Wrong order_number</b>');  

    $d['order_id'] = $order_id;    // this identifies the order record
    $d['order_comment'] = $Status; 
	
    //AUTHENTICATED and REGISTERED statuses are only returned if the TxType is AUTHENTICATE.
    if( strtoupper($payment_status) == 'OK' ) {

	$d['current_order_status'] = 'P'; 
        $d['order_status'] = 'C'; 
        $d['notify_customer'] = "Y";
    }
    else {
        $d['order_status'] = 'P';  
    }

    //else if( strtoupper($payment_status == 'DECLINED') ){ 

    // update order_payment_log      
    $q = "UPDATE #__{vm}_order_payment SET ";
    $q .= "\n order_payment_log='" . $order_payment_log_add . "',  "; 
    $q .= "\n order_payment_trans_id='" . $VPSTxID . "'  "; 
    $q .= "\n WHERE order_id='".$order_id."'";
    $db->query($q);               

    // set back error-handling 
    error_reporting($err_level); 
    update_order($d);

	// redirect to the order detail page
	$nextpage =  '../../../index.php?option=com_virtuemart&page=checkout.result_notify&order_id='. $order_id . 
		'&order_number='. $order_number .
		'&desc='. $StatusDetail;
	
	if (PXFM_DEBUG!=1) { ?>
		<script language="JavaScript">
		window.location.href='<?php echo $nextpage; ?>';
		</script>
	<?php } else { ?> 
		<p><strong>Order Updated</strong>
		<p><a href="<?php echo $nextpage; ?>"><?php echo $nextpage; ?></a> 
	<?php
	}
	

	
} else {  
    exit('<b>Error: No crypt data received</b>');  
}

?>
</body></html>
