<?php
// *************************************************************************
// *                                                                       *
// * Product: VirtueMart Sage Pay (Protx) VSP Forms                        *
// * Filename: protxvspform.form.php                                       *
// * Release Date: 2011.01.08                                              *
// * Build Time: 08 January 2011 00:01                                     *
// * Code Word: Srednekolymsk                                              *
// * Version: 1.5.31                                                       *
// * For: Joomla 1.5/VirtueMart 1.1.x                                      *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * Author: E-commerce Solution                                           *
// * Email: support@virtuemart-solutions.com                               *
// * Website: http://www.virtuemart-solutions.com                          *
// * Copyright: (C) 2008-2010 All rights reserved.                         *
// * Licence: Commercial                                                   *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * This software is furnished under a license and may be used and copied *
// * only  in  accordance  with  the  terms  of such  license and with the *
// * inclusion of the above copyright notice.  This software  or any other *
// * copies thereof may not be provided or otherwise made available to any *
// * other person.  No title to and  ownership of the  software is  hereby *
// * transferred.                                                          *
// *                                                                       *
// * You may not reverse  engineer, decompile, defeat  license  encryption *
// * mechanisms, or  disassemble this software product or software product *
// * license.  Ecom Solution  may  terminate  this license  if you fail to *
// * comply with any of the terms and conditions set forth in our end user *
// * license agreement (EULA).  In such event,  licensee  agrees to return *
// * licensor  or destroy  all copies of software  upon termination of the *
// * license.                                                              *
// *                                                                       *
// *************************************************************************


// comp settings
require_once(CLASSPATH.'payment/ps_protxvspform.php');  
require_once(CLASSPATH.'payment/ps_protxvspform.cfg.php');  
$payment_method = new ps_protxvspform(); 

// include generic_fns
require_once($mosConfig_absolute_path . '/administrator/components/' . $payment_method->THIS_OPTION. '/generic_fns.php');

// update the db and user of on order print
$dbt = new ps_DB; 

$q = "SELECT * FROM #__{vm}_orders WHERE order_id='$order_id'";
$db->query($q);
if( $db->next_record() ) {	
	  $user_id = $db->f("user_id");
	  $qt = "SELECT * from #__{vm}_order_user_info WHERE user_id='$user_id' AND order_id='$order_id' ORDER BY address_type ASC"; 
	  $dbt->query($qt);
	  $dbt->next_record();
	  $user = $dbt->record[0];	
	  if(count( $dbt->record ) == 2)
		$dbst = $dbt->record[1];
	  else
		$dbst = $user;
}


$amount = $db->f("order_total");       
$amount = sprintf ("%01.2f", $amount);

$stuff = '';
$stuff .= "VendorTxCode=" . $db->f("order_id")  . "&";    
$stuff .= "Amount=" . $amount  . "&";  
$stuff .= "Currency=" . $db->f("order_currency"). "&";
$stuff .= "Description=" . PXFM_DESC . " ". $db->f("order_id") ."&";
$stuff .= "SuccessURL=" . $payment_method->get_notify_url($db->f("order_number")) . "&";
$stuff .= "FailureURL=" . $payment_method->get_notify_url($db->f("order_number")) . "&";
 

// Billing Details:
$stuff .= "BillingFirstnames=" . substr($dbbt->f("first_name"), 0, 40) . "&";
$stuff .= "BillingSurname=" . substr($dbbt->f("last_name"), 0, 40) . "&";
$stuff .= "BillingAddress1=" . substr($dbbt->f("address_1"), 0, 60) . "&";
if (strlen($dbbt->f("address_2")) > 0) $stuff .= "BillingAddress2=" . substr($dbbt->f("address_2"), 0, 60) . "&";
$stuff .= "BillingCity=" . substr($dbbt->f("city"), 0, 40) . "&";

// NOTE: If you have a NON UK post code you should enter 000 in the billing post code field.
// $stuff .= "BillingPostCode=" . substr($dbbt->f("zip"), 0, 20) . "&";
if ( $dbbt->f("country") == 'GBR') {
    $stuff .= "BillingPostCode=" . substr($dbbt->f("zip"), 0, 20) . "&";
} else {
    $stuff .= "BillingPostCode=000" . "&";
}

$billingcountry = country_code_3_2($dbbt->f("country"));
$stuff .= "BillingCountry=" . $billingcountry  . "&";

// only us
if ($billingcountry == 'US')
  if (strlen($dbbt->f("state")) > 0) $stuff .= "BillingState=" . $dbbt->f("state") . "&";
if (strlen($dbbt->f("phone_1")) > 0) $stuff .= "BillingPhone=" . $dbbt->f("phone_1") . "&";

// Delivery Details:
$stuff .= "DeliveryFirstnames=" . substr($dbst->first_name, 0, 40) . "&";
$stuff .= "DeliverySurname=" . substr($dbst->last_name, 0, 40) . "&";
$stuff .= "DeliveryAddress1=" . substr($dbst->address_1, 0, 60) . "&";
if (strlen($dbst->address_2) > 0) $stuff .= "DeliveryAddress2=" . substr($dbst->address_2, 0, 60) . "&";
$stuff .= "DeliveryCity=" . substr($dbst->city, 0, 40) . "&";

// NOTE: If you have a NON UK post code you should enter 000 in the billing post code field.
// $stuff .= "DeliveryPostCode=" . substr($dbst->zip, 0, 20) . "&"; 
if ( $dbst->country == 'GBR') {
    $stuff .= "DeliveryPostCode=" . substr($dbst->zip, 0, 20) . "&";
} else {
    $stuff .= "DeliveryPostCode=000" . "&";
}


$deliverycountry = country_code_3_2($dbst->country);
$stuff .= "DeliveryCountry=" . $deliverycountry . "&";

// only us
if ($deliverycountry == 'US')
  if (strlen($dbst->state) > 0) $stuff .= "DeliveryState=" . $dbst->state . "&";
if (strlen($dbst->phone_1) > 0) $stuff .= "DeliveryPhone=" . $dbst->phone_1 . "&";  

// optional; 
if(PXFM_VENDOREMAIL != '')
  $stuff .= "VendorEmail=" . PXFM_VENDOREMAIL . "&";  
if(PXFM_EMAILMSG != '')
  $stuff .= "eMailMessage=" . PXFM_EMAILMSG . "&";      

$stuff .= "CustomerEMail=" . $user->user_email  . "&";   
$stuff .= "CustomerName=" . substr($dbbt->f("title"), 0, 10) . " " .
                            substr($dbbt->f("first_name"), 0, 40) . " " .
                            substr($dbbt->f("last_name"), 0, 40) . "&";  
							
// $stuff .= "Basket=" . get_basket_items($order_id, 'sagepay') . "&";

$stuff .= "ApplyAVSCV2=" . PXFM_ApplyAVSCV2 . "&";
$stuff .= "Apply3DSecure=" . PXFM_Apply3DSecure; // . "&"; 


// remove empty address lines
$stuff = str_replace("\n\n", "\n", $stuff );

$crypt = base64_encode(SimpleXor($stuff,PXFM_PASS));
DEFINE ('PXFM_CRYPT', $crypt);     					
	
	
define ('PXFM_URL_SIM', 'https://test.sagepay.com/Simulator/VSPFormGateway.asp'); 
define ('PXFM_URL_TEST', 'https://test.sagepay.com/gateway/service/vspform-register.vsp');
define ('PXFM_URL_LIVE', 'https://live.sagepay.com/gateway/service/vspform-register.vsp');

switch (PXFM_TEST ) {
        case 'TEST': DEFINE ('PXFM_URL', PXFM_URL_TEST); break;
        case 'LIVE': DEFINE ('PXFM_URL', PXFM_URL_LIVE); break;
        default: DEFINE ('PXFM_URL', PXFM_URL_SIM); break;
}

if(PXFM_DEBUG == '1') {
?>
<pre>
<?php print_r(KeyPairtoArray($stuff)); ?>
</pre>
<?php } ?>

 
<!-- Protx form -->
<FORM ACTION="<?php echo(PXFM_URL); ?>" METHOD="POST" NAME="formpxfm">
  <INPUT TYPE="hidden" NAME="VPSProtocol" VALUE="2.23">
  <INPUT TYPE="hidden" NAME="TxType" VALUE="<?php echo(PXFM_PAYMENTTYPE);?>">
  <INPUT TYPE="hidden" NAME="Vendor" VALUE="<?php echo(PXFM_VENDOR);?>">
  <INPUT TYPE="hidden" NAME="Crypt" VALUE="<?php echo(PXFM_CRYPT); ?>">
  <input type="hidden" name="cmd" value="cart" />  	


<p align="left">
<?php echo PXFM_BTN_TEXT; ?>
<input type="submit" name="Continue" value="Continue"> 
<p>

</FORM>
 
<?php if ( (PXFM_DEBUG != '1') && (PXFM_AUTO == '1') && ($page == 'checkout.thankyou')) { ?>

<script language="JavaScript">
  setTimeout("document.forms['formpxfm'].submit();", 1000);
</script> 
  
<?php 
} 
?>