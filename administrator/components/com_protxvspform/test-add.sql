INSERT INTO `#__vm_product` (
`vendor_id` ,
`product_parent_id` ,
`product_sku` ,
`product_s_desc` ,
`product_desc` ,
`product_thumb_image` ,
`product_full_image` ,
`product_publish` ,
`product_weight` ,
`product_weight_uom` ,
`product_length` ,
`product_width` ,
`product_height` ,
`product_lwh_uom` ,
`product_url` ,
`product_in_stock` ,
`product_available_date` ,
`product_availability` ,
`product_special` ,
`product_discount_id` ,
`ship_code_id` ,
`cdate` ,
`mdate` ,
`product_name` ,
`product_sales` ,
`attribute` ,
`custom_attribute` ,
`product_tax_id` ,
`product_unit` ,
`product_packaging` ,
`child_options` ,
`quantity_options` ,
`child_option_ids` ,
`product_order_levels` 
)
VALUES (
'1', '0', 'Test01', 'Test Product', '<p>Test Product</p>', '', '', 'Y', '0.0000', 'pounds', '0.0000', '0.0000', '0.0000', 'inches', '', '1000', '1251068400', '', 'N', '0', NULL , '1251125797', '1251125892', 'Test Product', '0', '', '', '3', 'piece', '0', 'N,N,N,N,N,Y,20%,10%,', 'none,0,0,1', '', '0,0'
);


INSERT INTO `#__vm_shipping_carrier` (
`shipping_carrier_id` ,
`shipping_carrier_name` ,
`shipping_carrier_list_order` 
)
VALUES (
'99', 'Test', '0'
);


INSERT INTO `#__vm_shipping_rate` (
`shipping_rate_name` ,
`shipping_rate_carrier_id` ,
`shipping_rate_country` ,
`shipping_rate_zip_start` ,
`shipping_rate_zip_end` ,
`shipping_rate_weight_start` ,
`shipping_rate_weight_end` ,
`shipping_rate_value` ,
`shipping_rate_package_fee` ,
`shipping_rate_currency_id` ,
`shipping_rate_vat_id` ,
`shipping_rate_list_order` 
)
VALUES (
 'Test', '99', 'GBR;USA;', '0', '99999', '0.000', '999', '0.01', '0.00', '52', '0', '0'
);
