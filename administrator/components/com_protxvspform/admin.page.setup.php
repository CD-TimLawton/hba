<?php 
// *************************************************************************
// * admin.page.setup.php                                                  *
// * Code Word: Srednekolymsk                                              *
// *************************************************************************
?> 
<h2>Setup</h2>
<p>For full Installation and Setup Instructions see the Installation Guide on 
  our support website: <br>
  <a href="<?php echo $help_url;?>"  target="_blank"> FAQ: 
  <?php echo $payment_method->THIS_PRODUCT; ?>
  </a> 
  
<?php if(COMP_IS_PAYMENT) { ?>   

<h4>Payment Method Configuration</h4>

<?php if( $payment_method->THIS_LICENCE != 'GPL') { ?>
    <p>
	First <a href="javascript:EL_open_Manager('<?php echo $payment_method->THIS_OPTION; ?>');">
	Enter your licence key</a>
<?php } ?>

<p>Enter your settings in the <a 
href="index2.php?option=<?php echo $payment_method->THIS_OPTION; ?>&amp;task=edit">Payment 
  Method Configuration</a>. To import previous settings use the <a href="index2.php?task=settings&amp;option=<?php echo $payment_method->THIS_OPTION; ?>"> 
  Import/Export settings</a> tool. </p>
<p>
<?php include('admin.panel.provider_setup.php'); ?>  
<p>
<?php include('admin.panel.provider_urls.php'); ?>

<?php } else { ?>  
<?php include('admin.panel.component_setup.php'); ?>  
<?php } // COMP_IS_PAYMENT ?>  
