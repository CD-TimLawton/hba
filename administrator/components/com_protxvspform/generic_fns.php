<?php
// *************************************************************************
// *                                                                       *
// * Generic VirtueMart Payment Method Generic Functions                   *
// * Release Date: 2010.07.03                                              *
// * Build Time: 3 July 2010 20:00                                         *
// * Version: 1.1.5                                                        *
// * Code Word: Srednekolymsk                                              *
// * For: Joomla 1.5/VirtueMart 1.1.x                                      *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * Author: E-commerce Solution                                           *
// * Email: support@virtuemart-solutions.com                               *
// * Website: http://www.virtuemart-solutions.com                          *
// * Copyright: (C) 2009 E-commerce Solution                               *
// * Licence: GPL                                                          *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * This program is free software: you can redistribute it and/or modify  *
// * it under the terms of the GNU General Public License as published by  *
// * the Free Software Foundation, either version 3 of the License, or     *
// * (at your option) any later version.                                   *
// *                                                                       *
// * This program is distributed in the hope that it will be useful,       *
// * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
// * GNU General Public License for more details.                          *
// *                                                                       *
// * You should have received a copy of the GNU General Public License     *
// * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
// *                                                                       *
// *                                                                       *
// *                                                                       *
// *************************************************************************


if(!defined('generic_fns')) {
define('generic_fns', 1);

// GENERIC FUNCTIONS 
global $img_ok, $img_no, $img_new;
$img_0 = '<img border="0" hspace="4" src="' . JURI::root( false ).'administrator/images/blank.png" width="16" height="16" align="absmiddle">'; 
$img_ok = '<img border="0" hspace="2" src="' . JURI::root( false ).'administrator/images/tick.png" width="16" height="16" align="absmiddle">'; 
$img_no = '<img border="0" hspace="2" src="' . JURI::root( false ).'administrator/images/publish_x.png" width="16" height="16" align="absmiddle">'; 
$img_new = '<img title="New" border="0" vspace="2" hspace="2" src="' . JURI::root( false ).'administrator/images/new.png" width="16" height="16" align="absmiddle">'; 

$img_edit = '<img title="Edit" border="0" hspace="2" src="' . JURI::root( false ).'/images/M_images/edit.png" width="16" height="16" align="absmiddle">'; 
$img_web = '<img title="Domains" border="0" hspace="2" src="' . JURI::root( false ).'/images/M_images/weblink.png" width="16" height="16" align="absmiddle">'; 
$img_eml = '<img title="Email" border="0" hspace="2" src="' . JURI::root( false ).'/images/M_images/emailButton.png" width="16" height="16" align="absmiddle">'; 
$img_mnu = '<img border="0" hspace="8" vspace="5" src="' . JURI::root( false ) . '/administrator/images/expandall.png" align="absmiddle" width="13" height="13">';
$img_copy = '<img title="Copy" border="0" hspace="2" src="' . JURI::root( false ) . '/components/com_virtuemart/shop_image/ps_image/copy_f2.gif" align="absmiddle" width="19" height="19">';
$img_info = '<img title="Info" border="0" hspace="2" src="' . JURI::root( false ) . '/components/com_virtuemart/shop_image/ps_image/log_info.png" align="absmiddle" width="22" height="22">';
$img_file = '<img border="0" src="' . JURI::root( false ) . '/administrator/images/download_f2.png" align="absmiddle" width="32" height="32">';


function showFileExists($sPath) { 
  $str = showYesNoImg(file_exists(JPATH_SITE . $sPath));
  $str .= "&nbsp;&nbsp;";
  $str .= $sPath;
  return $str;
}

function showYesNoImg($bln) { 
	global $img_ok, $img_no;
	if ($bln == '1') 
	  $img = $img_ok;
	else
	  $img = $img_no;
	return $img;	
} 


// function used as an error handler 
function option_explicit($errno, $errstr, $errfile, $errline) {
  print "<br /><b>Fatal error</b>: $errstr in <b>$errfile</b>";
  print " on line <b>$errline</b><br />";	 
  exit();
}

function on_error_resume_next($errno, $errstr, $errfile, $errline) {
  global $myerr;
  $myerr = "$errstr Line: $errline";
}

// revert error handlng to default 
function default_handler($errno, $errstr, $errfile, $errline) {
  return false;
}

function error_display($errno, $errstr, $errfile, $errline) {
  print "<br />Error: $errstr ";	
}

//***************************************************

function update_payment_extrainfo($code, $info) {

  $payment_extrainfo = file_get_contents1($info);
  $queries = array();  
  $queries[] = "UPDATE #__vm_payment_method 
SET payment_extrainfo= '" . addslashes($payment_extrainfo) . "' 
WHERE payment_method_code='" . $code . "'; "; 
  
  $database = &JFactory::getDBO(); 
  $report = "";
  foreach($queries as $k=>$sql){    
    $report .= "Running SQL query (" . (1 + $k) . ")... \n";
    $report .= $sql . "\n\n";
    $database->setQuery($sql);
    $database->query();  
  }
  
  return $report;
}


// is express checkout enabled?
function is_payment_enabled($p_method_code) {
  $database = &JFactory::getDBO(); 
  $q = "SELECT payment_enabled FROM #__vm_payment_method WHERE payment_method_code='$p_method_code'"; 
  $database->setQuery( $q );
  $rows = $database->loadObjectList();
  $ret = '';
  if ($rows) {
    $row = $rows[0];
    $ret = $row->payment_enabled; 
  }
  return $ret; 
}

//***************************************************

function get_payment_method_id_from_code($p_method_code) {
  $database = &JFactory::getDBO(); 
  $q = "SELECT * FROM #__vm_payment_method WHERE payment_method_code='$p_method_code'"; 
  $database->setQuery( $q );
  $rows = $database->loadObjectList();
  
  $ret = 0;
  if($rows) {
    $row = $rows[0];
    $ret = $row->payment_method_id; 
  }
  return $ret; 
}


//***************************************************

// request 
function get_req($v){
  return trim(get_query($v) . post($v));
}

//***************************************************

// request querystring
function get_query($var){
  $val = '';
  if (isset($_GET[$var])) {
     $val = $_GET[$var];
  } 
  if (isset($_GET[strtoupper($var)])) {
     $val = $_GET[strtoupper($var)];
  } 
  if (isset($_GET[strtolower($var)])) {
     $val = $_GET[strtolower($var)];
  } 
  return trim($val);
}

//***************************************************

// request post
function post($var){
  $val = '';
  $chk_length = 0;
  $possible_array ='';

  // was it a get request?
  if (strtoupper($_SERVER['REQUEST_METHOD']) != 'GET') {
        
      if (isset($_POST[$var])) {
        $possible_array = $_POST[$var];
        $chk_length = count($possible_array);
      }

      // is it an arrayc?
      if (is_array($possible_array)) {

        // construct comma delimited string
        for ($i = 0; $i < $chk_length; $i++) {

          // add a comma if not first item
          if (trim($val) != '') $val .= ',';
          $val .= $possible_array[$i];
        } 

      } else {
        // is not array 
        $val = $possible_array;
      }
      
  } // get
  return trim($val);
}


//***************************************************

// strip chars
function removechars($s) {
  $r = '';
  for ($i=0;$i<strlen($s);$i++) {
    $c = substr($s, $i, 1);
    if ((is_numeric($c)) || ($c == '.'))
      $r .= $c;
  }
  return $r;
}


//***************************************************

function server($var){
  $val = '';
  if (isset($_SERVER[$var])) {
     $val = $_SERVER[$var];
   }
  return trim($val);
}

//***************************************************

function setsession($ky, $val) {
  $_SESSION[$ky] = $val;
}  

//***************************************************

// session
function session($var){
  $val = '';
  if (isset($_SESSION[$var])) {
     $val = $_SESSION[$var];
   }
  if (isset($_SESSION[strtoupper($var)])) {
     $val = $_SESSION[strtoupper($var)];
  } 
  return trim($val);
}

//***************************************************

// file_get_contents replacement
// file_get_contents is 4.0.0 - 4.3.0 and 5.0.0 +

function file_get_contents1($f) {
  if (file_exists($f)) {  
    return implode('', file($f));
  } else {
    // ("File does not exist!");
  }
}

//***************************************************

// write string to file

function write2file($doc, $outputstring ){
  write2fileMode($doc, $outputstring, "w");
}

//***************************************************


// write string to file with Mode

function write2fileMode($doc, $outputstring, $for_mode) {
  if ($doc == '') return;
  
  // Set file for opening
  $fp = fopen($doc, $for_mode);
  // Check if can write to file
  if (!$fp) {
     print "Could not open desired file for writing";
  } else {
     fwrite($fp, $outputstring);
  }
  // Close the written file
  fclose($fp);    
}


//***************************************************

// get a value from array
function getArrayValue($prms, $var){
  $prms_lower = array_change_key_case($prms);
  $val = '';
  if (isset($prms_lower [ strtolower($var) ] )) {
    $val = $prms_lower[strtolower($var)];
  } 
  return trim($val);
}
function getTagValue($prms, $var){
  $val = '';
  if (isset($prms[$var])) {
    $val = $prms[$var];
  } 
  if (isset($prms[strtoupper($var)])) {
    $val = $prms[strtoupper($var)];
  } 
  return $val;
}

//***************************************************

function KeyPairtoArray($str) {
	$result = $str;
 	$ary = array();
	while(strlen($result)){
		// name
		$keypos= strpos($result,'=');
		$keyval = substr($result,0,$keypos);
		// value
		$valuepos = strpos($result,'&') ? strpos($result,'&'): strlen($result);
		$valval = substr($result,$keypos+1,$valuepos-$keypos-1);
		// decoding the respose
		$ary[$keyval] = $valval;
		$result = substr($result,$valuepos+1,strlen($result));
	}
	return $ary;
}

//***************************************************

function ccsession($var){
  $val = '';
  if (isset($_SESSION['ccdata'][$var])) {
     $val = $_SESSION['ccdata'][$var];
   }
  return trim($val);
}

//***************************************************

// get country name from code  3
function country_3_name($cd3) {
	$database = &JFactory::getDBO(); 
	$q  = "SELECT country_name FROM `#__vm_country` WHERE ( country_3_code = '" . $cd3 . "')";
	$database->setQuery( $q );
	$res = $database->loadResult();
	return $res;
}

//***************************************************

// get country code 3 from 2
function country_code_2_3($cd) {
	$database = &JFactory::getDBO(); 
	$q  = "SELECT country_3_code FROM `#__vm_country` WHERE ( country_2_code = '" . $cd . "')";
	$database->setQuery( $q );
	$res = $database->loadResult();
	return $res;
}

//***************************************************

// get country code 2 from 3
function country_code_3_2($cd) {
	$database = &JFactory::getDBO(); 
	$q  = "SELECT country_2_code FROM `#__vm_country` WHERE ( country_3_code = '" . $cd . "')";
	$database->setQuery( $q );
	$res = $database->loadResult();
	return $res;
}


//***************************************************

// get state code 2 from 3
function state_code_2_name($sn) {
	$database = &JFactory::getDBO(); 
	$q  = "SELECT state_2_code FROM `#__vm_state` WHERE ( state_name = '" . $sn . "')";
	$database->setQuery( $q );
	$res = $database->loadResult();
	return $res;
}

//***************************************************

function getInstallXMLFiles($p, $L_filesect, $L_filetag) { 
  $files = array();
  $filesect = strtoupper($L_filesect);
  $filetag = strtoupper($L_filetag);
  
  $xmlFile = $p->THIS_OPTION . '.xml';
  $xmlFile = JPATH_SITE  . DS . 'components' . DS . $p->THIS_OPTION . DS . $xmlFile;
  $xmlStr = file_get_contents1($xmlFile);  
  if($xmlStr == '') trigger_error("XML file not found: " . $xmlFile , E_USER_ERROR);
  
  $xml_parser = xml_parser_create();
  xml_parse_into_struct($xml_parser, $xmlStr, $vals, $index);
  xml_parser_free($xml_parser);  
  
  if(!isset($index[$filetag])) trigger_error($filetag . " tag not found in XML: " . $xmlFile , E_USER_ERROR);
  if(!isset($index[$filesect])) trigger_error($filesect . " tag not found in XML: " . $xmlFile , E_USER_ERROR);
  
  $exfNames = $index[ $filetag ];
  $exfFolders = $index[ $filesect ];
  if(count($exfNames) == 0) trigger_error("files not found in XML: " . $xmlFile , E_USER_ERROR);
    
  for ($i=0;$i<count($exfNames);$i++) {
  if(isset( $vals[$exfFolders[$i]]['value'] ))
    $files[ $vals[$exfNames[$i]]['value'] ] =  ToSafeDS( $vals[$exfFolders[$i]]['value'] );  
  }  
  return $files;  
}

function getExtraFiles($p) { 
  $files = array();
  
  $xmlFile = $p->THIS_OPTION . '.xml';
  $xmlFile = JPATH_SITE  . DS . 'components' . DS . $p->THIS_OPTION . DS . $xmlFile;
  $xmlStr = file_get_contents1($xmlFile);  
  if($xmlStr == '') trigger_error("XML file not found: " . $xmlFile , E_USER_ERROR);
  
  $xml_parser = xml_parser_create();
  xml_parse_into_struct($xml_parser, $xmlStr, $vals, $index);
  xml_parser_free($xml_parser);  
  
  if(!isset($index['EXTRAFILENAME'])) trigger_error("EXTRAFILENAME tag not found in XML: " . $xmlFile , E_USER_ERROR);
  if(!isset($index['EXTRAFOLDER'])) trigger_error("EXTRAFOLDER tag not found in XML: " . $xmlFile , E_USER_ERROR);
  
  $exfNames = $index['EXTRAFILENAME'];
  $exfFolders = $index['EXTRAFOLDER'];
  if(count($exfNames) == 0) trigger_error("EXTRA files not found in XML: " . $xmlFile , E_USER_ERROR);
    
  for ($i=0;$i<count($exfNames);$i++) {
    $files[ $vals[$exfNames[$i]]['value'] ] =  ToSafeDS( $vals[$exfFolders[$i]]['value'] );  
  }  
  //for ($i=0;$i<count($files);$i++) {
  //  $files[$i] = ToSafeDS($files[$i]) ;  
  //}     
  return $files;  
}

function ToSafeDS($sPath){
  $sPth = trim($sPath);
  $sPth = str_replace('\\' , DS , $sPth);
  $sPth = str_replace('/' , DS , $sPth);
  return $sPth;
}

function myCopy( $srcfolder, $destfolder, $afile){ 
  
  //$Installer = new mosInstaller();
  jimport( 'joomla.filesystem.file' );
  global $mainframe;
    
  $copy_str = '';  
  $src = remove_trailing_slash(JPATH_SITE . $srcfolder) . '/';  
  $dest = JPATH_SITE . $destfolder;
  
  // already exists?
  if (file_exists($dest . $afile)){    
      // copy to backup folder
      JFile::copy( $dest . $afile, $src . 'backup' . DS . $afile ); 

      //$copy_str .= 'Backed up: ';   
      //$copy_str .= $destfolder . $afile; 
  } 


  if (file_exists($src . $afile)){  
    // copy to folder
    JFile::copy($src . $afile, $dest . $afile );
    
    if (file_exists($dest . $afile)){    
      $copy_str .= 'Copied: ';    
      $copy_str .= $afile; 
    } else {
      $copy_str .= 'Could not copy: '; 
      $copy_str .= $afile;   
    }
    
  } else {
    $copy_str .= 'Source missing: '; 
    $copy_str .= $afile;   
  }

  return $copy_str;  
}


// public fn
function dohttp($method, $url, $port, $vars) {
  if (trim($method) == '') return;
  if (trim($url) == '') return;
 
  if(function_exists("curl_init") )
    return doCurlhttp($method, $url, $port, $vars);
  else 
    return doFsockhttp($method, $url, $port, $vars);
}

// curl fn
function doCurlhttp($method, $url, $port, $vars) {
  if (trim($method) == '') return;
  if (trim($url) == '') return;

  $ch = curl_init();
  if($ch) {
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_PORT, $port);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_USERAGENT, "PHP/Curl");	
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    if ( (trim(ini_get('open_basedir')) == '') && (ini_get('safe_mode') != 1) ) {
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt');
      curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie.txt');
    }

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, TRUE);
	
    if ($method == 'POST') {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
    }
    $data = curl_exec($ch);
    curl_close($ch);
    if ($data) { 
        return $data;
    } else {
        return curl_error($ch);
    }
  }
}

function doFsockhttp($method, $url, $port, $vars) {

    // add default protocol if not set
    if( substr(strtolower($url), 0, 4) != 'http' ) {
      if( $port == 443 )
        $url = 'https://' . $url;
      else
        $url = 'http://' . $url;
    }

    // add default port if not set
    if($port == "") {
      if( substr(strtolower($url), 0, 8) == 'https://' ) $port = 443;
      if( substr(strtolower($url), 0, 7) == 'http://' ) $port = 80;
    }

    $url_parts = parse_url($url);
 
            $out = '';
			
			$page = $url_parts['path'];
            if(strtolower($method) == 'post' ) {
                $out .= "POST $page HTTP/1.1\r\n";
            } else {	
                if(isset($url_parts['query'])) $page .= '?' . $url_parts['query'];
                $out .= "GET $page HTTP/1.0\r\n"; //HTTP/1.0 is much easier to handle than HTTP/1.1
            }
            $out .= "Host: $url_parts[host]\r\n";
            $out .= "Accept: text/*\r\n";
            $out .= "User-Agent: PHP/fsockopen\r\n";
            $out .= "Connection: Close\r\n";
            
            //HTTP Basic Authorization support
            if(isset($url_parts['user']) and isset($url_parts['pass'])) {
                $out .= "Authorization: Basic ".base64_encode($url_parts['user'].':'.$url_parts['pass']) . "\r\n";
            }

            //If the request is post - pass the data in a special way.
            if(strtolower($method) == 'post' ) {
                $out .= "Content-Type: application/x-www-form-urlencoded\r\n";
                $out .= 'Content-Length: ' . strlen($vars) . "\r\n";
                $out .= "\r\n" . $vars;
            }
            $out .= "\r\n";
            //echo $out;
            $response = '';
			
			
        $fp = fsockopen('ssl://' . $url_parts['host'], $port, $errno, $errstr, 30);
        if ($fp) {
			
            fwrite($fp, $out);
            while (!feof($fp)) {
                $response .= fgets($fp, 128);
            }
            fclose($fp);
        }
		
        //Seperate header and content
        $separator_position = strpos($response,"\r\n\r\n");
        $header_text = substr($response,0,$separator_position);
        $body = substr($response,$separator_position+4);
        
        foreach(explode("\n",$header_text) as $line) {
            $parts = explode(": ",$line);
            if(count($parts) == 2) $headers[$parts[0]] = chop($parts[1]);
        }
		
    //return $response;
    return $body;
}

function str_to_xml_params( $res_array ){
   // is it xml?
  $xml_parser = xml_parser_create();
  xml_parse_into_struct($xml_parser, $res_array, $vals, $index);
  xml_parser_free($xml_parser);

  // put into arry
  $params = array();
  $i = 0;
  foreach ($vals as $xml_elem) {
    if ($xml_elem['type'] == 'complete') {
        $params[trim($xml_elem['tag'])] = isset($xml_elem['value'])? trim($xml_elem['value']): '';
    } 
  }
  
   return $params;
}

// remove trailing slash 

function remove_trailing_slash($p) {
if (substr($p, -1, 1) == '/')  
  $p = substr($p, 0, -1); 
return $p;
}


// convert array
// to string key=value&key2=test
function ArraytoKeyPair($ary) {
  $kr ='';
  foreach($ary as $k=>$v)
    $kr .=  '&' . $k . '=' . urlencode($v);
    //$kr .=  '&' . $k . '=' . trim($v);
  return substr($kr, 1);
}

function getInput() {
$q = isset($_SERVER['QUERY_STRING']) ? trim($_SERVER['QUERY_STRING']) : '';
if ($q == '')
  return getRawPost();
else
  return $q;
}

function getRawPost() {
  $output = '';
  $fp = fopen("php://input", "r");  

  // while content exists, keep retrieving in chunks
  while( !feof( $fp ) ) {
        $output .= fgets( $fp, 1024);
  }
  // close the socket connection
  fclose( $fp);

  // return the response
  return $output;
}

function CheckAccess($Allowed) {
  global $your_ip;
  
  $access_ok = 0;
  foreach ($Allowed as $fld=>$value) {  
    if($value != '') {
		$value = trim(str_replace('.*', '.', $value));	
		if (strpos($your_ip, '..') === false )
		if (strpos($your_ip, '-') === false )
		if (strpos($your_ip, $value) !== false )
		  $access_ok = $value;
		
	} //$value
  }
  // return the ip if found otherwise 0
  return $access_ok;  
}	  
function CleanCommaList($dlist){
  $nlist = trim($dlist);
  $nlist = str_replace("\n", ',', $nlist);
  $nlist = str_replace("\r", ',', $nlist);
  $nlist = str_replace(' ', '', $nlist);
  
  while (strpos($nlist, ',,') !== false)
    $nlist = str_replace(',,', ',', $nlist);
	
  $nlist = str_replace(',', ', ', $nlist);
  return($nlist);
} 

function show_db_err() {
	$database = &JFactory::getDBO(); 
	if ($database->getErrorNum()) {
		echo $database->getQuery();
		echo $database->stderr();
		return false;
	}
}

function options_from_query($q, $s) {
	$database = &JFactory::getDBO(); 
	$str = '';

	$database->setQuery( $q ); 
	//echo($database->getQuery( ) ); 
	$res = $database->loadObjectList();
	show_db_err();
		
    if(is_array($res)) {
	foreach($res as $i=>$j)  {
	  $str .= '<option ';
	  // must be a better way than this to show first 2 props of stdClass?
	  $z = 0;
	  foreach($j as $k=>$v)  {
	    if ($z==0) {
	      if($v == $s) $str .= ' selected ';
	      $str .= ' value="' . $v . '">';		  
		} else  {
	      $str .= $v . ' ';	
		}
		$z++;
	  }
	  $str .= ' </option>';	  
	  	 
	}
	}
	return $str;
}
function options_from_array($res, $value, $usekeys = false) { 
  $str = '';  
		
  if(is_array($res)) {
	foreach($res as $i=>$j)  {
	  $str .= '<option ';

	  $ind = $i;
	  if($usekeys )$ind = $j;

	if( is_array( $value )) {
		if( in_array( $i, $value )) $str .= "selected=\"selected\"";
	} else {
		if(strtolower($value) == strtolower($i) ) $str .= "selected=\"selected\"";
	}


	  $str .= ' value="' . $ind . '">';	
	  $str .= $j . ' ';	

	  $str .= ' </option>';	  
	  $str .= "\r\n";	  
	  	 
	}
  }
  return $str;
}

function ShortenString($s, $c)	 {
    $ret = '';
	$ret = substr($s, 0, $c);
    if (strlen($s) > $c) $ret .= "...";
    return $ret;
}

function defconn(){
  global $mosConfig_host, $mosConfig_user, $mosConfig_password, $mosConfig_db;
  $joomla_link=mysql_connect($mosConfig_host, $mosConfig_user, $mosConfig_password);
  mysql_select_db($mosConfig_db, $joomla_link);
}
	
// is this mysql server version x?	
function is_mysqlv($x) {
	$mysqlversion = mysql_get_server_info();
    $mysqlv = substr($mysqlversion, 0, strpos($mysqlversion, "-"));
	return (substr($mysqlv, 0, 1) == $x);
}

function table_exists ($table) { 

  $conf =& JFactory::getConfig();
  $dbname = $conf->getValue('config.db');

	$tables = mysql_list_tables ($dbname); 
	while (list ($temp) = mysql_fetch_array ($tables)) {
		if ($temp == $table) {
			return TRUE;
		}
	}
	return FALSE;
}

function list_order_status($s){
  $q = "SELECT order_status_code, order_status_name FROM #__vm_order_status ORDER BY list_order";
  return options_from_query($q, $s);
}

function getVirtueMartVersion($short = false) { 

	$vm_version = '';	
	$vm_vsn_file = JPATH_SITE.'/administrator/components/com_virtuemart/version.php';
	$vm = file_exists( $vm_vsn_file );  
	if ($vm) {
		require_once($vm_vsn_file);
		$VMVERSION = new vmVersion(); 
		$vm_version = $VMVERSION->RELEASE;
		if(!$short) $vm_version .= " " . $VMVERSION->DEV_STATUS . " [". $VMVERSION->CODENAME . "]";
	}
    return $vm_version;
}
	
function ShowVersionInfoPanel($p = NULL) { 

	if ($p == NULL) {
		$str = 'For: ' .  THIS_VERSION_FOR . '<br> 
		&copy; ' .  THIS_COPYRIGHT . '<br>
		Vendor: ' .  THIS_VENDOR . '<br>
		Email: ' .  THIS_SUPPORT_EMAIL . '<br>
		Website: ' .  THIS_SUPPORT_WEBSITE . '<br>
		Release Date: ' .  THIS_VERSION_DATE . '<br>
		Version: ' .  THIS_VERSION . '<br>';
	} else {		
		$str = 'Platform: ' .  $p->THIS_VERSION_FOR . '<br>
Copyright ' .  $p->THIS_COPYRIGHT . '<br>
Vendor: ' .  $p->THIS_VENDOR . '<br>
Website: <a target="_blank" href="' .  $p->THIS_VENDOR_WEBSITE . '">' .  $p->THIS_VENDOR_WEBSITE . '</a><br>
Support: <a target="_blank" href="' .  $p->THIS_SUPPORT_WEBSITE . '">' .  $p->THIS_SUPPORT_WEBSITE . '</a><br>
Installed Version: ' .  $p->THIS_VERSION . '<br>
Version Date: ' .  $p->THIS_VERSION_DATE . '<br>
';
	}
	return $str; 
} 

function ShowLicenceInfoPanel($payment_method) {

  if($payment_method->THIS_LICENCE == 'GPL') { 


  } else {

	// include licence
	global $mainframe;
	global $licence;
	
	global $mosConfig_absolute_path; 	
	require_once(JPATH_BASE . DS . 'components' . DS. $payment_method->THIS_OPTION . DS. 'generic_fns.php');
	require_once(JPATH_BASE . DS . 'components' . DS . $payment_method->THIS_OPTION . DS . 'licencecheck.e.php');    

	$document = &JFactory::getDocument();
	$document->addScript( JURI::root( false ) . 'administrator/components/' . $payment_method->THIS_OPTION . '/licencewin.js' );

	$mylic_info = $licence->EL_get_info(); 
	if(!isset($mylic_info['key'])) $mylic_info['key'] = ''; 
	if(!isset($mylic_info['vsn'])) $mylic_info['vsn'] = ''; 
	if(!isset($mylic_info['exp'])) $mylic_info['exp'] = time()-1; 
	
	$warn_latest = ($mylic_info['vsn'] != $payment_method->THIS_VERSION);
	$warn_licence = ($licence->EL_need_input());
	$warn_expired = (intval( $mylic_info['exp'] ) < time());

	$task = JRequest::getCmd('task');
	if( $task != 'welcome') { 
		if($warn_licence)
		  $mainframe->enqueueMessage( $licence->EL_description, 'error'); 
		else
		{		  
			if($warn_latest)
			    $mainframe->enqueueMessage( "An updated version is available. ", 'message'); 

			if( ($task == 'support') || ($task == 'updates')) { 
				if($warn_expired)
				  $mainframe->enqueueMessage( "Your support period for technical support and upgrades has expired. ", 'message'); 
			}
		}	
	}
	
	?>	
	
	<?php 
	echo showYesNoImg(!$warn_latest); 						
	?>	
	Latest Version Installed:     
	<?php 
		if($mylic_info['vsn'] != '') echo $mylic_info['vsn']; 
		else echo 'Unknown';
	?>
	<br />
	
	<?php echo showYesNoImg( !$warn_expired ); ?>
	Support Period: 
	<?php echo date("Y.m.d",  $mylic_info['exp'] ); ?>
	<br />			  
	
	<?php echo showYesNoImg(!$warn_licence); ?>
	Licence Status: 
	<a href="javascript:EL_open_Manager('<?php echo $payment_method->THIS_OPTION; ?>');">
	<b><?php echo $licence->EL_description; ?></b></a>			  
	<?php // echo $mylic_info['key']; ?>
			  
<?php
  }

}
			  
function InstallCheckFile( $c, $f ){
  global $img_no;
  global $img_ok;
  
  // does it exist?
  if(!file_exists(JPATH_SITE . $f)) return $img_no . '&nbsp; File does not exist: ' . $f;
  
  $path_parts = pathinfo($f); 
  if( $path_parts['extension'] == 'php') {
    $str = file_get_contents1(JPATH_SITE . $f);
    if(strpos($f, '.cfg.php') === false )
    if(strpos($str, trim($c->THIS_CODEWORD)) === false )
	  return $img_no . '&nbsp; File needs updating: ' . $f;
  }
  // does it contain the code
  return $img_ok . '&nbsp; ' . $f; 
}

function get_basket_items($oid, $fmt = 'paypal', $tax = false) {
	$db1 = new ps_DB();
	
	$q = "SELECT 
		order_item_sku,
		order_item_name,
		product_quantity,
		product_item_price,
		product_final_price,
		order_item_currency,
		product_attribute
		FROM #__vm_order_item WHERE order_id='". $oid ."'";
		
	$db1->query($q);
	$items = $db1->record; 
	
	$result = array();
	$result['paypal'] = array();
	$result['sagepay'] = '';
	
	foreach($items as $k=>$v){
	  $i = $k+1;
	
	  $result['paypal']['item_name_'. $i] = $v->order_item_name;
	  $result['paypal']['quantity_'. $i] = $v->product_quantity;
	  $result['paypal']['item_number_'. $i] = $v->order_item_sku;
	  $result['paypal']['amount_'. $i] = $v->product_final_price; 
	  
	  $tax = $v->product_final_price - ($v->product_quantity * $v->product_item_price);
	  
	  if( $tax == 1 ){
	  	$result['sagepay'] .=  ":" . $v->order_item_name . ":\r\n" . 
	  				$v->product_quantity . ":\r\n" . 
	  				sprintf ("%01.2f", $v->product_item_price ). ":\r\n" . 
	  				sprintf ("%01.2f", $tax ) . ":\r\n" . 
	  				sprintf ("%01.2f", ($v->product_final_price - $tax) ) . ":\r\n" . 
	  				sprintf ("%01.2f", $v->product_final_price ). "\r\n" ; 
	  } else {
	  	$result['sagepay'] .=  ":" . $v->order_item_name . ":\r\n" . 
	  				$v->product_quantity . ":\r\n" . 
	  				sprintf ("%01.2f", ($v->product_final_price / $v->product_quantity) ). ":\r\n" . 
	  				sprintf ("%01.2f", 0 ) . ":\r\n" . 
	  				sprintf ("%01.2f", $v->product_final_price ) . ":\r\n" . 
	  				sprintf ("%01.2f", $v->product_final_price ). "\r\n" ; 
	  }			

	  $attr = explode('<br/>',$v->product_attribute);
	  foreach($attr as $j=>$y){
		  $result['paypal']['on' . $j . '_' . $i] = trim(substr($y, 0, strpos($y, ':')), " ");
		  $result['paypal']['os' . $j . '_' . $i] = trim(substr_replace(substr_replace($y, "", 0, strrpos($y, ':')), "", 0, 2));
	  }
	  
	}
	$result['sagepay'] = $i . $result['sagepay'];
	
	return $result[$fmt];
}

// get an vm count from db
function get_vm_count_tbl($tbl)  {
	$database = &JFactory::getDBO(); 	
	$sql = "SELECT COUNT(*) FROM " . $tbl . "; ";
	$database->setQuery( $sql );
	return $database->loadResult();
} 

function mail_Send( $mail_From, $mail_FromName, $mail_To, $mail_Subject, $mail_Body, $mail_silent = false) {
	global $vmLogger, $VM_LANG;
	
	$mail_Body = html_entity_decode($mail_Body);

	if (function_exists('vmMail') )
		$result = vmMail( $mail_From, $mail_FromName, $mail_To, $mail_Subject, $mail_Body, '');
	else {
		$result = false; 
		echo '<b>vmMail does not exist</b>';
	}

	//mail ( $mail_To , $mail_Subject, $mail_Body , 'From: ' . $mail_From . "\r\n" );
	//mail ( string $to , string $subject , string $message )

	if ($result) {
		//$vmLogger->info( $VM_LANG->_('PHPSHOP_DOWNLOADS_SEND_MSG',false) . ' ' . $mail_To );
		if (!$mail_silent) echo $VM_LANG->_('PHPSHOP_DOWNLOADS_SEND_MSG',false) . ' ' . $mail_To ;
	} else {
		//$vmLogger->warning( $VM_LANG->_('PHPSHOP_DOWNLOADS_ERR_SEND',false) . ' ' . $mail_To );
		if (!$mail_silent) echo $VM_LANG->_('PHPSHOP_DOWNLOADS_ERR_SEND',false) . ' ' . $mail_To ;
	}
	return $result;
}

function send_debug_email($pm, $dest_email, $body)  {				
		global $vendor_mail;
		global $vendor_name;

		$mail_Subject = "DEBUG: Order at " . JURI::root(false); 
		
		$info = "The following transaction details were received. \r\n";
		$info .= "\r\n";
		$info .= "Time: ". date("Y-m-d G:i:s", time()) . "\n";
		$info .= "Website: ". JURI::root() . "\n";
		$info .= "Path: ". JPATH_SITE  . "\n";
		$info .= "URL: ". $_SERVER['REQUEST_URI'] . "\n";
 		$info .= "Query: ". $_SERVER['QUERY_STRING'] . "\n";
		$info .= "\n";
 	
		// component Version details
		// its already written to screen as its the xml description
		$info .= strip_tags(ShowVersionInfoPanel($pm));
		$info .= "\n";
		
		// Version details
		$version = new JVersion();  
		$info .= "Joomla: ".  $version->getShortVersion() . "\n";
		$info .= "VirtueMart: ".  getVirtueMartVersion(true) . "\n";
		$info .= "PHP: ".  phpversion() . "\n";
		$info .= "MySQL: ".  mysql_get_server_info() . "\n";
		$info .= "\n";
		$info .= "-------------------------------------------------------\n";
		$info .= "\n";  
		$info .= $body;
												
		mail_Send( $vendor_mail, $vendor_name, $dest_email, $mail_Subject, $info, true ); 	
}
			
function do_success_scripts() {
	global $order_id; 
	global $mosConfig_sitename;
	global $mosConfig_mailfrom;
	global $mosConfig_fromname; 
	global $mosConfig_lang; 
	global $vendor_currency;
	global $vendor_image_url;
	global $vendor_mail; 
	global $vendor_name; 
	global $vendor_store_name; 
	global $your_ip;
	global $hostname;
	
	//out
    global $order;
    global $product;
	
	$success_scripts = array( 'all' );	 
	
	// get cust	
    $qcust = "SELECT * FROM #__{vm}_orders o, #__{vm}_user_info u ";
    $qcust .= " WHERE o.order_id='" . $order_id . "' ";
    $qcust .= " AND o.user_id = u.user_id; ";	
    $dbcust = new ps_DB;
	$dbcust->query($qcust);
	if ($dbcust->next_record()) $order = $dbcust->record[0];
	
	$qcart = "SELECT product_id, order_item_sku FROM #__{vm}_order_item WHERE #__{vm}_order_item.order_id='$order_id' ";
	$dbcart = new ps_DB;
	$dbcart->query($qcart);
	while ($dbcart->next_record()) {
	  $success_scripts[] = ereg_replace("[^A-Za-z0-9]", "", $dbcart->f("order_item_sku") );	  
	  $order->products = $dbcart->f("order_item_sku"); 
	}
	//$order->products = $success_scripts;
		
	foreach($success_scripts as $item_i=>$val_i) {
	    // get prod		
		$product = array();
		
		if($item_i > 0) {
	        $qprod = "SELECT * FROM #__{vm}_product WHERE product_sku='$val_i' ";
			$dbprod = new ps_DB;
			$dbprod->query($qprod);
			if ($dbprod->next_record()) $product = $dbprod->record[0]; 
			
			// get product_attribute
			$qprod = "SELECT product_attribute FROM #__{vm}_order_item WHERE order_item_sku='$val_i' ";
			$qprod .= " AND order_id='" . $order_id . "'; ";
			$dbprod->query($qprod);
			if ($dbprod->next_record()) {
			  // Size: big<br/> Power: 100W
			  $attr = trim( $dbprod->f("product_attribute"));
			  $attr = str_replace('<br/> ', '&', $attr);
			  $attr = str_replace(': ', '=', $attr);
			  
			  $product->product_attribute = KeyPairToArray( $attr );
			}
			
		} //$item_i
		
	    $success_i = "custom" . DS . "success_" . $val_i. ".php";	
		if( file_exists($success_i)) require_once($success_i);
	}
}


//jimport('joomla.filesystem.folder');
//$files = JFolder::files($path, '.sql');

function get_file_list($dir, $ext = NULL) {
  if(!$d = opendir($dir)) {
        trigger_error("Unable to open directory: $dir");
        return false;
  }

  $ret = array();
  while( ($i = readdir($d)) !== false ) {
        $e = array_pop(explode(".",$i));
        if ( ($ext == NULL) || ($ext == $e) )
          $ret[] = $i;
  }
  return $ret;
}

function ProperName( $string ) { 
  if ( trim($string) =='' ) return '';
  $string = str_replace( '_', ' ' , $string );
  $string = titlecase( $string );
  return trim($string);
}

function titlecase( $string ) { 
    $len = strlen( $string ); 

    $last = $new = ""; 
    $i = 0; 
         
    $string = strtoupper( $string ); 

    while( $i < $len ) { 
        $char = substr( $string, $i, 1 ); 

        if( ereg( "[A-Za-z0-9']", $last ) ) 
            $new .= strtolower( $char ); 
        else 
            $new .= strtoupper( $char ); 

        $last = $char; 
         
        $i++; 
    } 

    return( $new ); 
} 

function config_names( $ary, $d ) {

	$new_param = false;
	$my_array = array();
	foreach( $ary as $key => $value ) {
		if(!isset($d[$value])) { 
		  $d[$value] = '';
		  $new_param = true;
		}
		$my_array[$value] = $d[$value];
	}
	if($new_param) { 
	    global $mainframe;
	    $msg = 'Some settings were missing. Click Edit then Save to set defaults';
	    $mainframe->enqueueMessage($msg, 'error');  
	}        
	return $my_array;
}


function check_config($ary) {
		$missing = '';		
		foreach( $ary as $key => $value ) {
			if(!defined($value)) { 
			    define($value, '');
				$missing = $value;
			}
		}
		if($missing != '') {
				global $mainframe;
				$msg = 'Some settings are not yet saved in the config file. Click Save or Apply ';
				$mainframe->enqueueMessage($msg, 'message');  		
		}

}


function show_in_textbox($txt) {
?>
  <input type="text" style="width:90%;font-size:11px;" 
  onClick="this.focus();this.select();" 
  READONLY
  value="<?php echo $txt; ?>">
<?php
}

function ShowCpanelItem($t, $h, $i) {
?> 
<td width="33%"> 
<div align="center" style="background-color:#FFFFFF;border:1px solid #CCCCCC; "> 
        <a 
		  href="<?php echo $h; ?>"><img 
		  src="<?php echo $i; ?>" 
		  width="48" height="48" border="0" vspace="2"><br/>
<b><?php echo $t; ?></b></a> </div>
</td>
<?php
}


} 
//if(!defined('generic_fns')) 

?>