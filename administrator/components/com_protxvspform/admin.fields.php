<?php
// *************************************************************************
// * Filename: admin.fields.php                                            *
// * Code Word: Srednekolymsk                                              *
// *************************************************************************
?> 
   
<table class="adminform">
  <tr valign="top"> 
    <td width="150"><b>General</b></td>
    <td colspan="3">These settings apply to all transactions</td>
  </tr>
  <tr> 
    <td align="left"><b><strong>Vendor Name</strong></b></td>
    <td align="left"> 
      <input type="text" name="PXFM_VENDOR" class="inputbox" value="<?php  echo PXFM_VENDOR ?>" />
    </td>
    <td align="left">&nbsp;</td>
    <td align="left"> Sage Pay Vendor Name e.g. testvendor</td>
  </tr>
  <tr> 
    <td align="left"><strong> Test Mode </strong></td>
    <td align="left"> 
      <select name="PXFM_TEST" class="inputbox" >
        <option <?php  if (PXFM_TEST == 'SIM') echo "selected=\"selected\""; ?> value="SIM"> 
        Simulator </option>
        <option <?php  if (PXFM_TEST == 'TEST') echo " selected "; ?> value="TEST"> 
        Test </option>
        <option <?php  if (PXFM_TEST == 'LIVE') echo " selected "; ?> value="LIVE"> 
        Live </option>
      </select>
    </td>
    <td align="left">&nbsp;</td>
    <td align="left">The Encryption Key must correspond to the mode.<br />
      We recommend you first try Simulator mode<br>
      See <a 
href="index2.php?option=<?php echo $payment_method->THIS_OPTION; ?>&amp;task=test">Testing</a> 
    </td>
  </tr>
  <tr> 
    <td align="left" valign="top"><b><strong> Encryption Key </strong></b></td>
    <td align="left" valign="top"> 
      <input type="text" name="PXFM_PASS" class="inputbox" value="<?php  echo PXFM_PASS ?>" />
    </td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">Merchant/Vendor encryption key<br />
      For the Simulator, use EEiWX0M40MVsDOic</td>
  </tr>
  <tr valign="top"> 
    <td><strong> Payment Type</strong></td>
    <td> 
      <select name="PXFM_PAYMENTTYPE" class="inputbox" >
        <option <?php  if (PXFM_PAYMENTTYPE == 'PAYMENT') echo "\"selected\""; ?> value="PAYMENT"> 
        PAYMENT </option>
        <option <?php  if (PXFM_PAYMENTTYPE == 'DEFERRED') echo "\"selected\""; ?> value="DEFERRED"> 
        DEFERRED </option>
      </select>
    </td>
    <td>&nbsp;</td>
    <td>PAYMENT or DEFERRED<br>
    </td>
  </tr>
  <tr> 
    <td align="left"><b><strong>Description </strong></b></td>
    <td align="left"> 
      <input type="text" name="PXFM_DESC" class="inputbox" value="<?php  echo PXFM_DESC ?>" />
    </td>
    <td align="left">&nbsp;</td>
    <td align="left"> Order Description (the order Id will be added to the end)</td>
  </tr>
  <tr> 
    <td align="left" valign="top"><strong> Apply AVS2 </strong></td>
    <td align="left" valign="top"> 
      <select name="PXFM_ApplyAVSCV2" class="inputbox" >
        <option <?php  if (PXFM_ApplyAVSCV2 == '0') echo "\"selected\""; ?> value="0"> 
        0 </option>
        <option <?php  if (PXFM_ApplyAVSCV2 == '1') echo "\"selected\""; ?> value="1"> 
        1 </option>
        <option <?php  if (PXFM_ApplyAVSCV2 == '2') echo "\"selected\""; ?> value="2"> 
        2 </option>
        <option <?php  if (PXFM_ApplyAVSCV2 == '3') echo "\"selected\""; ?> value="3"> 
        3 </option>
      </select>
    </td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top"><strong>0</strong> - If AVS/CV2 enabled then 
      check them. If rules apply, use rules. (default) <br />
      <strong>1</strong> - Force AVS/CV2 checks even if not enabled for the account. 
      If rules apply, use rules. <br />
      <strong>2</strong> - Force NO AVS/CV2 checks even if enabled on account. 
      <br />
      <strong>3</strong> - Force AVS/CV2 checks even if not enabled for the account 
      but DON&rsquo;T apply any rules. </td>
  </tr>
  <tr> 
    <td align="left" valign="top"><strong> Apply 3DSecure </strong></td>
    <td align="left" valign="top"> 
      <select name="PXFM_Apply3DSecure" class="inputbox" >
        <option <?php  if (PXFM_Apply3DSecure == '0') echo "\"selected\""; ?> value="0"> 
        0 </option>
        <option <?php  if (PXFM_Apply3DSecure == '1') echo "\"selected\""; ?> value="1"> 
        1 </option>
        <option <?php  if (PXFM_Apply3DSecure == '2') echo "\"selected\""; ?> value="2"> 
        2 </option>
        <option <?php  if (PXFM_Apply3DSecure == '3') echo "\"selected\""; ?> value="3"> 
        3 </option>
      </select>
    </td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top"><strong>0</strong> - If 3D-Secure checks are 
      possible and rules allow perform the checks and apply the authorisation 
      rules. (default)<br />
      <strong>1</strong> - Force 3D-Secure checks for this transaction if possible 
      and apply rules for authorisation.<br />
      <strong>2</strong> - Do not perform 3D-Secure checks for this transaction 
      and always authorise.<br />
      <strong>3</strong> - Force 3D-Secure checks for this transaction if possible 
      but ALWAYS obtain an auth code, irrespective of rule base.</td>
  </tr>
  <tr align="left" valign="top"> 
    <td align="left" valign="top"><strong> Auto-submit: </strong></td>
    <td align="left" valign="top"> 
      <select name="PXFM_AUTO" class="inputbox" >
        <option <?php  if (PXFM_AUTO == '0') echo " selected "; ?> value="0"> 
        No &nbsp;</option>
        <option <?php  if (PXFM_AUTO == '1') echo " selected "; ?> value="1"> 
        Yes &nbsp;</option>
      </select>
    </td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">(Applies to checkout page only)</td>
  </tr>
  <tr align="left" valign="top"> 
    <td align="left" valign="top"><strong> Button Text</strong></td>
    <td align="left" valign="top"> 
      <textarea name="PXFM_BTN_TEXT"
     class="inputbox" cols="40" rows="4"><?php echo PXFM_BTN_TEXT ?></textarea>
    </td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">Enter text to appear with button </td>
  </tr>
  <tr> 
    <td align="left" valign="top"><strong> Debug Mode </strong></td>
    <td align="left" valign="top"> 
      <select name="PXFM_DEBUG" class="inputbox" >
        <option <?php if (PXFM_DEBUG == '0') echo "selected=\"selected\""; ?> value="0"> 
        Off &nbsp; </option>
        <option <?php if (PXFM_DEBUG == '1') echo "selected=\"selected\""; ?> value="1"> 
        On </option>
      </select>
    </td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top"><b>Warning: </b>Your Merchant details will be 
      displayed on the checkout pages </td>
  </tr>
  <tr> 
    <td align="left"><b><strong>Vendor Email </strong></b></td>
    <td align="left"> 
      <input type="text" name="PXFM_VENDOREMAIL" class="inputbox" value="<?php  echo PXFM_VENDOREMAIL ?>" />
    </td>
    <td align="left">&nbsp;</td>
    <td align="left"> Optional Vendor email to pass to Sage Pay</td>
  </tr>
  <tr> 
    <td align="left" valign="top"><strong>Email Message</strong></td>
    <td align="left"> 
      <textarea name="PXFM_EMAILMSG"
     class="inputbox" cols="40" rows="4" id="PXFM_EMAILMSG"><?php echo PXFM_EMAILMSG; ?></textarea>
    </td>
    <td align="left">&nbsp;</td>
    <td align="left" valign="top">A message to the customer which is inserted 
      into the <br />
      successful transaction Sage Pay-generated e-mails only.</td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<br>