<?php
// *************************************************************************
// *                                                                       *
// * Product: Generic Run SQL Script                                       *
// * Release Date: 2010.07.03                                              *
// * Build Time: 3 July 2010 20:00                                         *
// * Version: 1.0.2                                                        *
// * Code Word: Srednekolymsk                                              *
// * For: Joomla 1.5/VirtueMart 1.1.x                                      *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * Author: E-commerce Solution                                           *
// * Email: support@virtuemart-solutions.com                               *
// * Website: http://www.virtuemart-solutions.com                          *
// * Copyright: (C) 2008 All rights reserved.                              *
// * Licence: Commercial                                                   *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * This software is furnished under a license and may be used and copied *
// * only  in  accordance  with  the  terms  of such  license and with the *
// * inclusion of the above copyright notice.  This software  or any other *
// * copies thereof may not be provided or otherwise made available to any *
// * other person.  No title to and  ownership of the  software is  hereby *
// * transferred.                                                          *
// *                                                                       *
// * You may not reverse  engineer, decompile, defeat  license  encryption *
// * mechanisms, or  disassemble this software product or software product *
// * license.  Ecom Solution  may  terminate  this license  if you fail to *
// * comply with any of the terms and conditions set forth in our end user *
// * license agreement (EULA).  In such event,  licensee  agrees to return *
// * licensor  or destroy  all copies of software  upon termination of the *
// * license.                                                              *
// *                                                                       *
// *************************************************************************

// find sql files

$sqlpath = JPATH_SITE . DS . 'administrator' . DS . 'components' . DS . $payment_method->THIS_OPTION . DS;

jimport('joomla.filesystem.folder');
$sqlfiles = JFolder::files($sqlpath, '.sql');
$sqlsuffix = '.' . $payment_method->THIS_COMPONENT . '.sql';

$sqlfile = get_req('sqlfile'); 

if(COMP_IS_PAYMENT) 
	$install_sql_name ='Insert payment method record';
else
	$install_sql_name = 'Main Install script';

$sql_options = '<option value=""> </option>'; 
if (in_array('install' . $sqlsuffix, $sqlfiles)) {
  $sql_options .= '<option ';
  if ($sqlfile == 'install') $sql_options .= ' selected ';
  $sql_options .= ' value="install">' . $install_sql_name . '</option>';
}
if (in_array('creditcards' . $sqlsuffix, $sqlfiles)) {
  $sql_options .= '<option ';
  if ($sqlfile == 'creditcards') $sql_options .= ' selected ';
  $sql_options .= ' value="creditcards">Insert credit card types</option>';
}
if (in_array('table' . $sqlsuffix, $sqlfiles)) {
  $sql_options .= '<option ';
  if ($sqlfile == 'table') $sql_options .= ' selected ';
  $sql_options .= ' value="table">Create table</option>';
}


// without $sqlsuffix
if (in_array('test-add.sql', $sqlfiles)) {
  $sql_options .= '<option ';
  if ($sqlfile == 'test-add') $sql_options .= ' selected ';
  $sql_options .= ' value="test-add">Add Test products</option>';
}
if (in_array('test-rem.sql', $sqlfiles)) {
  $sql_options .= '<option ';
  if ($sqlfile == 'test-rem') $sql_options .= ' selected ';
  $sql_options .= ' value="test-rem">Remove Test products</option>';
}

$sql_exists = false;
if($sqlfile != '') {
  $sqlfile = str_replace('/', '', $sqlfile);
  $sqlfile = str_replace('\\', '', $sqlfile);

  if( $sqlfile == 'test-add')
    $sqlfile .= '.sql' ; 
  elseif( $sqlfile == 'test-rem')
    $sqlfile .= '.sql' ; 
  else
    $sqlfile .= $sqlsuffix ; 

  $sql_exists = ( file_exists($sqlpath . $sqlfile) && is_file($sqlpath . $sqlfile));
}


$msg = "";
$result = array(); 
$result['sql_ok'] = ''; 
$result['sql_file_size'] = ''; 
$result['sql_num_queries'] = '';  
$result['sql_num_executed'] = ''; 

$btn = get_req('btn');  
if($sql_exists) {
  $result = DoSQL($sqlpath . $sqlfile, ($btn == ''));
  if ($btn != '') {
    if ($result['sql_ok'] == '') 
	  $msg = "SQL has been run successfully";
	else
	  $msg = "Error running SQL";
    $msg .= " - ". $result['sql_num_executed'] . " queries executed";
	$mainframe->redirect( 'index.php?task=diag&option='.$payment_method->THIS_OPTION , $msg);
  }
}

$infopath = '/administrator/components/' . $payment_method->THIS_OPTION . '/payment_extrainfo.txt' ;
?> 
<h2>Run SQL File </h2>
<p>This page enables you to manually run the SQL installation scripts.
<p> <strong>Note</strong>: If you run the SQL file which inserts the payment method, 
  you must <a href="index2.php?option=<?php echo $payment_method->THIS_OPTION; ?>&amp;task=extrainfo">reset 
  the Extra Info Code</a> 
<p>
      
<table width="95%">
  <form action="index2.php?option=<?php echo $payment_method->THIS_OPTION; ?>&task=sqlfile" method="post" target="_top">
    <tr> 
      <td width="140" valign="top">Select: </td>
      <td valign="top"> 
        <select 
onChange="location.href='index2.php?option=<?php echo $payment_method->THIS_OPTION; ?>&task=sqlfile&sqlfile=' + this.value;"
name="sqlfile">
          <?php echo $sql_options; ?>
        </select>
      </td>
    </tr>
    <tr> 
      <td width="140" valign="top" height="25">SQL File: </td>
      <td valign="top" height="25"> 
        <?php echo $sqlfile; ?>
        <?php if($sqlfile != '') echo showYesNoImg($sql_exists); ?>
        <?php if($sql_exists) echo $result['sql_file_size'] . ' bytes'; ?>
      </td>
    </tr>
    <tr> 
      <td valign="top">View SQL: </td>
      <td valign="top"> 
        <textarea
  wrap="off"
  name="textarea" cols="60" rows="7"
  readonly="READONLY"><?php if($sql_exists) echo file_get_contents1($sqlpath . $sqlfile); ?>
        </textarea>
      </td>
    </tr>
    <tr> 
      <td valign="top">SQL Queries: </td>
      <td valign="top"> 
        <?php echo $result['sql_num_queries']; ?>
      </td>
    </tr>
	
    <tr> 
      <td valign="top"> </td>
      <td valign="top"> 
        <input type="submit" name="btn" value=" Run SQL " />
      </td>
    </tr>
  </form>
</table>
      
    <p>    
      <strong><?php echo $msg; ?></strong>
    <p>    
	
<?php 


function DoSQL($sfile, $read_only) {	
    $result_array = array();
	
	$result_array['sql_ok'] = 0; 
	$result_array['sql_file_size'] = 0; 
	$result_array['sql_num_queries'] = 0;  
	$result_array['sql_num_executed'] = 0; 
		
	$sep_array = '/* split here */';
	$sql = '';
	
	// remove comments and blanks
	$file_content = file($sfile);
	foreach($file_content as $sql_line){
		 if(trim($sql_line) != "")
		 if(strpos($sql_line, "--") === false){ 
		   $sql .= trim($sql_line) . "\r\n";
		 }
	}
	// go thru entire string looking for semicolons OUTSIDE quotes
	$bln_InQuotes = false;
	$strOut = '';
	for ($q=0;$q<strlen($sql);$q++){    
	  $ch = $sql[$q];     
	  if ($ch=="'") {
		$bln_InQuotes = !$bln_InQuotes; 
	  }
	  if ($ch==";") 
		if (!$bln_InQuotes ) 
		  $ch=$sep_array;	  
	  $strOut .= $ch;  
	} 
	$result_array['sql_file_size'] = strlen($strOut); 
	
	// into array
	$queries = explode($sep_array, $strOut);
	
	// remove blanks again
	$queries_clean = array();
	foreach($queries as $k=>$sql){   
		if(trim($sql) != "") $queries_clean[] = trim($sql);	 
	}
	$result_array['sql_num_queries'] = count($queries_clean );  
	
	if ($read_only) return $result_array;
	
	// run queries
	foreach($queries_clean as $k=>$sql){    
		mysql_query($sql); 
	 
		if( mysql_errno() != 0)  {
			echo "<pre>\n";
			echo $sql . "\n\n";
			echo "\n";   
			echo "</pre>\n";
			echo ( "<strong>" . mysql_error() . "</strong>\n");
		}
		if($k > 1000) break; 
	    $result_array['sql_num_executed'] = $k+1; 
	}
	$result_array['sql_ok'] = 0;  
	return $result_array;
}

?>