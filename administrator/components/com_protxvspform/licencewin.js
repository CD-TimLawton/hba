
// *************************************************************************
// *                                                                       *
// * Ecom Solution WorldPay MD5 Payment Component for Virtuemart           *
// * Copyright (c) 2008 Ecom Solution LTD. All Rights Reserved             *
// * Email: support@virtuemart-solutions.com                               *
// * Website: http://www.virtuemart-solutions.com                          *
// * Script Name: licencewin.js                                            *
// * Release Date: 2010.07.03                                              *
// * Build Time: 3 July 2010 20:00                                         *
// * Version 1.5 for Joomla 1.5                                            *
// *                                                                       *
// *************************************************************************
// 

function EL_open_Manager(com){
  var s = "width=550, height=255, status=yes, toolbar=no, menubar=no, resizable=yes, scrollbars=no";
  licencenewwin = open('components/'+ com +'/licencenew.php?com=' + com + '&t='+(new Date()).getTime(), 'licencenew', s);
  if (parseInt(navigator.appVersion) >= 4) { licencenewwin.window.focus(); }
}

