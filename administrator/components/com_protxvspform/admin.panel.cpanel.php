<?php
// *************************************************************************
// * admin.panel.cpanel.php                                                *
// * Code Word: Srednekolymsk                                              *
// *************************************************************************

?> 

<table width="95%" cellspacing="3" cellpadding="3">


  <tr valign="middle" > 
<?php 
ShowCpanelItem(	'Product Updates', 
				'index2.php?task=updates&amp;option=' . $payment_method->THIS_OPTION, 
				'templates/khepri/images/header/icon-48-install.png'); 
ShowCpanelItem(	'Setup', 
				'index2.php?task=setup&amp;option=' . $payment_method->THIS_OPTION, 
				'templates/khepri/images/header/icon-48-cpanel.png'); 
ShowCpanelItem(	'Test', 
				'index2.php?task=test&amp;option=' . $payment_method->THIS_OPTION, 
				'templates/khepri/images/header/icon-48-checkin.png'); 
?> 

  </tr>
  <tr valign="middle" > 

<?php 
ShowCpanelItem(	'Diagnostics', 
				'index2.php?task=diag&amp;option=' . $payment_method->THIS_OPTION, 
				'templates/khepri/images/header/icon-48-config.png'); 
ShowCpanelItem(	'Support', 
				'index2.php?task=support&amp;option=' . $payment_method->THIS_OPTION, 
				'templates/khepri/images/header/icon-48-help_header.png'); 
ShowCpanelItem(	'More Components', 
				$payment_method->THIS_VENDOR_WEBSITE, 
				'templates/khepri/images/header/icon-48-frontpage.png'); 
?> 

  </tr>
</table>
<p>
