<?php
// *************************************************************************
// * Code Word: Srednekolymsk                                              *
// * For: Joomla 1.5/VirtueMart 1.1.x                                      *
// *************************************************************************

$lic_file = 'licence.txt';
if($payment_method->THIS_LICENCE == 'GPL') 
  $lic_file = 'gpl_short.txt'; 

?> 
<h2>
  <?php echo $payment_method->THIS_LICENCE; ?>
  Licence</h2>

<?php
  if($payment_method->THIS_LICENCE != 'GPL') {	
?>
Click the <b>Licence Status</b> Link in the right-hand panel to enter or release 
your licence <br>
FAQ: <a href="<?php echo $payment_method->THIS_SUPPORT_WEBSITE; ?>/kb?c=15" target="_blank">Licence 
Issues</a> <br>
<?php
  }
?>

<p> 
<pre>			
<?php echo(file_get_contents1(JPATH_SITE . DS . 'administrator' . DS . 'components' . DS . $payment_method->THIS_OPTION . DS . $lic_file)); ?>
</pre>
<br>
<a target="_blank" 
href="<?php echo JURI::root( true ) . '/administrator/components/'. $payment_method->THIS_OPTION . '/'. $lic_file; ?>">View 
the complete licence supplied with this program</a> <br>
