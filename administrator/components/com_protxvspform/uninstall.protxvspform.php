<?php
// *************************************************************************
// *                                                                       *
// * Product: Generic Install Script                                       *
// * Release Date: 2010.07.03                                              *
// * Build Time: 3 July 2010 20:00                                         * 
// * Version: 1.5.25                                                       *
// * Code Word: Srednekolymsk                                              *
// * For: Joomla 1.5/VirtueMart 1.1.x                                      *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * Author: E-commerce Solution                                           *
// * Email: support@virtuemart-solutions.com                               *
// * Website: http://www.virtuemart-solutions.com                          *
// * Copyright: (C) 2009 E-commerce Solution                               *
// * Licence: GPL                                                          *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * This program is free software: you can redistribute it and/or modify  *
// * it under the terms of the GNU General Public License as published by  *
// * the Free Software Foundation, either version 3 of the License, or     *
// * (at your option) any later version.                                   *
// *                                                                       *
// * This program is distributed in the hope that it will be useful,       *
// * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
// * GNU General Public License for more details.                          *
// *                                                                       *
// * You should have received a copy of the GNU General Public License     *
// * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
// *                                                                       *
// *                                                                       *
// *                                                                       *
// *************************************************************************


function com_uninstall() { 

  error_reporting(E_ALL); 
  
  jimport( 'joomla.filesystem.file' );

  $filesrc = DS . 'administrator' . DS . 'components' . DS . 'com_protxvspform'; 

  // include generic fns 
  require_once(JPATH_SITE . $filesrc . DS . 'generic_fns.php'); 

  // component settings     
  require_once(JPATH_SITE . $filesrc . DS . 'ps_protxvspform.php');  
  global $payment_method;
  $payment_method = new ps_protxvspform(); 
  
  $report = '<p>Thank you for using this component. The component has been uninstalled.<br> ';  
 

  // FILES ------------------------   
  $extrafiles = getExtraFiles($payment_method);
   
  $files_copied = '';
  foreach ($extrafiles as $k=>$v) {
    // v administrator\components\com_virtuemart\classes
    // k ps_checkout.php

    if($k != 'ps_protxvspform.php') {
    if($k != 'ps_protxvspform.cfg.php') {

      // replace original
      $src = JPATH_SITE . $filesrc . DS . 'backup' . DS . $k ;
      $dest = JPATH_SITE . DS . $v . DS . $k ;

      //echo $src . " -&gt; \n";       
      $files_copied .= DS . $v . DS . $k . "\n"; 

      JFile::copy( $src, $dest );  
    } 
    } 

  } // for

  if($files_copied != '') {
    $report .= '<br>The following original files have been restored <pre>' . $files_copied . '</pre>'; 

  }

  echo $report;
  return true;
}


?>