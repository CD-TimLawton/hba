<?php
// *************************************************************************
// *                                                                       *
// * Product: Generic Install Script                                       *
// * Release Date: 2010.07.03                                              *
// * Build Time: 3 July 2010 20:00                                         *
// * Version: 1.5.27                                                       *
// * Code Word: Srednekolymsk                                              *
// * For: Joomla 1.5/VirtueMart 1.1.x                                      *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * Author: E-commerce Solution                                           *
// * Email: support@virtuemart-solutions.com                               *
// * Website: http://www.virtuemart-solutions.com                          *
// * Copyright: (C) 2009 E-commerce Solution                               *
// * Licence: GPL                                                          *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * This program is free software: you can redistribute it and/or modify  *
// * it under the terms of the GNU General Public License as published by  *
// * the Free Software Foundation, either version 3 of the License, or     *
// * (at your option) any later version.                                   *
// *                                                                       *
// * This program is distributed in the hope that it will be useful,       *
// * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
// * GNU General Public License for more details.                          *
// *                                                                       *
// * You should have received a copy of the GNU General Public License     *
// * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
// *                                                                       *
// *                                                                       *
// *                                                                       *
// *************************************************************************

    
function com_install() {   
  
  error_reporting(E_ALL); 
   
  $filesrc = DS . 'administrator' . DS . 'components' . DS . 'com_protxvspform';
    
  // include generic fns 
  require_once(JPATH_SITE . $filesrc . DS . 'generic_fns.php');

  // component settings     
  require_once(JPATH_SITE . $filesrc . DS . 'ps_protxvspform.php');  
  global $payment_method;
  $payment_method = new ps_protxvspform(); 


  // file log
  $log = ""; 
  // screen
  $report = '';  
 
  $log .= "Installation Log\n";
  $log .= "Time: ". date("Y-m-d G:i:s", time()) . "\n";
  $log .= "Website: ". JURI::root() . "\n";
  $log .= "Path: ". JPATH_SITE  . "\n";
  $log .= "\n";

  // component Version details
  // its already written to screen as its the xml description
  $log .= strip_tags(ShowVersionInfoPanel($payment_method));
  $log .= "\n";
  
  // Version details
  $version = new JVersion();  
  $log .= "Joomla: ".  $version->getShortVersion() . "\n";
  $log .= "VirtueMart: ".  getVirtueMartVersion(true) . "\n";
  $log .= "PHP: ".  phpversion() . "\n";
  $log .= "MySQL: ".  mysql_get_server_info() . "\n";
  $log .= "\n";
  
  $log .= "Copying files to final destination... \n";
  $log .= "From: " . $filesrc . DS . " \n"; 
  $log .= "Backup Folder: " . $filesrc . DS . "backup \n"; 
  $log .= "\n";  
    
        
  // FILES ------------------------   
  $extrafiles = getExtraFiles($payment_method);

  // what checkout version?
  if(isset( $extrafiles['ps_checkout.php'] )) {
  
    $vm_vsn_sh = trim(getVirtueMartVersion(true));
    $log .= "Checkout version: " . $vm_vsn_sh . " - ";
    jimport( 'joomla.filesystem.file' );
	
    // replace ps_checkout with old version
    if($vm_vsn_sh == '1.1.3') {
      $log .= "Using ps_checkout.1609.php\n";
      JFile::copy( JPATH_SITE . $filesrc . DS . 'ps_checkout.1609.php', JPATH_SITE . $filesrc . DS . 'ps_checkout.php'); 
    }
    if($vm_vsn_sh == '1.1.2') { 
      $log .= "Using ps_checkout.1451.php\n";
      JFile::copy( JPATH_SITE . $filesrc . DS . 'ps_checkout.1451.php', JPATH_SITE . $filesrc . DS . 'ps_checkout.php'); 
    }	
	
  }

  $files_copied = false;
  foreach($extrafiles as $k=>$v){    
    // copy files  

    $res = myCopy($filesrc, DS . $v . DS, $k) . "\n"; 
    $log .= $res; 
    //$report .= $res .'<br>';

    $files_copied = true;
  } 

  $report .= '<br>Note: Some files have been copied to folders outside the component folders. ';
  $report .= 'Any files which have been overwritten have been backed up. ';
  $report .= 'See the <a href="components/'.$payment_method->THIS_OPTION.'/backup/install.txt" 
              target="_blank">Install Log</a> for more details.<br>'; 
  
  // DB --------------------------

  $payment_code_file = JPATH_SITE . $filesrc . DS . 'payment_extrainfo.txt';
  if(file_exists($payment_code_file))
    $log .= update_payment_extrainfo($payment_method->payment_code, $payment_code_file );
  
  // LOG  ------------------------
        
  $install_log = JPATH_SITE . $filesrc . DS . 'backup' . DS . 'install.txt'; 
  if (file_exists($install_log))
    write2file($install_log, $log);  
  else
    $report .= "<br>Could not write to install log: " . $install_log . "<br>\n";    
            
  // LINK ------------------------   
      
  //$report .= '<br>Click the link below to Continue to the component admin page';
  $report .= '<br><br>&nbsp;<a href="index2.php?task=welcome&amp;option='.$payment_method->THIS_OPTION.'" 
              style="font-size: 16px; font-weight: bold">Continue</a>&nbsp;<p><strong>';  

  //include(JPATH_SITE . $filesrc . DS . 'admin.page.intro.php');
  echo $report; 
       
  // RETURN ------------------------   
               
  return true;  
}


?>