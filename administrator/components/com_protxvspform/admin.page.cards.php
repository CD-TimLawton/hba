<?php
// *************************************************************************
// * admin.page.cards.php                                                  *
// * Code Word: Srednekolymsk                                              *
// *************************************************************************
?> 
<h2> Credit Cards </h2>
  
<?php 
if(!isset($payment_method->THIS_CC_CODES)) {
?> 
	
<p> This payment method does not use credit card short codes</p>
  
<?php 
} else {
?>
<p> This payment method relies on correct credit card short codes in the <a href="index.php?pshop_mode=admin&page=store.creditcard_list&option=com_virtuemart">VirtueMart 
  Credit Cards List</a>. The correct credit cards are automatically added during 
  installation, 
  and are specified in the <a 
  href="index.php?option=<?php echo $payment_method->THIS_OPTION; ?>&task=sqlfile&sqlfile=creditcards">Installation 
  SQL file</a></p>
  

<p>Credit cards with a green tick are recognised by this payment method. Credit 
  cards with a red cross are <b>not</b> recognised. The payment method should 
  be associated only with recognised credit card types.</p>
	
	<?php 
	
	// check cards exist
	$cstr = $payment_method->THIS_CC_CODES;
	$cstr = str_replace(' ', '', $cstr);
	$cary = explode(',', $cstr);

	$q = "SELECT * FROM #__vm_creditcard"; // WHERE creditcard_code IN (" . $cstr . ")"; 
	$database->setQuery( $q );
	$rows = $database->loadObjectList();
	
	foreach ($rows as $row)  {
		echo showYesNoImg(in_array($row->creditcard_code, $cary));
		echo '&nbsp;'; 
		echo $row->creditcard_code . ' (' . $row->creditcard_name . ')<br>';
	}
	
} // isset($payment_method->THIS_CC_CODES)

?>
