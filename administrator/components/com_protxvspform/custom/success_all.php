<?php 
// *************************************************************************
// * success_all.php                                                       *
// * Code Word: Srednekolymsk                                              *
// * Sample success script                                                 *
// * send notification email to merchants                                  *
// * silent                                                                *
// *************************************************************************

$mail_Subject = "Order Success: " . $order_id; 
$mail_Body = "Hi,

The Status of Order No. $order_id has been changed. 
New Status is: Confirmed

$vendor_name
$vendor_mail
" . JURI::root(false);


mail_Send( $vendor_mail, $vendor_name, $vendor_mail, $mail_Subject, $mail_Body, true ); 
 
?>