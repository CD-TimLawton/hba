DELETE FROM #__vm_product
WHERE (product_sku = 'Test01');

DELETE FROM #__vm_shipping_carrier 
WHERE (shipping_carrier_id = '99')
AND (shipping_carrier_name = 'Test');

DELETE FROM #__vm_shipping_rate 
WHERE (shipping_rate_carrier_id = '99')
AND (shipping_rate_name = 'Test');
