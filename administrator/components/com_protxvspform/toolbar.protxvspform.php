<?php
// *************************************************************************
// * toolbar.php                                                      *
// * Code Word: Srednekolymsk                                              *
// *************************************************************************

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

require_once( JApplicationHelper::getPath( 'toolbar_html' ) );

switch ( $task )
{ 
	case '': 
	case 'intro':
	default:
		TOOLBAR_protxvspform::_DEFAULT();
		break;
}

?>