<?php
// *************************************************************************
// *                                                                       *
// * Generic VirtueMart Payment Method Admin Script                        *
// * Release Date: 2010.07.03                                              *
// * Build Time: 3 July 2010 20:00                                         *
// * Version: 1.0.4                                                        *
// * Code Word: Srednekolymsk                                              *
// * For: Joomla 1.5/VirtueMart 1.1.x                                      *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * Author: E-commerce Solution                                           *
// * Email: support@virtuemart-solutions.com                               *
// * Website: http://www.virtuemart-solutions.com                          *
// * Copyright: (C) 2009 E-commerce Solution                               *
// * Licence: GPL                                                          *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * This program is free software: you can redistribute it and/or modify  *
// * it under the terms of the GNU General Public License as published by  *
// * the Free Software Foundation, either version 3 of the License, or     *
// * (at your option) any later version.                                   *
// *                                                                       *
// * This program is distributed in the hope that it will be useful,       *
// * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
// * GNU General Public License for more details.                          *
// *                                                                       *
// * You should have received a copy of the GNU General Public License     *
// * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
// *                                                                       *
// *                                                                       *
// *                                                                       *
// *************************************************************************


  defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed. '  );

  error_reporting(E_ALL);

  // component settings  
  
  //virtuemart.cfg.php needs $mosConfig_absolute_path,$mosConfig_live_site, $mosConfig_secret
  require_once(JPATH_SITE.'/administrator/components/com_virtuemart/compat.joomla1.5.php'); 
  require_once(JPATH_SITE.'/administrator/components/com_virtuemart/virtuemart.cfg.php'); 
  
  // CLASSPATH might include JPATH_SITE
  require_once(CLASSPATH.'payment/ps_protxvspform.php');  
  require_once(CLASSPATH.'payment/ps_protxvspform.cfg.php');  
  
  global $payment_method;
  $payment_method = new ps_protxvspform(); 

  // Joomla 1.5/VirtueMart 1.1 compatibility
  global $mosConfig_absolute_path;
  $mosConfig_absolute_path = JPATH_SITE; 
  

  // define rss 
  global $url_rssnews, $url_rssupd, $url_rssupd_more;
  $url_rssnews = 'http://supernova.tele-pro.co.uk/index.php?option=com_content&view=cat2&format=feed&type=rss&ids=26';     
  // $url_rssupd = 'http://supernova.tele-pro.co.uk/index.php?option=com_content&view=category&format=feed&type=rss&id=' . $payment_method->THIS_RSS_ID;    
  $url_rssupd = 'http://supernova.tele-pro.co.uk/index.php?option=com_content&view=cat2&format=feed&type=rss&ids=59%2C' . $payment_method->THIS_RSS_ID;    
  $url_rssupd_more = 'http://supernova.tele-pro.co.uk/index.php?option=com_content&view=cat2&layout=blog&ids=59%2C' . $payment_method->THIS_RSS_ID;    


  // include generic_fns
  require_once($mosConfig_absolute_path . '/administrator/components/' . $payment_method->THIS_OPTION . '/generic_fns.php');
    
  // include lastRSS library
  require_once($mosConfig_absolute_path . '/administrator/components/' . $payment_method->THIS_OPTION . '/lastRSS.php');

  // get_payment_method_id_from_code	
  global $payment_method_id; 
  $payment_method_id = get_payment_method_id_from_code($payment_method->payment_code);

  // its a payment mthod
  define('COMP_IS_PAYMENT', 1);

  switch ( $task ) {

	case 'edit':
		$mainframe->redirect(JURI::root( true ) . '/administrator/index2.php?page=store.payment_method_form&limitstart=0&keyword=&payment_method_id=' . $payment_method_id . '&option=com_virtuemart', '' );
		break; 		

	case 'extrainfo':
		update_payment_extrainfo($payment_method->payment_code, JPATH_SITE . '/administrator/components/' . $payment_method->THIS_OPTION .  DS . 'payment_extrainfo.txt');	
		$msg = 'Extra Info Code Reset';
		$mainframe->redirect( 'index.php?task=diag&option='.$payment_method->THIS_OPTION , $msg);	
		break; 
		
    case 'export':
	    require_once($mosConfig_absolute_path . DS . 'administrator'. DS . 'components' . DS . $payment_method->THIS_OPTION . DS . 'admin.page.settings.php');
		exit;
		break;
		
	case 'settings':
	case 'import':
		ShowPage($payment_method_id, 'settings');
		break;
		
	case 'licence':	
	case 'test':	 
	case 'setup':	
	case 'sqlfile':	
	case 'cards':
	case 'support':
	case 'diag':
	case 'files':
	case 'updates':
	case 'intro':	
	case '':
		ShowPage($payment_method_id, $task);
		break;
		
	case 'welcome':	
		 $msg = 'Thanks for installing this component. Click Setup to get started.';
  		 $mainframe->enqueueMessage($msg, 'message'); 
	default:
		ShowPage($payment_method_id, '');
		break;		
  } 
 

function ShowPage($payment_method_id, $pg) {
	if($pg =='') $pg = 'intro';
	
	global $mainframe;
	global $mosConfig_absolute_path;  
	global $payment_method;
	global $img_new;  
	global $img_ok;  
	global $img_no;  
	
	global $url_rssnews;
	global $url_rssupd; 
	global $url_rssupd_more;
		
	$database = &JFactory::getDBO(); 
  
	$help_url = $payment_method->THIS_SUPPORT_WEBSITE . '/kb?c=' . $payment_method->THIS_HELP_ID;	 
	
  // show page
  ?> 
<table width="100%" border="0" cellpadding="0"  cellspacing="0" >
  <tr> 
    <td valign="top"> 
      <table class="adminform" width="100%"  >
        <tr> 
          <td valign="top" style="font-size:12px;"> 
            <?php
		  require_once($mosConfig_absolute_path . DS . 'administrator' . DS . 'components' . DS  . $payment_method->THIS_OPTION . DS . 'admin.page.' . $pg . '.php');
		  if($pg != 'settings') showHiddenForm();
		  ?>
          </td>
        </tr>
      </table>
    </td>
    <td width="10" valign="top">&nbsp;</td>
    <td width="310" valign="top"> 
      <table class="adminform" width="100%" >
        <tr> 
          <td valign="top" width="5">&nbsp;</td>
          <td valign="top" style="font-size:12px;"> 
            <h3></h3>
            <p> <a href="<?php echo $payment_method->THIS_VENDOR_WEBSITE; ?>" target="_blank"> 
              <img 
				src="<?php echo  JURI::root( false ); ?>components/<?php echo $payment_method->THIS_OPTION; ?>/ecom-solution.gif"
				align="absmiddle" border="0" hspace="10" ></a> 
            <p> <b>
              <?php echo $payment_method->THIS_PRODUCT ?>
              </b> 
            <p> 
              <!-- info panel -->
              <?php echo ShowVersionInfoPanel($payment_method); ?>
            <p> 
              <?php echo ShowLicenceInfoPanel($payment_method); ?>
			  
			
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<?php
}

function showHiddenForm() {
	global $payment_method;
?>
<form action="index.php" method="post" name="adminForm">
  <input type="hidden" name="task" value="">
  <input type="hidden" name="option" value="<?php echo $payment_method->THIS_OPTION; ?>">
</form>
<?php 
}

?>
