<?php

/**
 *
 * @package	VirtueMart
 * @subpackage Plugins  - Elements
 * @author Valarie Isaksen
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2011 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: $
 */

/*
 * This class is used by VirtueMart Payment or Shipment Plugins
 * which uses JParameter
 * So It should be an extension of JElement
 * Those plugins cannot be configured througth the Plugin Manager anyway.
 */
class JElementWorldpaysscUrl extends JElement
{

    /**
     * Element name
     * @access	protected
     * @var		string
     */
   var $_name = 'worldpaysscurl';

    function fetchElement($name, $value, &$node, $control_name)
    {
	 $class = $node->attributes( 'class' ) ? $node->attributes( 'class' ) : "text_area";
 
    $return = '<input type="text"' .
                     'name="' . $control_name . '[' . $name . ']"' .
                     'id="'   . $control_name . '[' . $name . ']"' .
                     'value="' . $value . '"' .
                     'class="' . $class . '" />';
                     
$url = JROUTE::_(JURI::root() . 'index.php?option=com_virtuemart&view=pluginresponse&task=pluginnotification&tmpl=component') . '<br/><br/>';

$return = '<br/>Go to Worldpay and login to your Worldpay Integration Setup Page<br/><br/>
Then set these options up as follows<br/><br/><b>Payment Response URL</b><br/>' .



$url .

'<b>Payment Response enabled?</b><br/>
This should be ticked.<br/><br/>
<b>Enable the Shopper response</b><br/>
This should be ticked.<br/><br/>';               
                

return $return;    }

}