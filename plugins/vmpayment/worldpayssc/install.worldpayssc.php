<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
//jimport('joomla.installer.installer');

/**
 *  Online Store Copyright 2013
 *  This code is copyright of online-store.co.uk
 *  You are not allowed to distribute or sell this code.
 *  http://plugins.online-store.co.uk
*/


class plgVmpaymentWorldpaysscInstallerScript
{
        /**
         * Method to install the extension
         * $parent is the class calling this method
         *
         * @return void
         */
        function install($parent) 
        {
             //   echo '<p>The module has been installed</p>';
        }
 
        /**
         * Method to uninstall the extension
         * $parent is the class calling this method
         *
         * @return void
         */
        function uninstall($parent) 
        {
           //     echo '<p>The module has been uninstalled</p>';
        }
 
        /**
         * Method to update the extension
         * $parent is the class calling this method
         *
         * @return void
         */
        function update($parent) 
        {
           //     echo '<p>The module has been updated to version' . $parent->get('manifest')->version) . '</p>';
        }
 
        /**
         * Method to run before an install/update/uninstall method
         * $parent is the class calling this method
         * $type is the type of change (install, update or discover_install)
         *
         * @return void
         */
        function preflight($type, $parent) 
        {
           //     echo '<p>Anything here happens before the installation/update/uninstallation of the module</p>';
        }
 
        /**
         * Method to run after an install/update/uninstall method
         * $parent is the class calling this method
         * $type is the type of change (install, update or discover_install)
         *
         * @return void
         */
function postflightorg($type, $parent) 
	{              
                

$db = JFactory::getDbo();
 
$extension_id = null;

if (version_compare (JVERSION, '1.6.0', 'ge'))
	{

	$db = JFactory::getDBO();
	$query =
	"SELECT " . $db->nameQuote('extension_id') .
	" FROM " . $db->nameQuote('#__extensions') .
	" WHERE " . $db->nameQuote('element') ." = " . $db->quote('worldpayssc').";";
	$db->setQuery($query);
	$result = $db->loadResult();
	$extension_id=$result;

	if(!empty($result))
		{
		// enable plugin
		$db = JFactory::getDBO();

		$query = $db->getQuery(true);
		$fields = array($db->nameQuote('enabled') . '=1');
		$conditions = array($db->nameQuote('extension_id') . '=' . $result);

		$query->update($db->nameQuote('#__extensions'))->set($fields)->where($conditions);
		$db->setQuery($query);
		$result = $db->query();

		}

	}
	
              
                
        }
        
        
function postflight($type, $parent) 
	{

$debug='0';

$db = JFactory::getDbo();
 
$extension_id = null;

if (version_compare (JVERSION, '1.6.0', 'ge'))
	{

	$db = JFactory::getDBO();
	$query =
	"SELECT " . $db->nameQuote('extension_id') .
	" FROM " . $db->nameQuote('#__extensions') .
	" WHERE " . $db->nameQuote('element') ." = " . $db->quote('worldpayssc').";";
	$db->setQuery($query);
	$result = $db->loadResult();
	$extension_id=$result;

	if(!empty($result))
		{
		// enable plugin
		$db = JFactory::getDBO();

		$query = $db->getQuery(true);
		$fields = array($db->quoteName('enabled') . '=1');
		$conditions = array($db->quoteName('extension_id') . '=' . $result);

		$query->update($db->quoteName('#__extensions'))->set($fields)->where($conditions);
		$db->setQuery($query);
		$result = $db->query();

		}

	
   // is plugin already listed in paymentmethods?

	$db = JFactory::getDBO();
	$q = 'SELECT `payment_jplugin_id` FROM `#__virtuemart_paymentmethods` WHERE `payment_element` = "worldpayssc"';
	$db->setQuery($q);
	$payment_jplugin_id = $db->loadResult();


	if($debug)
		{
		echo '<br>' . 'payment_jplugin_id ' . $payment_jplugin_id . '<br>';
		}
			
	$existing_virtuemart_paymentmethod_id=null;

	if(empty($payment_jplugin_id) && !empty($extension_id))
		{
		print 'payment_jplugin_id is empty' . $payment_jplugin_id . '<br>';
		print 'extension_id is not empty' . $extension_id . '<br>';
		
		$q='INSERT INTO `#__virtuemart_paymentmethods` (`virtuemart_paymentmethod_id`, `virtuemart_vendor_id`, `payment_jplugin_id`,  `payment_element`, `payment_params`, `shared`, `ordering`, `published`, `created_on`, `created_by`, `modified_on`, `modified_by`, `locked_on`, `locked_by`) VALUES
		(DEFAULT, 1, ' . $extension_id . ',  "worldpayssc", \'payment_logos=""|countries=""|payment_currency="0"|status_pending="U"|min_amount=""|max_amount=""|cost_per_transaction=""|cost_percent_total=""|tax_id="0"|payment_info=""|\', 0, 0, 0,  "0000-00-00 00:00:00", 0,  "0000-00-00 00:00:00", 0,  "0000-00-00 00:00:00", 0)';
		$db->setQuery($q);
		$db->query();
		}
	else
		{
		if($debug) print 'payment_jplugin_id already exists is:' . $payment_jplugin_id . '<br>';
		}

	$q = 'SELECT `virtuemart_paymentmethod_id` FROM `#__virtuemart_paymentmethods` WHERE `payment_element` = "worldpayssc"';
	$db->setQuery($q);
	$existing_virtuemart_paymentmethod_id = $db->loadResult();
		
	
//////////////////////////////////////////////////////////////////////////////////////	
	
	if (!class_exists('VmConfig'))
	  	require(JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_virtuemart' . DS . 'helpers' . DS . 'config.php');
	  	
	if (class_exists('VmConfig'))VmConfig::loadConfig();

	if(!defined('VMLANG'))
		{
		$params = JComponentHelper::getParams('com_languages');
		$lang = $params->get('site', 'en-GB');
		$lang = strtolower(strtr($lang,'-','_'));
		}
	else
		{
		$lang = VMLANG;
		}


	if($debug)
		{
		echo 'VMLANG: ' . VMLANG;
		echo '<br>' . 'extension id: ' . $extension_id . '<br><br>';
		}


	if(!empty($existing_virtuemart_paymentmethod_id))
	 	{
	 	
	 	$this->update_payment_methods($lang,$existing_virtuemart_paymentmethod_id,$debug);

		if($debug)
			{
			echo '<br>begin script<br>';
			}

		$url= "<a href='index.php?option=com_virtuemart&view=paymentmethod&task=edit&cid[]=" . $existing_virtuemart_paymentmethod_id . "' style='text-decoration: underline; font-size: large'>Click here to go to setup</a>";
		//echo "<noscript>";
		echo $url;
		//echo "</noscript>";
		echo "<br/><br/>";

		$url2 = "index.php?option=com_virtuemart&view=paymentmethod&task=edit&cid[]=" . $existing_virtuemart_paymentmethod_id;

		if(!$debug)
			{
			echo "<script>";
			echo 'window.location.href="';
			echo $url2;
			echo '"';
			echo "</script>";
			}
		
		if($debug)echo '<br>end script<br>';
	

		////// Begin secondary loop ////////////
		
		if($debug)echo '<br>Start secondary loop<br>';
	 	
		$language =JFactory::getLanguage();
		$jLangs = $language->getKnownLanguages(JPATH_BASE);
		
		
		foreach ($jLangs as $jLang)
			{
			$lang = strtolower(strtr($jLang['tag'],'-','_'));
			if($debug) echo '<br>' . 'lang: ' . $lang . '<br>' . $jLang['name'] . '<br>';

			$this->update_payment_methods($lang,$existing_virtuemart_paymentmethod_id,$debug);

			}
		////// End Secondary Loop /////////
		
		}		
		
		
	}
                         
                
   }
   
private function update_payment_methods($lang,$existing_virtuemart_paymentmethod_id,$debug)
	{

// is plugin already listed in paymentmethods_lang?	
//	$table = 'virtuemart_paymentmethods_' . $lang;
//   $this->table_exist($table,$debug);		 
		 
		 
  	$q = 'SELECT `virtuemart_paymentmethod_id` FROM `#__virtuemart_paymentmethods_' . $lang . '` WHERE `virtuemart_paymentmethod_id` = "' . $existing_virtuemart_paymentmethod_id . '"';
	$db = JFactory::getDBO();		
	$db->setQuery($q);
	$check_paymentmethod_id_lang = $db->loadResult();
		
	if($debug)
		{
		echo 'check_paymentmethod_id_lang: ' . $check_paymentmethod_id_lang . '<br>';
		echo 'lang: ' . $lang . '<br>';
		}
		
	if(empty($check_paymentmethod_id_lang))
		{
		if($debug)
			{
			echo 'Empty check_paymentmethod_id_lang:<br>';
			}
				
		$q="INSERT INTO `#__virtuemart_paymentmethods_" . $lang . "` (`virtuemart_paymentmethod_id`, `payment_name`, `payment_desc`, `slug`) VALUES (" . $existing_virtuemart_paymentmethod_id . ", 'Worldpay - Credit/Debit Card Payments', '', 'worldpay-html-direct')";
		$db->setQuery($q);
		$db->query();
		}
	else
		{
		if($debug)
			{
			echo 'Virtuemart Payment Method already defined in virtuemart_payment_methods_ lang table<br><br>';
			}
		}

	}  
	
	
 private function table_exist($table,$debug)
	{

	if (!class_exists('JConfig'))
		require(JPATH_CONFIGURATION . '/configuration.php');
	
	$cfg = new JConfig();
	$dbname = $cfg->db;
	$dbprefix = $cfg->dbprefix;		
		
	$table = $dbprefix . $table;

	if($debug)echo $table . '<br>Table name is ' . $table . '<br><br>';
		
	$tables = JFactory::getDbo()->getTableList();
	
	//$table = 'test';
	
	if(in_array($table,$tables))
		{
		if($debug)echo '<br>Table found<br><br>';
		return true;
		}
	else
		{
		if($debug)echo '<br>Table not found<br><br>';
		return false;
		}
	}       
}







