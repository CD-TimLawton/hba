<?php 

defined('_JEXEC') or die('Direct Access to ' . basename(__FILE__) . ' is not allowed.');

/**
 *
 *  Virtuemart 2 Worldpay HTML Redirect 1.0.10
 *  Online Store Copyright 2013
 *  This code is copyright of online-store.co.uk
 *  You are not allowed to distribute or sell this code.
 *  http://plugins.online-store.co.uk/worldpay_payment_plugin.html
*/
 
 
if (!class_exists('vmPSPlugin'))
    require(JPATH_VM_PLUGINS . DS . 'vmpsplugin.php');

class plgVmPaymentWorldpayssc extends vmPSPlugin {

    // instance of class
    public static $_this = false;

    function __construct(& $subject, $config) {
	//if (self::$_this)
	//   return self::$_this;
	parent::__construct($subject, $config);
	
	$this->_loggable = true;
	$this->tableFields = array_keys($this->getTableSQLFields());
	$this->_tablepkey = 'id'; //virtuemart_worldpay_id';
	$this->_tableId = 'id'; //'virtuemart_worldpay_id';


	
	$varsToPush = array(

	"worldpay_installation_id" => array('', 'char'),
	
	"worldpay_account_type" => array('', 'char'),
	
	"worldpay_authMode" => array('', 'char'),

	"worldpay_enable_password" => array('', 'int'),
	"worldpay_password" => array('', 'char'),
	
	"worldpay_enable_md5" => array('', 'int'),
	
	"worldpay_md5_word" => array('', 'char'),
	
	"worldpay_check_domain" => array(1, 'int'),
	 
	"payment_currency" => array('', 'int'),
	"sandbox" => array(0, 'int'),
	  
	"payment_logos" => array('', 'char'),
	"debug" => array(0, 'int'),
	"status_pending" => array('', 'char'),
	"status_success" => array('', 'char'),
	"status_canceled" => array('', 'char'),
	"countries" => array('', 'char'),
	"min_amount" => array('', 'int'),
	"max_amount" => array('', 'int'),

	"cost_per_transaction" => array('', 'int'),
	"cost_percent_total" => array('', 'int'),
	"tax_id" => array(0, 'int')
	);

	$this->setConfigParameterable($this->_configTableFieldName, $varsToPush);

	//self::$_this = $this;
    }

    public function getVmPluginCreateTableSQL() {

	return $this->createTableSQL('Payment Paypal Table');
    }

    function getTableSQLFields() {

	$SQLfields = array(
	    "id" => ' int(1) unsigned NOT NULL AUTO_INCREMENT ',
	    "virtuemart_order_id" => ' int(1) UNSIGNED DEFAULT NULL',
	    "order_number" => ' char(32) DEFAULT NULL',
	    "virtuemart_paymentmethod_id" => ' mediumint(1) UNSIGNED DEFAULT NULL',
	    "payment_name" => ' char(255) NOT NULL DEFAULT \'\' ',
	    "payment_order_total" => 'decimal(15,5) NOT NULL DEFAULT \'0.00000\' ',
	    "payment_currency" => 'char(3) ',
	    "cost_per_transaction" => ' decimal(10,2) DEFAULT NULL ',
	    "cost_percent_total" => ' decimal(10,2) DEFAULT NULL ',
	    "tax_id" => ' smallint(1) DEFAULT NULL',
	    "worldpay_custom" => ' varchar(255)  ',

        "worldpay_response_payment_status" => ' char(50) DEFAULT NULL',
	    "worldpay_response_raw" => ' varchar(512) DEFAULT NULL'
	);
	return $SQLfields;
    }

    function plgVmConfirmedOrder($cart, $order) {

    if (!($method = $this->getVmPluginMethod($order['details']['BT']->virtuemart_paymentmethod_id))) {
	    return null; // Another method was selected, do nothing
	}
	if (!$this->selectedThisElement($method->payment_element)) {
	    return false;
	}
	$session = JFactory::getSession();
	$return_context = $session->getId();
	$this->_debug = $method->debug;
	$this->logInfo('plgVmConfirmedOrder order number: ' . $order['details']['BT']->order_number, 'message');

	if (!class_exists('VirtueMartModelOrders'))
	    require( JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'orders.php' );
	if (!class_exists('VirtueMartModelCurrency'))
	    require(JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'currency.php');

	//$usr = & JFactory::getUser();
	$new_status = '';


	//$usr = & JFactory::getUser();
	$new_status = '';
	
	$address = $order['details']['BT'];
	$del_address;
	
	if(isset($order['details']['ST']))
	   {
	   $del_address = $order['details']['ST'];
	   }
	else
	   {
	   $del_address = $order['details']['BT'];
	   }
	

	
	
    if (!class_exists('TableVendors'))
	    require(JPATH_VM_ADMINISTRATOR . DS . 'table' . DS . 'vendors.php');
	$vendorModel = new VirtueMartModelVendor();
	$vendorModel->setId(1);
	$vendor = $vendorModel->getVendor();
	$vendorModel->addImages($vendor, 1);


	$this->getPaymentCurrency($method);  // code 0 is default ??
	$q = 'SELECT `currency_code_3` FROM `#__virtuemart_currencies` WHERE `virtuemart_currency_id`="' . $method->payment_currency . '" ';
	$db = &JFactory::getDBO();
	$db->setQuery($q);
	$currency_code_3 = $db->loadResult();


    $q = 'SELECT `currency_symbol` FROM `#__virtuemart_currencies` WHERE `virtuemart_currency_id`="' . $method->payment_currency . '" ';
	//$db = &JFactory::getDBO();
	$db->setQuery($q);
	$currency_symbol = $db->loadResult();


	$paymentCurrency = CurrencyDisplay::getInstance($method->payment_currency);
	$totalInPaymentCurrency = round($paymentCurrency->convertCurrencyTo($method->payment_currency, $order['details']['BT']->order_total,false), 2);
	$cd = CurrencyDisplay::getInstance($cart->pricesCurrency);



	// Prepare data that should be stored in the database
	$dbValues["order_number"] = $order["details"]["BT"]->order_number;
	$dbValues["payment_name"] = $this->renderPluginName($method, $order);
	$dbValues["virtuemart_paymentmethod_id"] = $cart->virtuemart_paymentmethod_id;
	$dbValues["custom"] = $return_context;
	$dbValues["cost_per_transaction"] = $method->cost_per_transaction;
	$dbValues["cost_percent_total"] = $method->cost_percent_total;
	$dbValues["payment_currency"] = $method->payment_currency;
	$dbValues["payment_order_total"] = $totalInPaymentCurrency;
//	$dbValues['tax_id'] = $method->tax_id;
	$this->storePSPluginInternalData($dbValues);

	
	
$amount 		= $totalInPaymentCurrency;

$amount = round($paymentCurrency->convertCurrencyTo($method->payment_currency, $amount, false),2);

$description 	= "";
$label 			= "Submit Payment"; // The is the label on the "submit" button


$loginID = "";


//$html .= print_r($cart);

$product_des = "";
$short_product_des = "";
$x_line_item_html="";
$br = "\n";	


if(strlen($product_des) >255)
   {
   $product_des = $short_product_des;
   }

$description = $product_des;


//// If an amount or description were posted to this page, the defaults are overidden
//if (array_key_exists("amount",$_REQUEST))
//	{ $amount = $_REQUEST["amount"]; }
//if (array_key_exists("amount",$_REQUEST))
//	{ $description = $_REQUEST["description"]; }
	
	$amount = number_format($amount,2,'.','');

// an invoice is generated using the date and time
$invoice	= date("YmdHis");
// a sequence number is randomly generated
$sequence	= rand(1, 1000);
// a timestamp is generated
$timeStamp	= time();


// Print the Amount and Description to the screen.

$html = "";

//$html .= "Amount: " . $amount  . "<br />";

//$html .= "Description: " . $description  . "<br />" . $br;



$instId = '';
$worldpay_url = '';

$testMode = '0';

if($method->worldpay_account_type === "Live")
   {
   $instId = $method->worldpay_installation_id;
   $worldpay_url = "https://secure.worldpay.com/wcc/purchase";
   $testMode = '0';
   }
else
   {
   $instId = $method->worldpay_installation_id;
   $worldpay_url = "https://secure-test.worldpay.com/wcc/purchase";
   
   $testMode = "100";
   }

if($method->debug)
	{
	echo '<span style="color: #FF0000">';
	echo "<br/>You have enabled debugging in setup. Turn it off to remove the debug information.<br/><br/>";
	echo '</span>';
	}

if($method->debug)
    {
    echo "Amount " . $totalInPaymentCurrency;
    echo "<br>currency code " . $currency_code_3;
    echo "<br>Currency ID " . $method->payment_currency;
    echo "<br>order_total " . $order["details"]["BT"]->order_total;
    echo "<br>USD " . round($paymentCurrency->convertCurrencyTo("144", $totalInPaymentCurrency, false),2);
    echo "<br>Currency Dispay:" .  $currency_symbol;
    echo "<br>";
    echo "<br>order number:" .  $order["details"]["BT"]->order_number;
    echo "<br>Account Type " . $totalInPaymentCurrency;
    echo "<br>WorldPay URL:" . $worldpay_url;
    echo "<br>Test Mode " . $testMode;
    }


//$demo
$d = 0;

if($d)
{
$html .= "<p style='font-size: large'>This is a demo version of the plugin.</p>
<p style='font-size: large'>Orders are restricted to Test Mode only.</p>
<p style='font-size: large'>Click the button below to contine.</p>";
$worldpay_url = "https://secure-test.worldpay.com/wcc/purchase";
$testMode = "100";

}


$html .= "<form  method='POST' name='vm_worldpay_form' action='";

  // $html .= 'https://select-test.wp3.rbsworldpay.com/wcc/purchase';
   
$html .= $worldpay_url;
      
   //$html .= 'https://select.worldpay.com/wcc/purchase';

$html .="'>";

   
$amount 		= $totalInPaymentCurrency;

//$amount = round($paymentCurrency->convertCurrencyTo("144", $amount, false),2);


//instId:amount:currency:cartId:MC_userId

//SignatureFields

$md5 = 1;

//<input type="hidden" name="signatureFields" value="amount:currency:cartId">

//<input type="hidden" name=signature value="58e41db32a6f2ff9c3c96eea6583ffbd">

if($method->worldpay_enable_md5 && $method->worldpay_md5_word != '')
	{
	$md5Secret = $method->worldpay_md5_word;
	
	$signatureFields = $md5Secret . ':' . $instId . ':'. $amount . ':' . $currency_code_3 . ':' . $order["details"]["BT"]->order_number;
	
//echo '<br>----' . $signatureFields . '----<br>';
//	if(phpversion() >= '5.1.2')
//		{
		//$fingerprint = hash_hmac("md5", $signatureFields);
		$fingerprint = md5( $signatureFields);
//		}
//	else 
//		{
//		$fingerprint = bin2hex(mhash(MHASH_MD5, $signatureFields));
//		}
		
   $html .= "<input type='hidden' name='signatureFields' value='instId:amount:currency:cartId'>" . $br;	
		
    $html .= "<input type='hidden' name='signature' value='" . $fingerprint . "'>" . $br;
	}

	$html .= "<input type='hidden' name='amount' value='" . $amount . "'>" . $br;
	$html .= "<input type='hidden' name='cartId' value='". $order["details"]["BT"]->order_number . "'>" . $br;
	$html .= "<input type='hidden' name='currency' value='" . $currency_code_3 . "'>" . $br;

	
	$html .= "<input type='hidden' name='instId' value='" . $instId . "'>" . $br;

$html .= "<input type='hidden' name='authMode' value='" . $method->worldpay_authMode . "'/>" . $br;

$html .= "<input type='hidden' name='testMode' value='" . $testMode ."'>" . $br;


$billing_name = $address->first_name . ' ' . $address->last_name;

$billing_address = $address->address_1 . "\n" .  $address->address_2 . "\n" . $address->city;


$html .= "<input type='hidden' name='name' value='" . $this->tfield($billing_name,40) . "'>" . $br;

$html .= "<input type='hidden' name='address' value='" .   $this->tfield($billing_address,255) . "'>" . $br;

$html .= "<input type='hidden' name='postcode' value='" . $this->tfield($address->zip,12) . "'>" . $br;

$html .= "<input type='hidden' name='tel' value='" . $this->tfield($address->phone_1,30) . "'>" . $br;

$html .= "<input type='hidden' name='fax' value='" . $this->tfield($address->fax,30) . "'>" . $br;

$html .= "<input type='hidden' name='email' value='" . $this->tfield($address->email,80) . "'>" . $br;


$country = ShopFunctions::getCountryByID($address->virtuemart_country_id, "country_2_code");

$html .= "<input type='hidden' name='country' value='" . $country . "'>" . $br;

//if($worldpay_billing_address eq 'Yes')
//   {
   $html .= "<input type='hidden' name='withDelivery' value='true'>";
   


$delivery_name = $del_address->first_name . ' ' . $del_address->last_name;

$delivery_address = $del_address->address_1 . "\n" .  $del_address->address_2 . "\n" . $del_address->city;


$html .= "<input type='hidden' name='delvName' value='" . $this->tfield($delivery_name,40) . "'>" . $br;

$html .= "<input type='hidden' name='delvAddress' value='" .   $this->tfield($delivery_address,255) . "'>" . $br;

$html .= "<input type='hidden' name='delvPostcode' value='" . $this->tfield($del_address->zip,12) . "'>" . $br;


$delivery_country = ShopFunctions::getCountryByID($del_address->virtuemart_country_id, "country_2_code");

$html .= "<input type='hidden' name='delvCountry' value='" . $delivery_country . "'>" . $br;


$pm = $order['details']['BT']->virtuemart_paymentmethod_id;

$html .= "<input type='hidden' name='M_pm' value='" . $pm . "'>" . $br;


/*   
   if($worldpay_billing_address eq 'Yes')
      {
      $html .= "<input type='hidden' name='withDelivery' value='true'>";
      }
     
   if($acc_Id ne "")
      {
      $html .= "<input type='hidden' name='accId1' value = " . $acc_Id . "'>";
   ////   $html .= "<input type='hidden' name='authMode' value='" . $auth . "'>";
      }
   
   $html .= "<input type='hidden' name='authMode' value='" . $auth . "'>";   
  */    
      

$product_des = "";
$short_product_des = "";
$x_line_item_html="";
$br = "\n";	

	  $i = 1;
	  foreach ($cart->products as $key => $product)
	     {
	     $item_name = strip_tags($product->product_name);
        $line_description = strip_tags($product->customfields);
        
        $item_name .= $line_description . ' ';
        
	     $id = strip_tags($product->product_sku);
	     $cost = $cart->pricesUnformatted[$key]["salesPrice"];
	     
	     $cost = round($paymentCurrency->convertCurrencyTo($method->payment_currency, $cost, false),2);
//	     $cost = number_format($cost,2,".","");
         $qty = $product->quantity;
	     $product_des .= $id . " " . $item_name . "(" . $qty . "x " . $currency_symbol . $cost . ")" . " ";
	     $short_product_des .= $id . " " . "(" . $qty . "x " . $currency_symbol . $cost . ")" . " ";
         
	     $i++;
	     }
	     
	     	     	     
	  if($cart->pricesUnformatted ["salesPriceShipment"] > 0)
	     {  
	     $shipping = $cart->pricesUnformatted ["salesPriceShipment"];
	     
	     $shipping = round($paymentCurrency->convertCurrencyTo($method->payment_currency, $shipping, false),2);
//	     $shipping =  number_format($shipping,2,".","");
	     $short_product_des .= "Shipping: " . $currency_symbol . $shipping;
	     $product_des .= "Shipping: " . $currency_symbol . $shipping;
          
	     }
	  	  


if(strlen($product_des) >255)
   {
   $product_des = $short_product_des;
   }


      
$pound = "";
      
//$short_product_des =~ s/&amp;#163;/$pound/g;
//$short_product_des =~ s/&amp;pound;/$pound/g;

//$product_des =~ s/&amp;#163;/$pound/g;
//$product_des =~ s/&amp;pound;/$pound/g;
        
   $html .="<input type='hidden' name='desc' value='" . $this->tfield($product_des,255) . "'>";
   
   $html .="<input type='hidden' name= 'MC_description' value='" . htmlspecialchars($product_des) . "'>";

   $html .="<input type='submit' value='" . JText::_("VMPAYMENT_WORLDPAY_SSC_PAY_BY_DEBIT_CREDIT_CARD") . "' name='submit2'>";

   $html .= "</form>";

if(!$method->debug)
   {
   $html .= '<script type="text/javascript">';
   
  // if($d == 1)
  //    {
  //    $html .= 'setTimeout(function(){alert(1);},10000);';
   //   }
      
   $html .= 'document.vm_worldpay_form.submit();';
   $html .= '</script>';
   }
	
	

print $html;

$html='';

print "<!-- Copyright Online Store 2013
";

$html="
-->";
	
	// 	2 = don't delete the cart, don't send email and don't redirect
	return $this->processConfirmedOrderPaymentResponse(2, $cart, $order, $html, $new_status);

    }
    
    
    
function tfield($field,$len)
  {
  $field = substr($field,0,$len);
  $field = htmlspecialchars($field);
  return $field;
  }

    
    
function plgVmgetPaymentCurrency($virtuemart_paymentmethod_id, &$paymentCurrencyId)
	{
    
	if (!($method = $this->getVmPluginMethod($virtuemart_paymentmethod_id))) {
	    return null; // Another method was selected, do nothing
	}
	if (!$this->selectedThisElement($method->payment_element)) {
	    return false;
	}
	$this->getPaymentCurrency($method);
	$paymentCurrencyId = $method->payment_currency;
    }
    
//Worldpay Response    

// print html background

function plgVmOnPaymentResponseReceived(&$html)
	{
    
//transId
//callbackPW
//transStatus
  //  echo 'plgVmOnPaymentNotification' . '<br>';
    
	if (!class_exists ('VirtueMartCart'))
		{
		require(JPATH_VM_SITE . DS . 'helpers' . DS . 'cart.php');
		}
		
	if (!class_exists ('shopFunctionsF'))
		{
		require(JPATH_VM_SITE . DS . 'helpers' . DS . 'shopfunctionsf.php');
		}
	
    
	if (!class_exists('VirtueMartModelOrders'))
	    require( JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'orders.php' );
	    
	    
	// the payment itself should send the parameter needed.
	$virtuemart_paymentmethod_id = JRequest::getInt ('M_pm',0,'post');
	
	if($virtuemart_paymentmethod_id == 0)
		{
		$virtuemart_paymentmethod_id = JRequest::getInt ('M_pm',0,'get');
		}


$ret = "<style>
br#hidessc + h3
{
width:100%;
opacity:0.0;
filter:alpha(opacity=0);
}
</style>
<br id='hidessc'/>
";

	
	
	if (!($method = $this->getVmPluginMethod ($virtuemart_paymentmethod_id)))
		{
		//print 'Another Payment method was selected, Virtuemart not updated<br/>';
		print $ret;
		return NULL; // Another method was selected, do nothing
		}

	if (!$this->selectedThisElement ($method->payment_element))
		{
		//print 'Virtuemart not updated<br/>';
		print $ret;
		return NULL;
		}
  

 //   echo 'Check for cartId' . '<br>';	    

$paypal_data_get = JRequest::get('get');

$paypal_data_post = JRequest::get('post');

$paypal_data = null;

$paypal_data = $paypal_data_get;

$form_data = '';

foreach($paypal_data_post as $index => $value)
	{
	$paypal_data[$index] = $value;
    }
	


if (!isset($paypal_data["cartId"]))
	{
	print $ret;
	return;
	}
	
	
$order_number = $paypal_data["cartId"];
$virtuemart_order_id = VirtueMartModelOrders::getOrderIdByOrderNumber($paypal_data["cartId"]);


$payment = $this->getDataByOrderId($virtuemart_order_id);
	
	
//   echo '<br>Check for Virtuemart Order ID' . '<br>';

if (!$virtuemart_order_id)
	{
    echo 'Worldpay invoice ' . $paypal_data["cartId"] . ' is not valid' . '<br>';
    print $ret;
    return;
	}


	
if($paypal_data["transStatus"] === 'Y')
	{
	print "<p style='font-size: x-large'>" . JText::_("VMPAYMENT_WORLDPAY_SSC_THANK_YOU_FOR_YOUR_ORDER") . "</p>";
	$this->print_order_details($virtuemart_order_id,$method,1);
	}


if($paypal_data["transStatus"] === 'C')
	{
	print "<p style='font-size: x-large'>" . JText::_("VMPAYMENT_WORLDPAY_SSC_ORDER_HAS_BEEN_CANCELLED") . "<br/>";
	print '<a href="' . JROUTE::_(JURI::root()) . 'index.php">' . JText::_("VMPAYMENT_WORLDPAY_SSC_CLICK_TO_CONTINUE") . '</a></p><br/>';
	print $ret;
    return false;

	}

if($method->debug)
	{
	echo '<span style="color: #FF0000">';
	echo "<br/>You have enabled debugging in setup. Turn it off to remove any debug information.<br/><br/>";
	echo '</span>';
	}

if (isset($paypal_data_get["viewOnly"]))
	{
	if($method->debug)
		{
		print 'View Only<br/>';
		}
		
	$cart = VirtueMartCart::getCart ();
	$cart->emptyCart ();
		
	print $ret;
	return;
	}

////echo "<br>Check valid method ?";

//$method = $this->getVmPluginMethod($payment->virtuemart_paymentmethod_id);    	
    	
//if (!$this->selectedThisElement($method->payment_element))
//	{
//	print $ret;
//  return false;
//	}
	

// $this->logInfo('plgVmOnPaymentNotification: virtuemart_order_id  found ' . $virtuemart_order_id, 'message');

if($method->worldpay_enable_password)
	{
	if($method->debug)
		{
		echo "<br>Check Worldpay Password :";
		//echo  $method->worldpay_password;
		}
      
	if( $paypal_data['callbackPW'] != $method->worldpay_password)
		{
		if($method->debug)
		echo "<br>Password not matched :";
		print $ret;
		return;
		}
	}
	

//POST /cgi-bin/test.cgi?msgType=authResult&installation=204793 HTTP/1.0
//Content-Length: 658
//Content-Type: application/x-www-form-urlencoded; charset=ISO-8859-1
//Host: test.worldpay.com:5095
//User-Agent: WJHRO/1.0 (WorldPay Java HTTP Request Object)
//'HTTP_HOST'

    //$http_host = $_SERVER['HTTP_HOST'];
    
   	//$http_host = trim($http_host);
   	
if($method->worldpay_check_domain)
	{
	$hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
	
	$hostname = trim($hostname);
	
	if (preg_match('/\.worldpay\.com$/i', $hostname) || preg_match('/\.rbsworldpay\.com$/i', $hostname))
//	if (preg_match('/\.as43234\.net$/i', $hostname))
		{
		if($method->debug)
			{
	    	echo "<br/>WorldPay hostname is correct.";
	    	echo '<br>hostname ' . $hostname . '<br>';
	    	echo $_SERVER['REMOTE_ADDR'];
	    	}
		}
	else
		{
		if($method->debug)
			{
	    	echo "<br/>Worldpay domain not matched. ";
			echo '<br>hostname ' . $hostname . '<br>';
	    	echo $_SERVER['REMOTE_ADDR']; 	    
	    	}
	    	
	    $mailsubject = "Pending Order: Cannot Verify Worldpay domain.";

		$mailbody = "The following order has been received but it can not be verifed if the order is from WorldPay.\n\n";
		
		$mailbody .= "Please log in to your WorldPay server to confirm the order.\n\n";

		foreach ($paypal_data as $key => $value) {$mailbody .= $key . " = " . $value . "\n\n";}
		$this->sendEmailToVendorAndAdmins ($mailsubject, $mailbody);
      
	    print $ret;
		return;
		}
	}
//	echo "<br>Check Order ID :" . $method->worldpay_enable_password . $paypal_data['callbackPW'];

	$this->_debug = $method->debug;
	if (!$payment)
		{
	    $this->logInfo('getDataByOrderId payment not found: exit ', 'ERROR');
	    return null;
		}
	$this->logInfo('paypal_data ' . implode('   ', $paypal_data), 'message');


	


	// get all know columns of the table
	$db = JFactory::getDBO();
	$query = 'SHOW COLUMNS FROM `' . $this->_tablename . '` ';
	$db->setQuery($query);
	$columns = $db->loadResultArray(0);
	$post_msg = '';
	foreach ($paypal_data as $key => $value) {
	    $post_msg .= $key . "=" . $value . "<br />";
	    $table_key = 'paypal_response_' . $key;
	    if (in_array($table_key, $columns)) {
		$response_fields[$table_key] = $value;
	    }
	}

	$this->logInfo('post variables',$post_msg);

	//$response_fields[$this->_tablepkey] = $this->_getTablepkeyValue($virtuemart_order_id);
	$response_fields['payment_name'] = $this->renderPluginName($method);
	$response_fields['worldpay_response_raw'] = $post_msg;
	$return_context = $paypal_data['custom'];
	$response_fields['order_number'] = $order_number;
	$response_fields['virtuemart_order_id'] = $virtuemart_order_id;
//$preload=true   preload the data here too preserve not updated data
 	$this->storePSPluginInternalData($response_fields, 'virtuemart_order_id', true);

	
 $paypal_status = $paypal_data["transStatus"];
 
  if($method->debug)
   echo "<br>Check " . $paypal_status; 
   
   
 $new_status=''; //Y or C
   
 if($paypal_status === "Y")
       {
	   $new_status = $method->status_success;
	   }
	elseif ($paypal_status === "C")
	   {
	   $new_status = $method->status_canceled;
	   }
	else
	   {
	   $new_status = $method->status_pending;
   	$new_status = 'P';
	   //$new_status = $method->status_canceled;
	   }

 //   $new_status = $method->status_success;


	//$this->logInfo("plgVmOnPaymentNotification return new_status:" . $new_status, $message);
	
	if ($virtuemart_order_id)
	{
	// send the email only if payment has been accepted
	// already defined
	//if (!class_exists('VirtueMartModelOrders'))
	//require( JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'orders.php' );
	$modelOrder = new VirtueMartModelOrders();
	$orderitems = $modelOrder->getOrder($virtuemart_order_id);
	$nb_history = count($orderitems['history']);
	$order['order_status'] = $new_status;
	$order['virtuemart_order_id'] = $virtuemart_order_id;
	$order['comments'] = JText::sprintf('VMPAYMENT_PAYPAL_PAYMENT_CONFIRMED', $order_number);
	   
	if ($nb_history == 1)
	   {
	   $order['comments'] .= "<br />" . JText::sprintf('VMPAYMENT_PAYPAL_EMAIL_SENT');
	   $this->logInfo('Notification, sentOrderConfirmedEmail ' . $order_number. ' '. $new_status, 'message');
	   $order['customer_notified'] = 1;
	   }
	else
	   {
	   $order['customer_notified'] = 0;
	   }
	   
	$modelOrder->updateStatusForOneOrder($virtuemart_order_id, $order, true);
	   
	//$this->emptyCart(null);
	
	$cart = VirtueMartCart::getCart ();
	$cart->emptyCart ();
		
	}

	print $ret;
	
	return null; 
    
	}

    function plgVmOnUserPaymentCancel() {

	if (!class_exists('VirtueMartModelOrders'))
	    require( JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'orders.php' );

	$order_number = JRequest::getVar('on');
	if (!$order_number)
	    return false;
	$db = JFactory::getDBO();
	$query = 'SELECT ' . $this->_tablename . '.`virtuemart_order_id` FROM ' . $this->_tablename . " WHERE  `order_number`= '" . $order_number . "'";

	$db->setQuery($query);
	$virtuemart_order_id = $db->loadResult();

	if (!$virtuemart_order_id) {
	    return null;
	}
	$this->handlePaymentUserCancel($virtuemart_order_id);

	//JRequest::setVar('paymentResponse', $returnValue);
	return true;
    }

    /*
     *   plgVmOnPaymentNotification() - This event is fired by Offline Payment. It can be used to validate the payment data as entered by the user.
     * Return:
     * Parameters:
     *  None
     *  @author Valerie Isaksen
     */

function plgVmOnPaymentNotification()
{

	if (!class_exists ('VirtueMartCart'))
		{
		require(JPATH_VM_SITE . DS . 'helpers' . DS . 'cart.php');
		}
		
	if (!class_exists ('shopFunctionsF'))
		{
		require(JPATH_VM_SITE . DS . 'helpers' . DS . 'shopfunctionsf.php');
		}
	
    
	if (!class_exists('VirtueMartModelOrders'))
	    require( JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'orders.php' );
	    
	    
	// the payment itself should send the parameter needed.
	$virtuemart_paymentmethod_id = JRequest::getInt ('M_pm',0,'post');
	
	if($virtuemart_paymentmethod_id == 0)
		{
		$virtuemart_paymentmethod_id = JRequest::getInt ('M_pm',0,'get');
		}



	if (!($method = $this->getVmPluginMethod ($virtuemart_paymentmethod_id)))
		{
		//print 'Another Payment method was selected, Virtuemart not updated<br/>';
		return NULL; // Another method was selected, do nothing
		}

	if (!$this->selectedThisElement ($method->payment_element))
		{
		//print 'Virtuemart not updated<br/>';
		return NULL;
		}
	     
	
$order_data_get = JRequest::get('get');

$order_data_post = JRequest::get('post');

$order_data = null;

$order_data = $order_data_get;

$form_data = '';

foreach($order_data_post as $index => $value)
	{
	$order_data[$index] = $value;
	
	$value = htmlspecialchars($value, ENT_QUOTES);
	
	$form_data .= "<input type='hidden' name='$index' value='$value' />";
    }
	
	
if (!isset($order_data["cartId"]))
	{
	return;
	}
	
	

$this->plgVmOnPaymentResponseReceived();


$link = 'index.php?option=com_virtuemart&view=pluginresponse&task=pluginresponsereceived';
//transStatus
//M_pm
//cartId	   
	   
	   
$M_pm = $order_data["M_pm"];
$cartId = $order_data["cartId"];
$transStatus = $order_data["transStatus"];

$link .=
'&viewOnly=1&M_pm=' . urlencode($M_pm) . 
'&cartId=' . urlencode($cartId) .
'&transStatus=' . urlencode($transStatus);


if(!$method->debug)
   {
   echo '<script type="text/javascript">';
   echo 'parent.location="' . $link . '"';
   echo '</script>';
   }

echo '<p style="font-family: Arial, Helvetica, sans-serif; font-size: medium">';
echo '<a href="' . $link . '">Click to Continue</a></p>';

return null;

    }
    
    
    
function print_order_details($virtuemart_order_id,$method,$show_shipping)
{
        
echo '<br/>';

$order_form_line_item='';
$br = "\n";	

$line_order_details = '';
$line_order_total = 0;

$order_total=0;
$total_discount=0;
$total_tax=0;
$total_shipping=0;

$table='';

// modelorders

$modelOrders = new VirtueMartModelOrders();
$order = $modelOrders->getOrder($virtuemart_order_id);	

list($table,$order_total,$line_order_total,$total_discount,$total_tax,$total_shipping,$line_order_details) = $this->order_form($order,$method);

echo $table;

$address = $order['details']['BT'];

$del_address;
	
	if(isset($order['details']['ST']))
	   {
	   $del_address = $order['details']['ST'];
	   }
	else
	   {
	   $del_address = $order['details']['BT'];
	   }


$country = ShopFunctions::getCountryByID($address->virtuemart_country_id, "country_2_code");
$cc_state = isset($address->virtuemart_state_id) ? ShopFunctions::getStateByID($address->virtuemart_state_id) : "";

$name = $this->tfield($address->first_name,50) . ' ' . $this->tfield($address->last_name,50);

$del_country = ShopFunctions::getCountryByID($del_address->virtuemart_country_id, "country_2_code");
$del_state = isset($address->virtuemart_state_id) ? ShopFunctions::getStateByID($del_address->virtuemart_state_id) : "";
//$del_state_name = $del_state;


if($country === 'US' || $country === 'CA')
  {
  $cc_state = isset($address->virtuemart_state_id) ? ShopFunctions::getStateByID($address->virtuemart_state_id,"state_2_code") : "";
  }




if($del_country === 'US' || $del_country === 'CA')
  {
  $del_state = isset($del_address->virtuemart_state_id) ? ShopFunctions::getStateByID($del_address->virtuemart_state_id,"state_2_code") : "";
  }



print '<table style="width: 600px;border-style:hidden;border:0px">';
print '<tr><td style="border-style:hidden;border:0px" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;';

#if($shipping_address == 1)
#	{
	print '<br/><b>&nbsp;&nbsp;&nbsp;&nbsp;' . JText::_("VMPAYMENT_WORLDPAY_SSC_BILLING_ADDRESS")  . '</b><br/><br/>';
	print $this->billing_address_html($address,$country,$cc_state);
#	}
#else
#	{
#	print '<a href="' . JROUTE::_(JURI::root()) . 'index.php?option=com_virtuemart&view=user&task=editaddresscart&addrtype=BT">' . JText::_('VMPAYMENT_WORLDPAY_SSC_EDIT_ADDRESS') . '</a></strong>';
#	print $this->billing_address_html($address,$country,$state);
#	}

print '</td><td style="border-style:hidden;border:0px" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;';

if($show_shipping == 1)
	{
	print '<br/><b>&nbsp;' . JText::_("VMPAYMENT_WORLDPAY_SSC_SHIPPING_ADDRESS") . '</b><br/><br/>';
	print $this->shipping_address_html($del_address,$del_country,$del_state);

	}

print '</td></tr></table>';

#list($order_form,$order_form_html_billing,$order_form_html_shipping) = $this->billing_shipping_address($address,$del_address,$country,$del_country,$cc_state,$del_state);

echo '<br/>';


//This variable is not defined ???????????????????????????????
//echo $order_form_html_shipping;

    
    
    
}

function order_form($order,$method)
{


	if (!class_exists('CurrencyDisplay'))
	    require(JPATH_VM_ADMINISTRATOR . DS . 'helpers' . DS . 'currencydisplay.php');
	$paymentCurrency = CurrencyDisplay::getInstance();



$order_form_line_item='';
$br = "\n";	

$line_order_details = '';
$line_order_total = 0;

$inc_tax = 0;

	  if(isset($order['details']['BT']->order_tax) && $order['details']['BT']->order_tax > 0)
	     {
	     $inc_tax = 1;
	     }



//JText::_('VMPAYMENT_WORLDPAY_SSC_ID')
//JText::_('VMPAYMENT_WORLDPAY_SSC_ITEM')
//JText::_('VMPAYMENT_WORLDPAY_SSC_UNIT_PRICE')
//JText::_('VMPAYMENT_WORLDPAY_SSC_QUANTITY')
//JText::_('VMPAYMENT_WORLDPAY_SSC_TOTALS')
// $line_fields = str_replace($text, $replace, $line_fields);


$unit_price = JText::_("VMPAYMENT_WORLDPAY_SSC_UNIT_PRICE");

$unit_price = str_replace(' ','&nbsp;',$unit_price);

$table='<table border="1" width="600" cellspacing="0" cellpadding="2" class="ssc">
<tr>
	<td style="background-color: rgb(220,220,220)" width="50"><strong><font face="Arial"
	size="2">' . JText::_('VMPAYMENT_WORLDPAY_SSC_ID') . '&nbsp;&nbsp;</font></strong></td>
	<td style="background-color: rgb(220,220,220)" width="400"><strong><font face="Arial"
	size="2">' . JText::_('VMPAYMENT_WORLDPAY_SSC_ITEM') .  '</font></strong></td>
	<td style="background-color: rgb(220,220,220); text-align: right;" width="50"><font face="Arial"
	size="2"><strong>' . $unit_price .'&nbsp;</strong></font></td>
	<td style="background-color: rgb(220,220,220); text-align: right;" width="50"><strong><font face="Arial"
	size="2">&nbsp;' . JText::_('VMPAYMENT_WORLDPAY_SSC_QUANTITY') . '&nbsp;</font></strong></td>
	<td style="background-color: rgb(220,220,220); text-align: right;" width="50"><strong><font face="Arial"
	size="2">&nbsp;' . JText::_('VMPAYMENT_WORLDPAY_SSC_TOTALS') . '</font></strong></td>
</tr>';

 //   <td style="background-color: rgb(220,220,220); text-align: right;" width="50"><strong><font face="Arial"
 //   size="2">&nbsp;Line&nbsp;Total</font></strong></td>
 // </tr>';


	  $i = 1;

	  foreach ($order["items"] as $key => $product)
	     {
	     $line_item_name = substr(strip_tags($product->order_item_name), 0, 127);
	     $line_id = substr(strip_tags($product->order_item_sku), 0, 127);
	     
	     //$cost = number_format($product->product_item_price,2,'.','');
	     
	     
	     $cost = $product->product_item_price;
	     
	     $cost = $paymentCurrency->convertCurrencyTo($method->payment_currency, $cost, false);
	     
	     $cost = number_format($cost,2,'.','');
	     
	

	     $line = $product->product_subtotal_with_tax;
	     
	     $line = $paymentCurrency->convertCurrencyTo($method->payment_currency, $line, false);
	     
	     $line = number_format($line,2,'.','');
	     

     
	     $qty = $product->product_quantity;
	     
	     $line_fields = $product->product_attribute;

//----{"16":" <span class=\"costumTitle\">Size<\/span><span class=\"costumValue\" >150<\/span>","13":" <span class=\"costumTitle\">Color<\/span><span class=\"costumValue\" >yellow<\/span>","9":" <span class=\"costumTitle\">add a showel<\/span><span class=\"costumValue\" >13<\/span>"}------------
/*
	     $text = '<\\/span><span class=\\"costumValue\\';
	     
	    $replace = ':<\\/span><span class=\\"costumValue\\';
	     
	    $line_fields = str_replace($text, $replace, $line_fields);
	     
	    $line_fields = strip_tags($line_fields);

	   $line_fields = preg_replace('/\{"[0-9]*":" /','',$line_fields);

		$line_fields = preg_replace('/"}$/','',$line_fields);
		
    	$line_fields = preg_replace('/","[0-9]*":" /',' - ',$line_fields);

         $line_description = substr($line_id . $line_fields, 0, 127);
*/         
         
         
//// new line fields ////////         
         
if (!empty($product->product_attribute))
	{
	if(!class_exists('VirtueMartModelCustomfields'))require(JPATH_VM_ADMINISTRATOR.DS.'models'.DS.'customfields.php');
	$line_fields = VirtueMartModelCustomfields::CustomsFieldOrderDisplay($product,'FE');
	}
	
$strip_line_fields = strip_tags($line_fields);

//$strip_line_fields = str_replace('  ',' ', $strip_line_fields);

//echo '****************' . $strip_line_fields . '***********************';

//	$line_fields = json_decode($product->product_attribute, TRUE);
//<div class="product_attributes"><div class="vm-customfield-mod"> <span class="product-field-type-V">

if($line_fields === '')
	{
    $line_description = substr($line_id, 0, 127);
	}
else
	{
	$line_description = substr($line_id . ' ' . $strip_line_fields, 0, 127);
	}
         
//// end of new line fields ////               
	     
	    $n = $i-1;
	    
	$line_order_details .= 
    "&L_PAYMENTREQUEST_0_NAME" . "$n=" . urlencode($line_item_name) .
    "&L_PAYMENTREQUEST_0_NUMBER" . "$n=" . urlencode($line_description) .
    "&L_PAYMENTREQUEST_0_QTY" . "$n=" . $qty .  
    "&L_PAYMENTREQUEST_0_AMT" . "$n=" . $cost;
    
   $line_order_total += $qty * $cost;
     
     
   $table = $table . '<tr>';    
   
   $table = $table . '<td valign="top">' . $line_id . "</td>"; 
     
   $table = $table . '<td valign="top">' . $line_item_name  . '&nbsp;' . $line_fields;   
     
   $table = $table . '</td>'; 
      
   $table = $table . '<td valign="top" align="right">' . $cost . "</td>"; 
      
   $table = $table . '<td valign="top" align="right">' . $qty . "</td>"; 
      
   $table = $table . '<td valign="top" align="right">&nbsp;&nbsp;' . number_format($qty * $cost,2,'.','') . '</td>'; 
  
   
// $table = $table . '<td valign="top" align="right">&nbsp;&nbsp;' . number_format($line,2,'.','') . '</td>'; 
  
   $table = $table . '</tr>';
    
	     $i++;
	     
	     } 	  
	     	     


//order_payment ???
	  if(isset($order['details']['BT']->order_payment) && $order['details']['BT']->order_payment > 0)
	     {  
	     $order_payment = $order['details']['BT']->order_payment;
	     
         $order_payment = $paymentCurrency->convertCurrencyTo($method->payment_currency, $order_payment, false);
         
         $order_payment = number_format($order_payment,2,'.','');
	     
	     $n = $i-1;
	     
	     $line_order_details .= 
         "&L_PAYMENTREQUEST_0_NAME" . "$n=Order+Payment" . 
         "&L_PAYMENTREQUEST_0_NUMBER" . "$n=transcost" .
         "&L_PAYMENTREQUEST_0_QTY" . "$n=1" .  
         "&L_PAYMENTREQUEST_0_AMT" . "$n=" . $order_payment;
    
         #    urlencode($line_item_name) .
    
         $line_order_total = $line_order_total + $order_payment;
    
	     $i++;
	     
        $table = $table . '<tr>';
	    $table = $table . '<td colspan="4" align="right">Order Payment:&nbsp;</td>';
	    $table = $table . '<td align="right">&nbsp;' . number_format($order_payment,2,'.','') . '</td></tr>';

	     }



$table = $table . '<tr>';
$table = $table . '<td colspan="4" align="right">' . JText::_('VMPAYMENT_WORLDPAY_SSC_ITEM_TOTAL') . ':&nbsp;</td>';
$table = $table . '<td align="right">&nbsp;' . number_format($line_order_total,2,'.','') . '</td></tr>';     
	     
	    
$line_order_total = round($line_order_total,2);

$shipping = 0;

	if(isset($order['details']['BT']->order_shipment) && $order['details']['BT']->order_shipment > 0)
		{  
	    $shipping = $order['details']['BT']->order_shipment;
	    
	    
	    $shipping = $paymentCurrency->convertCurrencyTo($method->payment_currency, $shipping, false);    
	    
	    $shipping = number_format($shipping,2,'.','');
	     	     
		$table = $table . '<tr>';
	    $table = $table . '<td colspan="4" align="right">' . JText::_('VMPAYMENT_WORLDPAY_SSC_SHIPPING') .':&nbsp;</td>';
	    $table = $table . '<td align="right">&nbsp;' . number_format($shipping,2,'.','') . '</td></tr>';
	    }


//number_format($amount,2,'.','');
$total_tax = 0;

	  if(isset($order['details']['BT']->order_tax) && $order['details']['BT']->order_tax > 0)
	     {  
	     $total_tax = $order['details']['BT']->order_tax;
	     
	     $total_tax = $paymentCurrency->convertCurrencyTo($method->payment_currency, $total_tax, false);
         }	     

	  if(isset($order['details']['BT']->order_shipment_tax) && $order['details']['BT']->order_shipment_tax > 0)
	     {
         $order_shippment_tax = $order['details']['BT']->order_shipment_tax;
         
         $order_shippment_tax = $paymentCurrency->convertCurrencyTo($method->payment_currency, $order_shippment_tax, false);
         
	     $total_tax += $order_shippment_tax;
	     }

	  if(isset($order['details']['BT']->order_payment_tax) && $order['details']['BT']->order_payment_tax > 0)
	     {
         $order_payment_tax = $order['details']['BT']->order_payment_tax;
         
         $order_payment_tax = $paymentCurrency->convertCurrencyTo($method->payment_currency, $order_payment_tax, false);
         
	     $total_tax += $order_payment_tax;
	     }


      if($total_tax > 0)
       {
	    $total_tax = number_format($total_tax,2,'.','');
	    
// add surcharge to tax total	    

	    $line_order_total2 = number_format($line_order_total,2,'.','');
		$shipping2 = number_format($shipping,2,'.','');
		$total_tax2 = number_format($total_tax,2,'.','');

		$order_total2 = $line_order_total2 + $shipping2 + $total_tax2;
		$order_total2 = number_format($order_total2,2,'.','');

		$cart_order_total2 = $order['details']['BT']->order_total;

		$cart_order_total2 = number_format($cart_order_total2,2,'.','');

		if($order_total2 < $cart_order_total2)
			{  
			$total_discount2 = $cart_order_total2 - $order_total2;
	     
			//$order_total2 = $cart_order_total2;
			$total_discount2 = number_format($total_discount2,2,'.','');
			    
	    	//if($total_tax-$total_discount2>0)
	    	//	{
	    		$total_tax += $total_discount2;
	    	//	}
	    
	    	}

//end of surchange adjustment



 
		$table = $table . '<tr>';
	    $table = $table . '<td colspan="4" align="right">' . JText::_('VMPAYMENT_WORLDPAY_SSC_TAX') . ':&nbsp;</td>';
	    $table = $table . '<td align="right">&nbsp;' . number_format($total_tax,2,'.','') . '</td></tr>';


	     }
	     
	     
$total_discount = 0;
//billDiscountAmount

/*
	  if(isset($order['details']['BT']->coupon_discount) && $order['details']['BT']->coupon_discount > 0)
	     {  
	     $total_discount = $order['details']['BT']->coupon_discount;
	     
         $total_discount = $paymentCurrency->convertCurrencyTo($method->payment_currency, $total_discount, false);
         
         $total_discount = number_format($total_discount,2,'.','');
	     
	     $n = $i-1;
	     
	     $line_order_details .= 
         "&L_PAYMENTREQUEST_0_NAME" . "$n=Coupon+Discount" . 
         "&L_PAYMENTREQUEST_0_NUMBER" . "$n=$order->couponCode" .
         "&L_PAYMENTREQUEST_0_QTY" . "$n=1" .  
         "&L_PAYMENTREQUEST_0_AMT" . "$n=-" . $total_discount;
    
         #    urlencode($line_item_name) .
    
         $line_order_total = $line_order_total - $total_discount;
    
	     $i++;
	     
        $table = $table . '<tr>';
	    $table = $table . '<td colspan="4"><p align="right">Coupon:&nbsp;</td>';
	    $table = $table . '<td align="right">&nbsp;' . number_format($total_discount,2,'.','') . '</td></tr>';

	     }
	     
	     
$total_discount = 0;
//billDiscountAmount
	  if(isset($order['details']['BT']->order_billDiscountAmount) && $order['details']['BT']->order_billDiscountAmount >0)
	     {  
	     $total_discount = $order['details']['BT']->order_billDiscountAmount;
	     
         $total_discount = $paymentCurrency->convertCurrencyTo($method->payment_currency, $total_discount, false);
         
         $total_discount = number_format($total_discount,2,'.','');
	     
	     // $total_discount = round($paymentCurrency->convertCurrencyTo($method->payment_currency, $total_discount, false),2);
	     
	     $n = $i-1;
	     
	     $line_order_details .= 
         "&L_PAYMENTREQUEST_0_NAME" . "$n=Discount" . 
         "&L_PAYMENTREQUEST_0_NUMBER" . "$n=Discount" .
         "&L_PAYMENTREQUEST_0_QTY" . "$n=1" .  
         "&L_PAYMENTREQUEST_0_AMT" . "$n=-" . $total_discount;
    
         #    urlencode($line_item_name) .
    
         $line_order_total = $line_order_total - $total_discount;
    
	     $i++;
	     
	    $table = $table . '<tr>';
	    $table = $table . '<td colspan="4"><p align="right">Discount:&nbsp;</td>';
	    $table = $table . '<td align="right">&nbsp;' . number_format($total_discount,2,'.','') . '</td></tr>';

	     }
*/


$line_order_total = number_format($line_order_total,2,'.','');
$shipping = number_format($shipping,2,'.','');
$total_tax = number_format($total_tax,2,'.','');

$order_total = $line_order_total + $shipping + $total_tax;
$order_total = number_format($order_total,2,'.','');


$cart_order_total = $order['details']['BT']->order_total;

$cart_order_total = number_format($cart_order_total,2,'.','');

if($order_total > $cart_order_total)
	     {  
	     $total_discount = $cart_order_total - $order_total;
	     
	     $order_total = $cart_order_total;
	     
         $total_discount = number_format($total_discount,2,'.','');
	     
	     $n = $i-1;
	     
	     $line_order_details .= 
         "&L_PAYMENTREQUEST_0_NAME" . "$n=Discount" . 
         "&L_PAYMENTREQUEST_0_NUMBER" . "$n=Discount" .
         "&L_PAYMENTREQUEST_0_QTY" . "$n=1" .  
         "&L_PAYMENTREQUEST_0_AMT" . "$n=" . $total_discount;
    
         #    urlencode($line_item_name) .
    
         $line_order_total = $line_order_total + $total_discount;
    
	     $i++;
	     
        $table = $table . '<tr>';
	    $table = $table . '<td colspan="4" align="right">Discount:&nbsp;</td>';
	    $table = $table . '<td align="right">&nbsp;' . number_format($total_discount,2,'.','') . '</td></tr>';

	     }


if($order_total < $cart_order_total)
	     {  
	     $total_discount = $cart_order_total - $order_total;
	     
	     $order_total = $cart_order_total;
	     
         $total_discount = number_format($total_discount,2,'.','');
	     
	     $n = $i-1;
	     
	     $line_order_details .= 
         "&L_PAYMENTREQUEST_0_NAME" . "$n=Surcharge" . 
         "&L_PAYMENTREQUEST_0_NUMBER" . "$n=Surcharge" .
         "&L_PAYMENTREQUEST_0_QTY" . "$n=1" .  
         "&L_PAYMENTREQUEST_0_AMT" . "$n=" . $total_discount;
    
         #    urlencode($line_item_name) .
    
         $line_order_total = $line_order_total + $total_discount;
    
	     $i++;
	     
        $table = $table . '<tr>';
	    $table = $table . '<td colspan="4" align="right">' . JText::_('VMPAYMENT_WORLDPAY_SSC_SURCHARGE') . ':&nbsp;</td>';
	    $table = $table . '<td align="right">&nbsp;' . number_format($total_discount,2,'.','') . '</td></tr>';

	     }





	    $table = $table . '<tr>';
	    $table = $table . '<td colspan="4" align="right">' . JText::_('VMPAYMENT_WORLDPAY_SSC_ORDER_TOTAL') . ':&nbsp;</td>';
	    $table = $table . '<td align="right">&nbsp;' . number_format($order_total,2,'.','') . '</td></tr>';

 $table .= '</table>';


//$method->debug = 0;
//if($method->debug)
//	{

//print '--------------------------';
//print_r($order);
//print '++++++++++++++++++++++++++<br>';
//print 'Version 1.0.4<br>';
//}

return array($table,$order_total,$line_order_total,$total_discount,$total_tax,$shipping,$line_order_details);

}


function billing_address_html($address,$country,$state)
{

$editbillingaddress =  JText::_('VMPAYMENT_WORLDPAY_SSC_EDIT_BILLING_ADDRESS');
$order_form_name = JText::_('VMPAYMENT_WORLDPAY_SSC_NAME');
$order_form_address = JText::_('VMPAYMENT_WORLDPAY_SSC_ADDRESS');
$order_form_city = JText::_('VMPAYMENT_WORLDPAY_SSC_CITY');
$order_form_state = JText::_('VMPAYMENT_WORLDPAY_SSC_STATE');
$order_form_country = JText::_('VMPAYMENT_WORLDPAY_SSC_COUNTRY');
$order_form_state = JText::_('VMPAYMENT_WORLDPAY_SSC_STATE');
$order_form_zip = JText::_('VMPAYMENT_WORLDPAY_SSC_ZIP');
#$order_form_telephone = JText::_('VMPAYMENT_WORLDPAY_SSC_TELEPHONE');
#$order_form_email = JText::_('VMPAYMENT_WORLDPAY_SSC_EMAIL');
#$order_form_paypal_billing_address = JText::_('VMPAYMENT_WORLDPAY_SSC_PAYPAL_BILLING_ADDRESS');
#$order_form_item_total = JText::_('VMPAYMENT_WORLDPAY_SSC_ITEM_TOTAL');
#$order_form_tax = JText::_('VMPAYMENT_WORLDPAY_SSC_TAX');
#$order_form_surcharge = JText::_('VMPAYMENT_WORLDPAY_SSC_SURCHARGE');
#$order_form_shipping = JText::_('VMPAYMENT_WORLDPAY_SSC_SHIPPING');
#$order_form_order_total = JText::_('VMPAYMENT_WORLDPAY_SSC_ORDER_TOTAL');


$order_form_html_shipping = <<<directorderform
<table
   cellpadding="2">
    <tr align="center">
       <td style="border-style:hidden" align="right">$order_form_name:</td>
       <td style="border-style:hidden" align="left" >&nbsp;
$address->first_name $address->last_name
      </td>
    </tr>
    <tr align="center">
       <td style="border-style:hidden" align="right">$order_form_address:</td>
       <td style="border-style:hidden" align="left" >&nbsp;
$address->address_1      
      </td>
    </tr>
    <tr align="center">
       <td style="border-style:hidden" align="right"></td>
       <td style="border-style:hidden" align="left" >&nbsp;
$address->address_2
      </td>
    </tr>
    <tr align="center">
       <td style="border-style:hidden" align="right">$order_form_city:</td>
       <td style="border-style:hidden" align="left" >&nbsp;
$address->city
      </td>
    </tr><tr align="center">
       <td style="border-style:hidden" align="right">$order_form_state:</td>
       <td style="border-style:hidden" align="left" >&nbsp;
$state
      </td>
    </tr><tr align="center">
       <td style="border-style:hidden" align="right">$order_form_country:</td>
       <td style="border-style:hidden" align="left" >&nbsp;
$country      
      </td>
    </tr>
    <tr align="center">
       <td style="border-style:hidden" align="right">$order_form_zip:</td>
       <td style="border-style:hidden" align="left" >&nbsp;
$address->zip
      </td>
    </tr>
    <tr align="center">
       <td style="border-style:hidden" align="right">&nbsp;</td>
       <td style="border-style:hidden" align="left" >&nbsp;
      </td>
    </tr>
    </tbody>
  </table>
directorderform;

return $order_form_html_shipping;
}





function shipping_address_html($del_address,$del_country,$del_state)
{

$editbillingaddress =  JText::_('VMPAYMENT_WORLDPAY_SSC_EDIT_BILLING_ADDRESS');
$order_form_name = JText::_('VMPAYMENT_WORLDPAY_SSC_NAME');
$order_form_address = JText::_('VMPAYMENT_WORLDPAY_SSC_ADDRESS');
$order_form_city = JText::_('VMPAYMENT_WORLDPAY_SSC_CITY');
$order_form_state = JText::_('VMPAYMENT_WORLDPAY_SSC_STATE');
$order_form_country = JText::_('VMPAYMENT_WORLDPAY_SSC_COUNTRY');
$order_form_state = JText::_('VMPAYMENT_WORLDPAY_SSC_STATE');
$order_form_zip = JText::_('VMPAYMENT_WORLDPAY_SSC_ZIP');
#$order_form_telephone = JText::_('VMPAYMENT_WORLDPAY_SSC_TELEPHONE');
#$order_form_email = JText::_('VMPAYMENT_WORLDPAY_SSC_EMAIL');
#$order_form_paypal_billing_address = JText::_('VMPAYMENT_WORLDPAY_SSC_PAYPAL_BILLING_ADDRESS');
#$order_form_item_total = JText::_('VMPAYMENT_WORLDPAY_SSC_ITEM_TOTAL');
#$order_form_tax = JText::_('VMPAYMENT_WORLDPAY_SSC_TAX');
#$order_form_surcharge = JText::_('VMPAYMENT_WORLDPAY_SSC_SURCHARGE');
#$order_form_shipping = JText::_('VMPAYMENT_WORLDPAY_SSC_SHIPPING');
#$order_form_order_total = JText::_('VMPAYMENT_WORLDPAY_SSC_ORDER_TOTAL');


$order_form_html_shipping = <<<directorderform
<table
   cellpadding="2">
    <tr align="center">
       <td style="border-style:hidden" align="right">$order_form_name:</td>
       <td style="border-style:hidden" align="left" >&nbsp;
$del_address->first_name $del_address->last_name
      </td>
    </tr>
    <tr align="center">
       <td style="border-style:hidden" align="right">$order_form_address:</td>
       <td style="border-style:hidden" align="left" >&nbsp;
$del_address->address_1      
      </td>
    </tr>
    <tr align="center">
       <td style="border-style:hidden" align="right"></td>
       <td style="border-style:hidden" align="left" >&nbsp;
$del_address->address_2
      </td>
    </tr>
    <tr align="center">
       <td style="border-style:hidden" align="right">$order_form_city:</td>
       <td style="border-style:hidden" align="left" >&nbsp;
$del_address->city
      </td>
    </tr><tr align="center">
       <td style="border-style:hidden" align="right">$order_form_state:</td>
       <td style="border-style:hidden" align="left" >&nbsp;
$del_state
      </td>
    </tr><tr align="center">
       <td style="border-style:hidden" align="right">$order_form_country:</td>
       <td style="border-style:hidden" align="left" >&nbsp;
$del_country      
      </td>
    </tr>
    <tr align="center">
       <td style="border-style:hidden" align="right">$order_form_zip:</td>
       <td style="border-style:hidden" align="left" >&nbsp;
$del_address->zip
      </td>
    </tr>
    <tr align="center">
       <td style="border-style:hidden" align="right">&nbsp;</td>
       <td style="border-style:hidden" align="left" >&nbsp;
      </td>
    </tr>

    </tbody>
  </table>
directorderform;

return $order_form_html_shipping;
}





function sendEmailToVendorAndAdmins ($subject, $message)
	{

		// recipient is vendor and admin
		$vendorId = 1;
		$vendorModel = VmModel::getModel ('vendor');
		$vendorEmail = $vendorModel->getVendorEmail ($vendorId);
		$vendorName = $vendorModel->getVendorName ($vendorId);
		JUtility::sendMail ($vendorEmail, $vendorName, $vendorEmail, $subject, $message);
		if (JVM_VERSION === 1) {
			//get all super administrator
			$query = 'SELECT name, email, sendEmail' .
				' FROM #__users' .
				' WHERE LOWER( usertype ) = "super administrator"';
		} else {
			$query = 'SELECT name, email, sendEmail' .
				' FROM #__users' .
				' WHERE sendEmail=1';
		}
		$db = JFactory::getDBO ();
		$db->setQuery ($query);
		$rows = $db->loadObjectList ();

		$subject = html_entity_decode ($subject, ENT_QUOTES);

		// get superadministrators id
		foreach ($rows as $row) {
			if ($row->sendEmail) {
			
			if($vendorEmail === $row->sendEmail)
				{
				$message = html_entity_decode ($message, ENT_QUOTES);
				JUtility::sendMail ($vendorEmail, $vendorName, $row->email, $subject, $message);
				}
			}
		}
	}












    function _getTablepkeyValue($virtuemart_order_id) {
	$db = JFactory::getDBO();
	$q = 'SELECT ' . $this->_tablepkey . ' FROM `' . $this->_tablename . '` '
		. 'WHERE `virtuemart_order_id` = ' . $virtuemart_order_id;
	$db->setQuery($q);

	if (!($pkey = $db->loadResult())) {
	    JError::raiseWarning(500, $db->getErrorMsg());
	    return '';
	}
	return $pkey;
    }

    /**
     * Display stored payment data for an order
     * @see components/com_virtuemart/helpers/vmPSPlugin::plgVmOnShowOrderBEPayment()
     */
    function plgVmOnShowOrderBEPayment($virtuemart_order_id, $payment_method_id) {

	if (!$this->selectedThisByMethodId($payment_method_id)) {
	    return null; // Another method was selected, do nothing
	}

	$db = JFactory::getDBO();
	$q = 'SELECT * FROM `' . $this->_tablename . '` '
		. 'WHERE `virtuemart_order_id` = ' . $virtuemart_order_id;
	$db->setQuery($q);
	if (!($paymentTable = $db->loadObject())) {
	    // JError::raiseWarning(500, $db->getErrorMsg());
	    return '';
	}
	$this->getPaymentCurrency($paymentTable);
	$q = 'SELECT `currency_code_3` FROM `#__virtuemart_currencies` WHERE `virtuemart_currency_id`="' . $paymentTable->payment_currency . '" ';
	$db = &JFactory::getDBO();
	$db->setQuery($q);
	$currency_code_3 = $db->loadResult();
	$html = '<table class="adminlist">' . "\n";
	$html .=$this->getHtmlHeaderBE();
	$html .= $this->getHtmlRowBE('PAYPAL_PAYMENT_NAME', $paymentTable->payment_name);
	//$html .= $this->getHtmlRowBE('PAYPAL_PAYMENT_TOTAL_CURRENCY', $paymentTable->payment_order_total.' '.$currency_code_3);
	$code = "paypal_response_";
	foreach ($paymentTable as $key => $value) {
	    if (substr($key, 0, strlen($code)) == $code) {
		$html .= $this->getHtmlRowBE($key, $value);
	    }
	}
	$html .= '</table>' . "\n";
	return $html;
    }

   

    function getCosts(VirtueMartCart $cart, $method, $cart_prices) {
	if (preg_match('/%$/', $method->cost_percent_total)) {
	    $cost_percent_total = substr($method->cost_percent_total, 0, -1);
	} else {
	    $cost_percent_total = $method->cost_percent_total;
	}
	return ($method->cost_per_transaction + ($cart_prices['salesPrice'] * $cost_percent_total * 0.01));
    }

    /**
     * Check if the payment conditions are fulfilled for this payment method
     * @author: Valerie Isaksen
     *
     * @param $cart_prices: cart prices
     * @param $payment
     * @return true: if the conditions are fulfilled, false otherwise
     *
     */
    protected function checkConditions($cart, $method, $cart_prices) {


	$address = (($cart->ST == 0) ? $cart->BT : $cart->ST);

	$amount = $cart_prices['salesPrice'];
	$amount_cond = ($amount >= $method->min_amount AND $amount <= $method->max_amount OR ($method->min_amount <= $amount AND ($method->max_amount == 0) ));

	$countries = array();
	if (!empty($method->countries)) {
	    if (!is_array($method->countries)) {
		$countries[0] = $method->countries;
	    } else {
		$countries = $method->countries;
	    }
	}
	// probably did not gave his BT:ST address
	if (!is_array($address)) {
	    $address = array();
	    $address['virtuemart_country_id'] = 0;
	}

	if (!isset($address['virtuemart_country_id']))
	    $address['virtuemart_country_id'] = 0;
	if (in_array($address['virtuemart_country_id'], $countries) || count($countries) == 0) {
	    if ($amount_cond) {
		return true;
	    }
	}

	return false;
    }

    /**
     * We must reimplement this triggers for joomla 1.7
     */

    /**
     * Create the table for this plugin if it does not yet exist.
     * This functions checks if the called plugin is active one.
     * When yes it is calling the standard method to create the tables
     * @author Valrie Isaksen
     *
     */
    function plgVmOnStoreInstallPaymentPluginTable($jplugin_id) {

	return $this->onStoreInstallPluginTable($jplugin_id);
    }

    /**
     * This event is fired after the payment method has been selected. It can be used to store
     * additional payment info in the cart.
     *
     * @author Max Milbers
     * @author Valrie isaksen
     *
     * @param VirtueMartCart $cart: the actual cart
     * @return null if the payment was not selected, true if the data is valid, error message if the data is not vlaid
     *
     */
    public function plgVmOnSelectCheckPayment(VirtueMartCart $cart) {
	return $this->OnSelectCheck($cart);
    }

    /**
     * plgVmDisplayListFEPayment
     * This event is fired to display the pluginmethods in the cart (edit shipment/payment) for exampel
     *
     * @param object $cart Cart object
     * @param integer $selected ID of the method selected
     * @return boolean True on succes, false on failures, null when this plugin was not selected.
     * On errors, JError::raiseWarning (or JError::raiseError) must be used to set a message.
     *
     * @author Valerie Isaksen
     * @author Max Milbers
     */
    public function plgVmDisplayListFEPayment(VirtueMartCart $cart, $selected = 0, &$htmlIn) {
	return $this->displayListFE($cart, $selected, $htmlIn);
    }

    /*
     * plgVmonSelectedCalculatePricePayment
     * Calculate the price (value, tax_id) of the selected method
     * It is called by the calculator
     * This function does NOT to be reimplemented. If not reimplemented, then the default values from this function are taken.
     * @author Valerie Isaksen
     * @cart: VirtueMartCart the current cart
     * @cart_prices: array the new cart prices
     * @return null if the method was not selected, false if the shiiping rate is not valid any more, true otherwise
     *
     *
     */

    public function plgVmonSelectedCalculatePricePayment(VirtueMartCart $cart, array &$cart_prices, &$cart_prices_name) {
	return $this->onSelectedCalculatePrice($cart, $cart_prices, $cart_prices_name);
    }

    /**
     * plgVmOnCheckAutomaticSelectedPayment
     * Checks how many plugins are available. If only one, the user will not have the choice. Enter edit_xxx page
     * The plugin must check first if it is the correct type
     * @author Valerie Isaksen
     * @param VirtueMartCart cart: the cart object
     * @return null if no plugin was found, 0 if more then one plugin was found,  virtuemart_xxx_id if only one plugin is found
     *
     */
    function plgVmOnCheckAutomaticSelectedPayment(VirtueMartCart $cart, array $cart_prices = array()) {
	return $this->onCheckAutomaticSelected($cart, $cart_prices);
    }

    /**
     * This method is fired when showing the order details in the frontend.
     * It displays the method-specific data.
     *
     * @param integer $order_id The order ID
     * @return mixed Null for methods that aren't active, text (HTML) otherwise
     * @author Max Milbers
     * @author Valerie Isaksen
     */
    public function plgVmOnShowOrderFEPayment($virtuemart_order_id, $virtuemart_paymentmethod_id, &$payment_name) {
	$this->onShowOrderFE($virtuemart_order_id, $virtuemart_paymentmethod_id, $payment_name);
    }

    /**
     * This event is fired during the checkout process. It can be used to validate the
     * method data as entered by the user.
     *
     * @return boolean True when the data was valid, false otherwise. If the plugin is not activated, it should return null.
     * @author Max Milbers

      public function plgVmOnCheckoutCheckDataPayment($psType, VirtueMartCart $cart) {
      return null;
      }
     */

    /**
     * This method is fired when showing when priting an Order
     * It displays the the payment method-specific data.
     *
     * @param integer $_virtuemart_order_id The order ID
     * @param integer $method_id  method used for this order
     * @return mixed Null when for payment methods that were not selected, text (HTML) otherwise
     * @author Valerie Isaksen
     */
    function plgVmonShowOrderPrintPayment($order_number, $method_id) {
	return $this->onShowOrderPrint($order_number, $method_id);
    }

    /**
     * Save updated order data to the method specific table
     *
     * @param array $_formData Form data
     * @return mixed, True on success, false on failures (the rest of the save-process will be
     * skipped!), or null when this method is not actived.
     * @author Oscar van Eijk

      public function plgVmOnUpdateOrderPayment(  $_formData) {
      return null;
      }
     */
    /**
     * Save updated orderline data to the method specific table
     *
     * @param array $_formData Form data
     * @return mixed, True on success, false on failures (the rest of the save-process will be
     * skipped!), or null when this method is not actived.
     * @author Oscar van Eijk

      public function plgVmOnUpdateOrderLine(  $_formData) {
      return null;
      }
     */
    /**
     * plgVmOnEditOrderLineBE
     * This method is fired when editing the order line details in the backend.
     * It can be used to add line specific package codes
     *
     * @param integer $_orderId The order ID
     * @param integer $_lineId
     * @return mixed Null for method that aren't active, text (HTML) otherwise
     * @author Oscar van Eijk

      public function plgVmOnEditOrderLineBE(  $_orderId, $_lineId) {
      return null;
      }
     */

    /**
     * This method is fired when showing the order details in the frontend, for every orderline.
     * It can be used to display line specific package codes, e.g. with a link to external tracking and
     * tracing systems
     *
     * @param integer $_orderId The order ID
     * @param integer $_lineId
     * @return mixed Null for method that aren't active, text (HTML) otherwise
     * @author Oscar van Eijk

      public function plgVmOnShowOrderLineFE(  $_orderId, $_lineId) {
      return null;
      }
     */
    function plgVmDeclarePluginParamsPayment($name, $id, &$data) {
	return $this->declarePluginParams('payment', $name, $id, $data);
    }

    function plgVmSetOnTablePluginParamsPayment($name, $id, &$table) {
	return $this->setOnTablePluginParams($name, $id, $table);
    }

}

// No closing tag

